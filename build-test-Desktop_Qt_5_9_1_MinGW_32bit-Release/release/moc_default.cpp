/****************************************************************************
** Meta object code from reading C++ file 'default.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../test/default.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'default.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Default_t {
    QByteArrayData data[10];
    char stringdata0[156];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Default_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Default_t qt_meta_stringdata_Default = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Default"
QT_MOC_LITERAL(1, 8, 16), // "ButtonOk_Clicked"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 24), // "ButtonOK_PayPass_Clicked"
QT_MOC_LITERAL(4, 51, 20), // "def_change_text_edit"
QT_MOC_LITERAL(5, 72, 23), // "on_pushButtonOK_clicked"
QT_MOC_LITERAL(6, 96, 27), // "on_pushButtonCancel_clicked"
QT_MOC_LITERAL(7, 124, 12), // "set_text_MSB"
QT_MOC_LITERAL(8, 137, 5), // "index"
QT_MOC_LITERAL(9, 143, 12) // "set_text_LSB"

    },
    "Default\0ButtonOk_Clicked\0\0"
    "ButtonOK_PayPass_Clicked\0def_change_text_edit\0"
    "on_pushButtonOK_clicked\0"
    "on_pushButtonCancel_clicked\0set_text_MSB\0"
    "index\0set_text_LSB"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Default[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   51,    2, 0x0a /* Public */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    0,   53,    2, 0x08 /* Private */,
       7,    1,   54,    2, 0x08 /* Private */,
       9,    1,   57,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Int,    8,

       0        // eod
};

void Default::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Default *_t = static_cast<Default *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ButtonOk_Clicked(); break;
        case 1: _t->ButtonOK_PayPass_Clicked(); break;
        case 2: _t->def_change_text_edit(); break;
        case 3: _t->on_pushButtonOK_clicked(); break;
        case 4: _t->on_pushButtonCancel_clicked(); break;
        case 5: _t->set_text_MSB((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->set_text_LSB((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Default::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Default::ButtonOk_Clicked)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Default::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Default::ButtonOK_PayPass_Clicked)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject Default::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Default.data,
      qt_meta_data_Default,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Default::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Default::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Default.stringdata0))
        return static_cast<void*>(const_cast< Default*>(this));
    return QDialog::qt_metacast(_clname);
}

int Default::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void Default::ButtonOk_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Default::ButtonOK_PayPass_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
