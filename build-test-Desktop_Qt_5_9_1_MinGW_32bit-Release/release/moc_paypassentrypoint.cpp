/****************************************************************************
** Meta object code from reading C++ file 'paypassentrypoint.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../test/paypassentrypoint.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'paypassentrypoint.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PayPassEntryPoint_t {
    QByteArrayData data[34];
    char stringdata0[770];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PayPassEntryPoint_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PayPassEntryPoint_t qt_meta_stringdata_PayPassEntryPoint = {
    {
QT_MOC_LITERAL(0, 0, 17), // "PayPassEntryPoint"
QT_MOC_LITERAL(1, 18, 27), // "ToolButtonAddTerCap_clicked"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 28), // "ToolButtonTACDefault_clicked"
QT_MOC_LITERAL(4, 76, 27), // "ToolButtonTACDenial_clicked"
QT_MOC_LITERAL(5, 104, 27), // "ToolButtonTACOnline_clicked"
QT_MOC_LITERAL(6, 132, 24), // "ToolButtonSecCap_clicked"
QT_MOC_LITERAL(7, 157, 26), // "ToolButtonCardData_clicked"
QT_MOC_LITERAL(8, 184, 25), // "ToolButtonnCVMCap_clicked"
QT_MOC_LITERAL(9, 210, 27), // "ToolButtonKerConfig_clicked"
QT_MOC_LITERAL(10, 238, 10), // "UpdateData"
QT_MOC_LITERAL(11, 249, 15), // "UpdateAddTerCap"
QT_MOC_LITERAL(12, 265, 13), // "UpdateTACData"
QT_MOC_LITERAL(13, 279, 19), // "Update1ByteLineEdit"
QT_MOC_LITERAL(14, 299, 14), // "UpdateCardData"
QT_MOC_LITERAL(15, 314, 12), // "UpdateSecCap"
QT_MOC_LITERAL(16, 327, 12), // "UpdateCVMCap"
QT_MOC_LITERAL(17, 340, 15), // "UpdateKerConfig"
QT_MOC_LITERAL(18, 356, 24), // "checkbox_update_lineEdit"
QT_MOC_LITERAL(19, 381, 5), // "index"
QT_MOC_LITERAL(20, 387, 17), // "button_ok_clicked"
QT_MOC_LITERAL(21, 405, 30), // "on_toolButtonAddTerCap_clicked"
QT_MOC_LITERAL(22, 436, 31), // "on_toolButtonTACDefault_clicked"
QT_MOC_LITERAL(23, 468, 30), // "on_toolButtonTACDenial_clicked"
QT_MOC_LITERAL(24, 499, 30), // "on_toolButtonTACOnline_clicked"
QT_MOC_LITERAL(25, 530, 27), // "on_toolButtonSecCap_clicked"
QT_MOC_LITERAL(26, 558, 29), // "on_toolButtonCardData_clicked"
QT_MOC_LITERAL(27, 588, 27), // "on_toolButtonCVMCap_clicked"
QT_MOC_LITERAL(28, 616, 29), // "on_toolButtonCVMCapNo_clicked"
QT_MOC_LITERAL(29, 646, 27), // "on_toolButtonKerCon_clicked"
QT_MOC_LITERAL(30, 674, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(31, 696, 25), // "on_checkBoxcombo1_clicked"
QT_MOC_LITERAL(32, 722, 25), // "on_checkBoxCombo2_clicked"
QT_MOC_LITERAL(33, 748, 21) // "on_checkBox17_clicked"

    },
    "PayPassEntryPoint\0ToolButtonAddTerCap_clicked\0"
    "\0ToolButtonTACDefault_clicked\0"
    "ToolButtonTACDenial_clicked\0"
    "ToolButtonTACOnline_clicked\0"
    "ToolButtonSecCap_clicked\0"
    "ToolButtonCardData_clicked\0"
    "ToolButtonnCVMCap_clicked\0"
    "ToolButtonKerConfig_clicked\0UpdateData\0"
    "UpdateAddTerCap\0UpdateTACData\0"
    "Update1ByteLineEdit\0UpdateCardData\0"
    "UpdateSecCap\0UpdateCVMCap\0UpdateKerConfig\0"
    "checkbox_update_lineEdit\0index\0"
    "button_ok_clicked\0on_toolButtonAddTerCap_clicked\0"
    "on_toolButtonTACDefault_clicked\0"
    "on_toolButtonTACDenial_clicked\0"
    "on_toolButtonTACOnline_clicked\0"
    "on_toolButtonSecCap_clicked\0"
    "on_toolButtonCardData_clicked\0"
    "on_toolButtonCVMCap_clicked\0"
    "on_toolButtonCVMCapNo_clicked\0"
    "on_toolButtonKerCon_clicked\0"
    "on_pushButton_clicked\0on_checkBoxcombo1_clicked\0"
    "on_checkBoxCombo2_clicked\0"
    "on_checkBox17_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PayPassEntryPoint[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      31,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  169,    2, 0x06 /* Public */,
       3,    0,  170,    2, 0x06 /* Public */,
       4,    0,  171,    2, 0x06 /* Public */,
       5,    0,  172,    2, 0x06 /* Public */,
       6,    0,  173,    2, 0x06 /* Public */,
       7,    0,  174,    2, 0x06 /* Public */,
       8,    0,  175,    2, 0x06 /* Public */,
       9,    0,  176,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,  177,    2, 0x0a /* Public */,
      11,    0,  178,    2, 0x0a /* Public */,
      12,    0,  179,    2, 0x0a /* Public */,
      13,    0,  180,    2, 0x0a /* Public */,
      14,    0,  181,    2, 0x0a /* Public */,
      15,    0,  182,    2, 0x0a /* Public */,
      16,    0,  183,    2, 0x0a /* Public */,
      17,    0,  184,    2, 0x0a /* Public */,
      18,    1,  185,    2, 0x0a /* Public */,
      20,    0,  188,    2, 0x0a /* Public */,
      21,    0,  189,    2, 0x08 /* Private */,
      22,    0,  190,    2, 0x08 /* Private */,
      23,    0,  191,    2, 0x08 /* Private */,
      24,    0,  192,    2, 0x08 /* Private */,
      25,    0,  193,    2, 0x08 /* Private */,
      26,    0,  194,    2, 0x08 /* Private */,
      27,    0,  195,    2, 0x08 /* Private */,
      28,    0,  196,    2, 0x08 /* Private */,
      29,    0,  197,    2, 0x08 /* Private */,
      30,    0,  198,    2, 0x08 /* Private */,
      31,    0,  199,    2, 0x08 /* Private */,
      32,    0,  200,    2, 0x08 /* Private */,
      33,    0,  201,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PayPassEntryPoint::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PayPassEntryPoint *_t = static_cast<PayPassEntryPoint *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ToolButtonAddTerCap_clicked(); break;
        case 1: _t->ToolButtonTACDefault_clicked(); break;
        case 2: _t->ToolButtonTACDenial_clicked(); break;
        case 3: _t->ToolButtonTACOnline_clicked(); break;
        case 4: _t->ToolButtonSecCap_clicked(); break;
        case 5: _t->ToolButtonCardData_clicked(); break;
        case 6: _t->ToolButtonnCVMCap_clicked(); break;
        case 7: _t->ToolButtonKerConfig_clicked(); break;
        case 8: _t->UpdateData(); break;
        case 9: _t->UpdateAddTerCap(); break;
        case 10: _t->UpdateTACData(); break;
        case 11: _t->Update1ByteLineEdit(); break;
        case 12: _t->UpdateCardData(); break;
        case 13: _t->UpdateSecCap(); break;
        case 14: _t->UpdateCVMCap(); break;
        case 15: _t->UpdateKerConfig(); break;
        case 16: _t->checkbox_update_lineEdit((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->button_ok_clicked(); break;
        case 18: _t->on_toolButtonAddTerCap_clicked(); break;
        case 19: _t->on_toolButtonTACDefault_clicked(); break;
        case 20: _t->on_toolButtonTACDenial_clicked(); break;
        case 21: _t->on_toolButtonTACOnline_clicked(); break;
        case 22: _t->on_toolButtonSecCap_clicked(); break;
        case 23: _t->on_toolButtonCardData_clicked(); break;
        case 24: _t->on_toolButtonCVMCap_clicked(); break;
        case 25: _t->on_toolButtonCVMCapNo_clicked(); break;
        case 26: _t->on_toolButtonKerCon_clicked(); break;
        case 27: _t->on_pushButton_clicked(); break;
        case 28: _t->on_checkBoxcombo1_clicked(); break;
        case 29: _t->on_checkBoxCombo2_clicked(); break;
        case 30: _t->on_checkBox17_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonAddTerCap_clicked)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonTACDefault_clicked)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonTACDenial_clicked)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonTACOnline_clicked)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonSecCap_clicked)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonCardData_clicked)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonnCVMCap_clicked)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (PayPassEntryPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PayPassEntryPoint::ToolButtonKerConfig_clicked)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject PayPassEntryPoint::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PayPassEntryPoint.data,
      qt_meta_data_PayPassEntryPoint,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PayPassEntryPoint::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PayPassEntryPoint::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PayPassEntryPoint.stringdata0))
        return static_cast<void*>(const_cast< PayPassEntryPoint*>(this));
    return QDialog::qt_metacast(_clname);
}

int PayPassEntryPoint::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 31)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 31)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 31;
    }
    return _id;
}

// SIGNAL 0
void PayPassEntryPoint::ToolButtonAddTerCap_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void PayPassEntryPoint::ToolButtonTACDefault_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void PayPassEntryPoint::ToolButtonTACDenial_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void PayPassEntryPoint::ToolButtonTACOnline_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void PayPassEntryPoint::ToolButtonSecCap_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void PayPassEntryPoint::ToolButtonCardData_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void PayPassEntryPoint::ToolButtonnCVMCap_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void PayPassEntryPoint::ToolButtonKerConfig_clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
