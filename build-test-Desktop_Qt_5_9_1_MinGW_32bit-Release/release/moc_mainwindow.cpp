/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../test/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[33];
    char stringdata0[745];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 22), // "ButtonTerminal_Clicked"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 22), // "ButtonAddition_Clicked"
QT_MOC_LITERAL(4, 58, 21), // "ButtonDefault_Clicked"
QT_MOC_LITERAL(5, 80, 20), // "ButtonOnline_Clicked"
QT_MOC_LITERAL(6, 101, 16), // "Button_7_Clicked"
QT_MOC_LITERAL(7, 118, 13), // "UpdatePaypass"
QT_MOC_LITERAL(8, 132, 15), // "UpdatePureEntry"
QT_MOC_LITERAL(9, 148, 13), // "UpdatePaywave"
QT_MOC_LITERAL(10, 162, 16), // "UpdateExpressPay"
QT_MOC_LITERAL(11, 179, 9), // "UpdateJCB"
QT_MOC_LITERAL(12, 189, 16), // "GroupButtonClick"
QT_MOC_LITERAL(13, 206, 5), // "Index"
QT_MOC_LITERAL(14, 212, 17), // "Additional_Update"
QT_MOC_LITERAL(15, 230, 15), // "Terminal_Update"
QT_MOC_LITERAL(16, 246, 14), // "Default_Update"
QT_MOC_LITERAL(17, 261, 13), // "OpenNewWindow"
QT_MOC_LITERAL(18, 275, 29), // "on_toolButtonAddition_clicked"
QT_MOC_LITERAL(19, 305, 28), // "on_toolButtonDefault_clicked"
QT_MOC_LITERAL(20, 334, 27), // "on_toolButtonOnline_clicked"
QT_MOC_LITERAL(21, 362, 23), // "on_toolButton_7_clicked"
QT_MOC_LITERAL(22, 386, 26), // "on_checkBoxDefault_clicked"
QT_MOC_LITERAL(23, 413, 29), // "on_toolButtonTerminal_clicked"
QT_MOC_LITERAL(24, 443, 34), // "on_toolButtonAddProcessing_cl..."
QT_MOC_LITERAL(25, 478, 37), // "on_toolButtonDenialProcessing..."
QT_MOC_LITERAL(26, 516, 37), // "on_toolButtonOnlineProcessing..."
QT_MOC_LITERAL(27, 554, 40), // "on_toolButtonDefaultProcessin..."
QT_MOC_LITERAL(28, 595, 28), // "on_toolButtonEntryUp_clicked"
QT_MOC_LITERAL(29, 624, 30), // "on_toolButtonEntryDown_clicked"
QT_MOC_LITERAL(30, 655, 29), // "on_toolButtonEntryAdd_clicked"
QT_MOC_LITERAL(31, 685, 32), // "on_toolButtonEntryRemove_clicked"
QT_MOC_LITERAL(32, 718, 26) // "on_ButtonEntrySave_clicked"

    },
    "MainWindow\0ButtonTerminal_Clicked\0\0"
    "ButtonAddition_Clicked\0ButtonDefault_Clicked\0"
    "ButtonOnline_Clicked\0Button_7_Clicked\0"
    "UpdatePaypass\0UpdatePureEntry\0"
    "UpdatePaywave\0UpdateExpressPay\0UpdateJCB\0"
    "GroupButtonClick\0Index\0Additional_Update\0"
    "Terminal_Update\0Default_Update\0"
    "OpenNewWindow\0on_toolButtonAddition_clicked\0"
    "on_toolButtonDefault_clicked\0"
    "on_toolButtonOnline_clicked\0"
    "on_toolButton_7_clicked\0"
    "on_checkBoxDefault_clicked\0"
    "on_toolButtonTerminal_clicked\0"
    "on_toolButtonAddProcessing_clicked\0"
    "on_toolButtonDenialProcessing_clicked\0"
    "on_toolButtonOnlineProcessing_clicked\0"
    "on_toolButtonDefaultProcessing_2_clicked\0"
    "on_toolButtonEntryUp_clicked\0"
    "on_toolButtonEntryDown_clicked\0"
    "on_toolButtonEntryAdd_clicked\0"
    "on_toolButtonEntryRemove_clicked\0"
    "on_ButtonEntrySave_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  164,    2, 0x06 /* Public */,
       3,    0,  165,    2, 0x06 /* Public */,
       4,    0,  166,    2, 0x06 /* Public */,
       5,    0,  167,    2, 0x06 /* Public */,
       6,    0,  168,    2, 0x06 /* Public */,
       7,    0,  169,    2, 0x06 /* Public */,
       8,    0,  170,    2, 0x06 /* Public */,
       9,    0,  171,    2, 0x06 /* Public */,
      10,    0,  172,    2, 0x06 /* Public */,
      11,    0,  173,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,  174,    2, 0x0a /* Public */,
      14,    0,  177,    2, 0x0a /* Public */,
      15,    0,  178,    2, 0x0a /* Public */,
      16,    0,  179,    2, 0x0a /* Public */,
      17,    1,  180,    2, 0x0a /* Public */,
      18,    0,  183,    2, 0x08 /* Private */,
      19,    0,  184,    2, 0x08 /* Private */,
      20,    0,  185,    2, 0x08 /* Private */,
      21,    0,  186,    2, 0x08 /* Private */,
      22,    0,  187,    2, 0x08 /* Private */,
      23,    0,  188,    2, 0x08 /* Private */,
      24,    0,  189,    2, 0x08 /* Private */,
      25,    0,  190,    2, 0x08 /* Private */,
      26,    0,  191,    2, 0x08 /* Private */,
      27,    0,  192,    2, 0x08 /* Private */,
      28,    0,  193,    2, 0x08 /* Private */,
      29,    0,  194,    2, 0x08 /* Private */,
      30,    0,  195,    2, 0x08 /* Private */,
      31,    0,  196,    2, 0x08 /* Private */,
      32,    0,  197,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ButtonTerminal_Clicked(); break;
        case 1: _t->ButtonAddition_Clicked(); break;
        case 2: _t->ButtonDefault_Clicked(); break;
        case 3: _t->ButtonOnline_Clicked(); break;
        case 4: _t->Button_7_Clicked(); break;
        case 5: _t->UpdatePaypass(); break;
        case 6: _t->UpdatePureEntry(); break;
        case 7: _t->UpdatePaywave(); break;
        case 8: _t->UpdateExpressPay(); break;
        case 9: _t->UpdateJCB(); break;
        case 10: _t->GroupButtonClick((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->Additional_Update(); break;
        case 12: _t->Terminal_Update(); break;
        case 13: _t->Default_Update(); break;
        case 14: _t->OpenNewWindow((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_toolButtonAddition_clicked(); break;
        case 16: _t->on_toolButtonDefault_clicked(); break;
        case 17: _t->on_toolButtonOnline_clicked(); break;
        case 18: _t->on_toolButton_7_clicked(); break;
        case 19: _t->on_checkBoxDefault_clicked(); break;
        case 20: _t->on_toolButtonTerminal_clicked(); break;
        case 21: _t->on_toolButtonAddProcessing_clicked(); break;
        case 22: _t->on_toolButtonDenialProcessing_clicked(); break;
        case 23: _t->on_toolButtonOnlineProcessing_clicked(); break;
        case 24: _t->on_toolButtonDefaultProcessing_2_clicked(); break;
        case 25: _t->on_toolButtonEntryUp_clicked(); break;
        case 26: _t->on_toolButtonEntryDown_clicked(); break;
        case 27: _t->on_toolButtonEntryAdd_clicked(); break;
        case 28: _t->on_toolButtonEntryRemove_clicked(); break;
        case 29: _t->on_ButtonEntrySave_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonTerminal_Clicked)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonAddition_Clicked)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonDefault_Clicked)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonOnline_Clicked)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::Button_7_Clicked)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::UpdatePaypass)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::UpdatePureEntry)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::UpdatePaywave)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::UpdateExpressPay)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::UpdateJCB)) {
                *result = 9;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::ButtonTerminal_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void MainWindow::ButtonAddition_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void MainWindow::ButtonDefault_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void MainWindow::ButtonOnline_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void MainWindow::Button_7_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void MainWindow::UpdatePaypass()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void MainWindow::UpdatePureEntry()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void MainWindow::UpdatePaywave()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void MainWindow::UpdateExpressPay()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void MainWindow::UpdateJCB()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
