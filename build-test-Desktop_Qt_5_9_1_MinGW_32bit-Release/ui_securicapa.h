/********************************************************************************
** Form generated from reading UI file 'securicapa.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SECURICAPA_H
#define UI_SECURICAPA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SecuriCapa
{
public:
    QLabel *label;
    QLineEdit *lineEdit;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_8;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *SecuriCapa)
    {
        if (SecuriCapa->objectName().isEmpty())
            SecuriCapa->setObjectName(QStringLiteral("SecuriCapa"));
        SecuriCapa->resize(235, 290);
        SecuriCapa->setStyleSheet(QLatin1String("#QDialog{\n"
"background-color: rgb(245, 245, 245);\n"
"}"));
        label = new QLabel(SecuriCapa);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 40, 101, 20));
        lineEdit = new QLineEdit(SecuriCapa);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(140, 40, 51, 20));
        lineEdit->setStyleSheet(QLatin1String("border: 1px solid black;\n"
"border-radius: 2px;"));
        checkBox = new QCheckBox(SecuriCapa);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(40, 210, 70, 17));
        checkBox_2 = new QCheckBox(SecuriCapa);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(40, 190, 70, 17));
        checkBox_3 = new QCheckBox(SecuriCapa);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(40, 170, 70, 17));
        checkBox_4 = new QCheckBox(SecuriCapa);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(40, 150, 70, 17));
        checkBox_5 = new QCheckBox(SecuriCapa);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(40, 126, 70, 21));
        checkBox_6 = new QCheckBox(SecuriCapa);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setGeometry(QRect(40, 110, 111, 17));
        checkBox_7 = new QCheckBox(SecuriCapa);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setGeometry(QRect(40, 90, 70, 17));
        checkBox_8 = new QCheckBox(SecuriCapa);
        checkBox_8->setObjectName(QStringLiteral("checkBox_8"));
        checkBox_8->setGeometry(QRect(40, 70, 70, 17));
        widget = new QWidget(SecuriCapa);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(30, 250, 158, 25));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QLatin1String("color: rgb(239, 239, 239);\n"
"background-color: rgb(43, 43, 43);"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setStyleSheet(QLatin1String("color: rgb(236, 236, 236);\n"
"background-color: rgb(44, 44, 44);"));

        horizontalLayout->addWidget(pushButton_2);


        retranslateUi(SecuriCapa);

        QMetaObject::connectSlotsByName(SecuriCapa);
    } // setupUi

    void retranslateUi(QDialog *SecuriCapa)
    {
        SecuriCapa->setWindowTitle(QApplication::translate("SecuriCapa", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("SecuriCapa", "Security Capabilities", Q_NULLPTR));
        checkBox->setText(QApplication::translate("SecuriCapa", "RFU", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("SecuriCapa", "RFU", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("SecuriCapa", "RFU", Q_NULLPTR));
        checkBox_4->setText(QApplication::translate("SecuriCapa", "CDA", Q_NULLPTR));
        checkBox_5->setText(QApplication::translate("SecuriCapa", "RFU", Q_NULLPTR));
        checkBox_6->setText(QApplication::translate("SecuriCapa", "Card Capture", Q_NULLPTR));
        checkBox_7->setText(QApplication::translate("SecuriCapa", "DDA", Q_NULLPTR));
        checkBox_8->setText(QApplication::translate("SecuriCapa", "SDA", Q_NULLPTR));
        pushButton->setText(QApplication::translate("SecuriCapa", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("SecuriCapa", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SecuriCapa: public Ui_SecuriCapa {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SECURICAPA_H
