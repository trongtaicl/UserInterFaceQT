/********************************************************************************
** Form generated from reading UI file 'default.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEFAULT_H
#define UI_DEFAULT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Default
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *checkBoxCDANotPerform;
    QCheckBox *checkBoxSDAFailed;
    QCheckBox *checkBoxICCData;
    QCheckBox *checkBoxCardOnTerminal;
    QCheckBox *checkBoxDDAFailed;
    QCheckBox *checkBoxCDAFailed;
    QCheckBox *checkBoxSDASelected;
    QCheckBox *checkBoxRFU1;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *checkBoxICCAndTerminal;
    QCheckBox *checkBoxExpired;
    QCheckBox *checkBoxApplication;
    QCheckBox *checkBoxRequested;
    QCheckBox *checkBoxNewCard;
    QCheckBox *checkBoxRFU2;
    QCheckBox *checkBoxRFU3;
    QCheckBox *checkBoxRFU4;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *checkBoxCVMNotSuccess;
    QCheckBox *checkBoxUnrecognized;
    QCheckBox *checkBoxPinTryLimit;
    QCheckBox *checkBoxPinEntryReq1;
    QCheckBox *checkBoxPINEntryReq2;
    QCheckBox *checkBoxOnlinePIN;
    QCheckBox *checkBoxRFU5;
    QCheckBox *checkBoxRFU6;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBoxTransactionExceed;
    QCheckBox *checkBoxLCOLExceed;
    QCheckBox *checkBoxUCOLExeed;
    QCheckBox *checkBoxTransactionSelected;
    QCheckBox *checkBoxMerchantForce;
    QCheckBox *checkBoxRFU7;
    QCheckBox *checkBoxRFU8;
    QCheckBox *checkBoxRFU9;
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBoxDefaultTDOL;
    QCheckBox *checkBoxIssuer;
    QCheckBox *checkBoxScriptBeforeAC;
    QCheckBox *checkBoxScriptAfterAC;
    QCheckBox *checkBoxRelayThreshold;
    QCheckBox *checkBoxRelayTime;
    QCheckBox *checkBoxRRP1;
    QCheckBox *checkBoxRRP2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *Default)
    {
        if (Default->objectName().isEmpty())
            Default->setObjectName(QStringLiteral("Default"));
        Default->resize(806, 472);
        Default->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        gridLayout = new QGridLayout(Default);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(Default);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        lineEdit = new QLineEdit(Default);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy);
        lineEdit->setStyleSheet(QStringLiteral("border: 1px solid black;"));

        horizontalLayout_2->addWidget(lineEdit);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_6->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        checkBoxCDANotPerform = new QCheckBox(Default);
        checkBoxCDANotPerform->setObjectName(QStringLiteral("checkBoxCDANotPerform"));

        verticalLayout_5->addWidget(checkBoxCDANotPerform);

        checkBoxSDAFailed = new QCheckBox(Default);
        checkBoxSDAFailed->setObjectName(QStringLiteral("checkBoxSDAFailed"));

        verticalLayout_5->addWidget(checkBoxSDAFailed);

        checkBoxICCData = new QCheckBox(Default);
        checkBoxICCData->setObjectName(QStringLiteral("checkBoxICCData"));

        verticalLayout_5->addWidget(checkBoxICCData);

        checkBoxCardOnTerminal = new QCheckBox(Default);
        checkBoxCardOnTerminal->setObjectName(QStringLiteral("checkBoxCardOnTerminal"));

        verticalLayout_5->addWidget(checkBoxCardOnTerminal);

        checkBoxDDAFailed = new QCheckBox(Default);
        checkBoxDDAFailed->setObjectName(QStringLiteral("checkBoxDDAFailed"));

        verticalLayout_5->addWidget(checkBoxDDAFailed);

        checkBoxCDAFailed = new QCheckBox(Default);
        checkBoxCDAFailed->setObjectName(QStringLiteral("checkBoxCDAFailed"));

        verticalLayout_5->addWidget(checkBoxCDAFailed);

        checkBoxSDASelected = new QCheckBox(Default);
        checkBoxSDASelected->setObjectName(QStringLiteral("checkBoxSDASelected"));

        verticalLayout_5->addWidget(checkBoxSDASelected);

        checkBoxRFU1 = new QCheckBox(Default);
        checkBoxRFU1->setObjectName(QStringLiteral("checkBoxRFU1"));

        verticalLayout_5->addWidget(checkBoxRFU1);


        horizontalLayout_3->addLayout(verticalLayout_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        checkBoxICCAndTerminal = new QCheckBox(Default);
        checkBoxICCAndTerminal->setObjectName(QStringLiteral("checkBoxICCAndTerminal"));

        verticalLayout_4->addWidget(checkBoxICCAndTerminal);

        checkBoxExpired = new QCheckBox(Default);
        checkBoxExpired->setObjectName(QStringLiteral("checkBoxExpired"));

        verticalLayout_4->addWidget(checkBoxExpired);

        checkBoxApplication = new QCheckBox(Default);
        checkBoxApplication->setObjectName(QStringLiteral("checkBoxApplication"));

        verticalLayout_4->addWidget(checkBoxApplication);

        checkBoxRequested = new QCheckBox(Default);
        checkBoxRequested->setObjectName(QStringLiteral("checkBoxRequested"));

        verticalLayout_4->addWidget(checkBoxRequested);

        checkBoxNewCard = new QCheckBox(Default);
        checkBoxNewCard->setObjectName(QStringLiteral("checkBoxNewCard"));

        verticalLayout_4->addWidget(checkBoxNewCard);

        checkBoxRFU2 = new QCheckBox(Default);
        checkBoxRFU2->setObjectName(QStringLiteral("checkBoxRFU2"));

        verticalLayout_4->addWidget(checkBoxRFU2);

        checkBoxRFU3 = new QCheckBox(Default);
        checkBoxRFU3->setObjectName(QStringLiteral("checkBoxRFU3"));

        verticalLayout_4->addWidget(checkBoxRFU3);

        checkBoxRFU4 = new QCheckBox(Default);
        checkBoxRFU4->setObjectName(QStringLiteral("checkBoxRFU4"));

        verticalLayout_4->addWidget(checkBoxRFU4);


        horizontalLayout_3->addLayout(verticalLayout_4);


        verticalLayout_6->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        checkBoxCVMNotSuccess = new QCheckBox(Default);
        checkBoxCVMNotSuccess->setObjectName(QStringLiteral("checkBoxCVMNotSuccess"));

        verticalLayout_3->addWidget(checkBoxCVMNotSuccess);

        checkBoxUnrecognized = new QCheckBox(Default);
        checkBoxUnrecognized->setObjectName(QStringLiteral("checkBoxUnrecognized"));

        verticalLayout_3->addWidget(checkBoxUnrecognized);

        checkBoxPinTryLimit = new QCheckBox(Default);
        checkBoxPinTryLimit->setObjectName(QStringLiteral("checkBoxPinTryLimit"));

        verticalLayout_3->addWidget(checkBoxPinTryLimit);

        checkBoxPinEntryReq1 = new QCheckBox(Default);
        checkBoxPinEntryReq1->setObjectName(QStringLiteral("checkBoxPinEntryReq1"));

        verticalLayout_3->addWidget(checkBoxPinEntryReq1);

        checkBoxPINEntryReq2 = new QCheckBox(Default);
        checkBoxPINEntryReq2->setObjectName(QStringLiteral("checkBoxPINEntryReq2"));

        verticalLayout_3->addWidget(checkBoxPINEntryReq2);

        checkBoxOnlinePIN = new QCheckBox(Default);
        checkBoxOnlinePIN->setObjectName(QStringLiteral("checkBoxOnlinePIN"));

        verticalLayout_3->addWidget(checkBoxOnlinePIN);

        checkBoxRFU5 = new QCheckBox(Default);
        checkBoxRFU5->setObjectName(QStringLiteral("checkBoxRFU5"));

        verticalLayout_3->addWidget(checkBoxRFU5);

        checkBoxRFU6 = new QCheckBox(Default);
        checkBoxRFU6->setObjectName(QStringLiteral("checkBoxRFU6"));

        verticalLayout_3->addWidget(checkBoxRFU6);


        horizontalLayout_4->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        checkBoxTransactionExceed = new QCheckBox(Default);
        checkBoxTransactionExceed->setObjectName(QStringLiteral("checkBoxTransactionExceed"));

        verticalLayout_2->addWidget(checkBoxTransactionExceed);

        checkBoxLCOLExceed = new QCheckBox(Default);
        checkBoxLCOLExceed->setObjectName(QStringLiteral("checkBoxLCOLExceed"));

        verticalLayout_2->addWidget(checkBoxLCOLExceed);

        checkBoxUCOLExeed = new QCheckBox(Default);
        checkBoxUCOLExeed->setObjectName(QStringLiteral("checkBoxUCOLExeed"));

        verticalLayout_2->addWidget(checkBoxUCOLExeed);

        checkBoxTransactionSelected = new QCheckBox(Default);
        checkBoxTransactionSelected->setObjectName(QStringLiteral("checkBoxTransactionSelected"));

        verticalLayout_2->addWidget(checkBoxTransactionSelected);

        checkBoxMerchantForce = new QCheckBox(Default);
        checkBoxMerchantForce->setObjectName(QStringLiteral("checkBoxMerchantForce"));

        verticalLayout_2->addWidget(checkBoxMerchantForce);

        checkBoxRFU7 = new QCheckBox(Default);
        checkBoxRFU7->setObjectName(QStringLiteral("checkBoxRFU7"));

        verticalLayout_2->addWidget(checkBoxRFU7);

        checkBoxRFU8 = new QCheckBox(Default);
        checkBoxRFU8->setObjectName(QStringLiteral("checkBoxRFU8"));

        verticalLayout_2->addWidget(checkBoxRFU8);

        checkBoxRFU9 = new QCheckBox(Default);
        checkBoxRFU9->setObjectName(QStringLiteral("checkBoxRFU9"));

        verticalLayout_2->addWidget(checkBoxRFU9);


        horizontalLayout_4->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        checkBoxDefaultTDOL = new QCheckBox(Default);
        checkBoxDefaultTDOL->setObjectName(QStringLiteral("checkBoxDefaultTDOL"));

        verticalLayout->addWidget(checkBoxDefaultTDOL);

        checkBoxIssuer = new QCheckBox(Default);
        checkBoxIssuer->setObjectName(QStringLiteral("checkBoxIssuer"));

        verticalLayout->addWidget(checkBoxIssuer);

        checkBoxScriptBeforeAC = new QCheckBox(Default);
        checkBoxScriptBeforeAC->setObjectName(QStringLiteral("checkBoxScriptBeforeAC"));

        verticalLayout->addWidget(checkBoxScriptBeforeAC);

        checkBoxScriptAfterAC = new QCheckBox(Default);
        checkBoxScriptAfterAC->setObjectName(QStringLiteral("checkBoxScriptAfterAC"));

        verticalLayout->addWidget(checkBoxScriptAfterAC);

        checkBoxRelayThreshold = new QCheckBox(Default);
        checkBoxRelayThreshold->setObjectName(QStringLiteral("checkBoxRelayThreshold"));

        verticalLayout->addWidget(checkBoxRelayThreshold);

        checkBoxRelayTime = new QCheckBox(Default);
        checkBoxRelayTime->setObjectName(QStringLiteral("checkBoxRelayTime"));

        verticalLayout->addWidget(checkBoxRelayTime);

        checkBoxRRP1 = new QCheckBox(Default);
        checkBoxRRP1->setObjectName(QStringLiteral("checkBoxRRP1"));

        verticalLayout->addWidget(checkBoxRRP1);

        checkBoxRRP2 = new QCheckBox(Default);
        checkBoxRRP2->setObjectName(QStringLiteral("checkBoxRRP2"));

        verticalLayout->addWidget(checkBoxRRP2);


        horizontalLayout_4->addLayout(verticalLayout);


        verticalLayout_6->addLayout(horizontalLayout_4);


        gridLayout->addLayout(verticalLayout_6, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButtonOK = new QPushButton(Default);
        pushButtonOK->setObjectName(QStringLiteral("pushButtonOK"));

        horizontalLayout->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(Default);
        pushButtonCancel->setObjectName(QStringLiteral("pushButtonCancel"));

        horizontalLayout->addWidget(pushButtonCancel);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);


        retranslateUi(Default);

        QMetaObject::connectSlotsByName(Default);
    } // setupUi

    void retranslateUi(QDialog *Default)
    {
        Default->setWindowTitle(QApplication::translate("Default", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("Default", "Transaction Verification Result", Q_NULLPTR));
        checkBoxCDANotPerform->setText(QApplication::translate("Default", "CDA not performed", Q_NULLPTR));
        checkBoxSDAFailed->setText(QApplication::translate("Default", "SDA failed", Q_NULLPTR));
        checkBoxICCData->setText(QApplication::translate("Default", "ICC data missing", Q_NULLPTR));
        checkBoxCardOnTerminal->setText(QApplication::translate("Default", "Card on terminal exception file", Q_NULLPTR));
        checkBoxDDAFailed->setText(QApplication::translate("Default", "DDA failed", Q_NULLPTR));
        checkBoxCDAFailed->setText(QApplication::translate("Default", "CDA failed", Q_NULLPTR));
        checkBoxSDASelected->setText(QApplication::translate("Default", "SDA selected", Q_NULLPTR));
        checkBoxRFU1->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxICCAndTerminal->setText(QApplication::translate("Default", "ICC and terminal have different application versions", Q_NULLPTR));
        checkBoxExpired->setText(QApplication::translate("Default", "Expired application", Q_NULLPTR));
        checkBoxApplication->setText(QApplication::translate("Default", "Application not yet effective", Q_NULLPTR));
        checkBoxRequested->setText(QApplication::translate("Default", "Requested service not allowed", Q_NULLPTR));
        checkBoxNewCard->setText(QApplication::translate("Default", "New card", Q_NULLPTR));
        checkBoxRFU2->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxRFU3->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxRFU4->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxCVMNotSuccess->setText(QApplication::translate("Default", "CVM not sucessful", Q_NULLPTR));
        checkBoxUnrecognized->setText(QApplication::translate("Default", "Unrecognized CVM", Q_NULLPTR));
        checkBoxPinTryLimit->setText(QApplication::translate("Default", "PIN try limit exceeded", Q_NULLPTR));
        checkBoxPinEntryReq1->setText(QApplication::translate("Default", "PIN entry req., PIN pad not present or not working", Q_NULLPTR));
        checkBoxPINEntryReq2->setText(QApplication::translate("Default", "PIN entry req., PIN pad present but PIN not entered", Q_NULLPTR));
        checkBoxOnlinePIN->setText(QApplication::translate("Default", "Online PIN entered", Q_NULLPTR));
        checkBoxRFU5->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxRFU6->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxTransactionExceed->setText(QApplication::translate("Default", "Transaction exceed floor limit", Q_NULLPTR));
        checkBoxLCOLExceed->setText(QApplication::translate("Default", "LCOL exceeded", Q_NULLPTR));
        checkBoxUCOLExeed->setText(QApplication::translate("Default", "UCOL exceeded", Q_NULLPTR));
        checkBoxTransactionSelected->setText(QApplication::translate("Default", "Transaction selected randomly", Q_NULLPTR));
        checkBoxMerchantForce->setText(QApplication::translate("Default", "Merchant forced transaction online", Q_NULLPTR));
        checkBoxRFU7->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxRFU8->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxRFU9->setText(QApplication::translate("Default", "RFU", Q_NULLPTR));
        checkBoxDefaultTDOL->setText(QApplication::translate("Default", "Default TDOL used", Q_NULLPTR));
        checkBoxIssuer->setText(QApplication::translate("Default", "Issuer authentication failed", Q_NULLPTR));
        checkBoxScriptBeforeAC->setText(QApplication::translate("Default", "script failed before GenAC", Q_NULLPTR));
        checkBoxScriptAfterAC->setText(QApplication::translate("Default", "Script failed after Gen AC", Q_NULLPTR));
        checkBoxRelayThreshold->setText(QApplication::translate("Default", "Relay resistance threshold exceeded", Q_NULLPTR));
        checkBoxRelayTime->setText(QApplication::translate("Default", "Relay resistance time limits exceeded", Q_NULLPTR));
        checkBoxRRP1->setText(QApplication::translate("Default", "Relay resistance performed", Q_NULLPTR));
        checkBoxRRP2->setText(QApplication::translate("Default", "Relay resistance performed", Q_NULLPTR));
        pushButtonOK->setText(QApplication::translate("Default", "OK", Q_NULLPTR));
        pushButtonCancel->setText(QApplication::translate("Default", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Default: public Ui_Default {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEFAULT_H
