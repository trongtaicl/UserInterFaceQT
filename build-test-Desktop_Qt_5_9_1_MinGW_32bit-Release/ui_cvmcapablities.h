/********************************************************************************
** Form generated from reading UI file 'cvmcapablities.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CVMCAPABLITIES_H
#define UI_CVMCAPABLITIES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CVMCapablities
{
public:
    QLabel *label;
    QLineEdit *lineEdit;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_8;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *CVMCapablities)
    {
        if (CVMCapablities->objectName().isEmpty())
            CVMCapablities->setObjectName(QStringLiteral("CVMCapablities"));
        CVMCapablities->resize(346, 263);
        CVMCapablities->setStyleSheet(QLatin1String("#QDialog{\n"
"background-color: rgb(240, 240, 240);\n"
"}"));
        label = new QLabel(CVMCapablities);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 47, 13));
        lineEdit = new QLineEdit(CVMCapablities);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(80, 20, 51, 20));
        lineEdit->setStyleSheet(QLatin1String("border: 1px solid black;\n"
"border-radius: 2px"));
        checkBox = new QCheckBox(CVMCapablities);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(20, 190, 70, 17));
        checkBox_2 = new QCheckBox(CVMCapablities);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(20, 170, 70, 17));
        checkBox_3 = new QCheckBox(CVMCapablities);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(20, 150, 70, 17));
        checkBox_4 = new QCheckBox(CVMCapablities);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(20, 130, 181, 17));
        checkBox_5 = new QCheckBox(CVMCapablities);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(20, 110, 211, 17));
        checkBox_6 = new QCheckBox(CVMCapablities);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setGeometry(QRect(20, 90, 191, 17));
        checkBox_7 = new QCheckBox(CVMCapablities);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setGeometry(QRect(20, 70, 211, 17));
        checkBox_8 = new QCheckBox(CVMCapablities);
        checkBox_8->setObjectName(QStringLiteral("checkBox_8"));
        checkBox_8->setGeometry(QRect(20, 50, 70, 17));
        widget = new QWidget(CVMCapablities);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(160, 220, 158, 25));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QLatin1String("color: rgb(248, 248, 248);\n"
"background-color: rgb(48, 48, 48);"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setStyleSheet(QLatin1String("color: rgb(243, 243, 243);\n"
"background-color: rgb(67, 67, 67);"));

        horizontalLayout->addWidget(pushButton_2);


        retranslateUi(CVMCapablities);

        QMetaObject::connectSlotsByName(CVMCapablities);
    } // setupUi

    void retranslateUi(QDialog *CVMCapablities)
    {
        CVMCapablities->setWindowTitle(QApplication::translate("CVMCapablities", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("CVMCapablities", "TextLabel", Q_NULLPTR));
        checkBox->setText(QApplication::translate("CVMCapablities", "RFU", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("CVMCapablities", "RFU", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("CVMCapablities", "RFU", Q_NULLPTR));
        checkBox_4->setText(QApplication::translate("CVMCapablities", "No CVM required", Q_NULLPTR));
        checkBox_5->setText(QApplication::translate("CVMCapablities", "Enciphered PIN for offline verification", Q_NULLPTR));
        checkBox_6->setText(QApplication::translate("CVMCapablities", "Signature (paper)", Q_NULLPTR));
        checkBox_7->setText(QApplication::translate("CVMCapablities", "Enciphered PIN for online verification", Q_NULLPTR));
        checkBox_8->setText(QApplication::translate("CVMCapablities", "Plaintext PIN for ICC verification", Q_NULLPTR));
        pushButton->setText(QApplication::translate("CVMCapablities", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("CVMCapablities", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CVMCapablities: public Ui_CVMCapablities {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CVMCAPABLITIES_H
