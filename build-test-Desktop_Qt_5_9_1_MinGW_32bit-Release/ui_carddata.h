/********************************************************************************
** Form generated from reading UI file 'carddata.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CARDDATA_H
#define UI_CARDDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_carddata
{
public:
    QLabel *label;
    QLineEdit *lineEdit;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_8;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *carddata)
    {
        if (carddata->objectName().isEmpty())
            carddata->setObjectName(QStringLiteral("carddata"));
        carddata->resize(321, 300);
        carddata->setStyleSheet(QLatin1String("#QDialog{\n"
"background-color: rgb(247, 247, 247);\n"
"}"));
        label = new QLabel(carddata);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 141, 21));
        lineEdit = new QLineEdit(carddata);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(160, 20, 61, 20));
        lineEdit->setStyleSheet(QLatin1String("border: 1px solid black; \n"
"border-radius: 2px;"));
        checkBox = new QCheckBox(carddata);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(20, 200, 161, 17));
        checkBox_2 = new QCheckBox(carddata);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(20, 180, 171, 17));
        checkBox_3 = new QCheckBox(carddata);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(20, 160, 171, 17));
        checkBox_4 = new QCheckBox(carddata);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(20, 140, 181, 17));
        checkBox_5 = new QCheckBox(carddata);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(20, 120, 181, 17));
        checkBox_6 = new QCheckBox(carddata);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setGeometry(QRect(20, 100, 181, 17));
        checkBox_7 = new QCheckBox(carddata);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setGeometry(QRect(20, 80, 181, 17));
        checkBox_8 = new QCheckBox(carddata);
        checkBox_8->setObjectName(QStringLiteral("checkBox_8"));
        checkBox_8->setGeometry(QRect(20, 60, 191, 17));
        widget = new QWidget(carddata);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(150, 250, 158, 25));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 0);\n"
"color: rgb(238, 238, 238);"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 0);\n"
"color: rgb(238, 238, 238);"));

        horizontalLayout->addWidget(pushButton_2);


        retranslateUi(carddata);

        QMetaObject::connectSlotsByName(carddata);
    } // setupUi

    void retranslateUi(QDialog *carddata)
    {
        carddata->setWindowTitle(QApplication::translate("carddata", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("carddata", "Card Data Input Capabilities", Q_NULLPTR));
        checkBox->setText(QApplication::translate("carddata", "RFU", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("carddata", "RFU", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("carddata", "RFU", Q_NULLPTR));
        checkBox_4->setText(QApplication::translate("carddata", "RFU", Q_NULLPTR));
        checkBox_5->setText(QApplication::translate("carddata", "RFU", Q_NULLPTR));
        checkBox_6->setText(QApplication::translate("carddata", "ICC with contacts", Q_NULLPTR));
        checkBox_7->setText(QApplication::translate("carddata", "Magnetic Stripe", Q_NULLPTR));
        checkBox_8->setText(QApplication::translate("carddata", "Manual Key Entry", Q_NULLPTR));
        pushButton->setText(QApplication::translate("carddata", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("carddata", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class carddata: public Ui_carddata {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CARDDATA_H
