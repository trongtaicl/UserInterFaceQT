/********************************************************************************
** Form generated from reading UI file 'additionalterminal.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDITIONALTERMINAL_H
#define UI_ADDITIONALTERMINAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AdditionalTerminal
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QLineEdit *lineEdit;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *checkBoxCash;
    QCheckBox *checkBoxGoods;
    QCheckBox *checkBoxService;
    QCheckBox *checkBoxCashback;
    QCheckBox *checkBoxInquiry;
    QCheckBox *checkBoxTransfer;
    QCheckBox *checkBoxPayment;
    QCheckBox *checkBoxAdmin;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *checkBoxCashDepo;
    QCheckBox *checkBoxRFU1;
    QCheckBox *checkBoxRFU2;
    QCheckBox *checkBoxRFU3;
    QCheckBox *checkBoxRFU4;
    QCheckBox *checkBoxRFU5;
    QCheckBox *checkBoxRFU6;
    QCheckBox *checkBoxRFU7;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *checkBoxNumeric;
    QCheckBox *checkBoxAlpha;
    QCheckBox *checkBoxCommand;
    QCheckBox *checkBoxFunction;
    QCheckBox *checkBoxRFU8;
    QCheckBox *checkBoxRFU9;
    QCheckBox *checkBoxRFU10;
    QCheckBox *checkBoxRFU11;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBoxPrint;
    QCheckBox *checkBoxCardholder;
    QCheckBox *checkBoxAttendant;
    QCheckBox *checkBoxDisplayCard;
    QCheckBox *checkBoxRFU12;
    QCheckBox *checkBoxRFU13;
    QCheckBox *checkBoxCode10;
    QCheckBox *checkBoxCode9;
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBoxCode8;
    QCheckBox *checkBoxCode7;
    QCheckBox *checkBoxCode6;
    QCheckBox *checkBoxCode5;
    QCheckBox *checkBoxCod4;
    QCheckBox *checkBoxCode3;
    QCheckBox *checkBoxCode2;
    QCheckBox *checkBoxCode1;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *AdditionalTerminal)
    {
        if (AdditionalTerminal->objectName().isEmpty())
            AdditionalTerminal->setObjectName(QStringLiteral("AdditionalTerminal"));
        AdditionalTerminal->resize(560, 476);
        AdditionalTerminal->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        gridLayout = new QGridLayout(AdditionalTerminal);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(AdditionalTerminal);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_4->addWidget(label);

        lineEdit = new QLineEdit(AdditionalTerminal);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setStyleSheet(QStringLiteral("border: 1px solid black;"));

        horizontalLayout_4->addWidget(lineEdit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);


        verticalLayout_6->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        checkBoxCash = new QCheckBox(AdditionalTerminal);
        checkBoxCash->setObjectName(QStringLiteral("checkBoxCash"));

        verticalLayout_5->addWidget(checkBoxCash);

        checkBoxGoods = new QCheckBox(AdditionalTerminal);
        checkBoxGoods->setObjectName(QStringLiteral("checkBoxGoods"));

        verticalLayout_5->addWidget(checkBoxGoods);

        checkBoxService = new QCheckBox(AdditionalTerminal);
        checkBoxService->setObjectName(QStringLiteral("checkBoxService"));

        verticalLayout_5->addWidget(checkBoxService);

        checkBoxCashback = new QCheckBox(AdditionalTerminal);
        checkBoxCashback->setObjectName(QStringLiteral("checkBoxCashback"));

        verticalLayout_5->addWidget(checkBoxCashback);

        checkBoxInquiry = new QCheckBox(AdditionalTerminal);
        checkBoxInquiry->setObjectName(QStringLiteral("checkBoxInquiry"));

        verticalLayout_5->addWidget(checkBoxInquiry);

        checkBoxTransfer = new QCheckBox(AdditionalTerminal);
        checkBoxTransfer->setObjectName(QStringLiteral("checkBoxTransfer"));

        verticalLayout_5->addWidget(checkBoxTransfer);

        checkBoxPayment = new QCheckBox(AdditionalTerminal);
        checkBoxPayment->setObjectName(QStringLiteral("checkBoxPayment"));

        verticalLayout_5->addWidget(checkBoxPayment);

        checkBoxAdmin = new QCheckBox(AdditionalTerminal);
        checkBoxAdmin->setObjectName(QStringLiteral("checkBoxAdmin"));

        verticalLayout_5->addWidget(checkBoxAdmin);


        horizontalLayout_2->addLayout(verticalLayout_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        checkBoxCashDepo = new QCheckBox(AdditionalTerminal);
        checkBoxCashDepo->setObjectName(QStringLiteral("checkBoxCashDepo"));

        verticalLayout_4->addWidget(checkBoxCashDepo);

        checkBoxRFU1 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU1->setObjectName(QStringLiteral("checkBoxRFU1"));

        verticalLayout_4->addWidget(checkBoxRFU1);

        checkBoxRFU2 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU2->setObjectName(QStringLiteral("checkBoxRFU2"));

        verticalLayout_4->addWidget(checkBoxRFU2);

        checkBoxRFU3 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU3->setObjectName(QStringLiteral("checkBoxRFU3"));

        verticalLayout_4->addWidget(checkBoxRFU3);

        checkBoxRFU4 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU4->setObjectName(QStringLiteral("checkBoxRFU4"));

        verticalLayout_4->addWidget(checkBoxRFU4);

        checkBoxRFU5 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU5->setObjectName(QStringLiteral("checkBoxRFU5"));

        verticalLayout_4->addWidget(checkBoxRFU5);

        checkBoxRFU6 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU6->setObjectName(QStringLiteral("checkBoxRFU6"));

        verticalLayout_4->addWidget(checkBoxRFU6);

        checkBoxRFU7 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU7->setObjectName(QStringLiteral("checkBoxRFU7"));

        verticalLayout_4->addWidget(checkBoxRFU7);


        horizontalLayout_2->addLayout(verticalLayout_4);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        checkBoxNumeric = new QCheckBox(AdditionalTerminal);
        checkBoxNumeric->setObjectName(QStringLiteral("checkBoxNumeric"));

        verticalLayout_3->addWidget(checkBoxNumeric);

        checkBoxAlpha = new QCheckBox(AdditionalTerminal);
        checkBoxAlpha->setObjectName(QStringLiteral("checkBoxAlpha"));

        verticalLayout_3->addWidget(checkBoxAlpha);

        checkBoxCommand = new QCheckBox(AdditionalTerminal);
        checkBoxCommand->setObjectName(QStringLiteral("checkBoxCommand"));

        verticalLayout_3->addWidget(checkBoxCommand);

        checkBoxFunction = new QCheckBox(AdditionalTerminal);
        checkBoxFunction->setObjectName(QStringLiteral("checkBoxFunction"));

        verticalLayout_3->addWidget(checkBoxFunction);

        checkBoxRFU8 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU8->setObjectName(QStringLiteral("checkBoxRFU8"));

        verticalLayout_3->addWidget(checkBoxRFU8);

        checkBoxRFU9 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU9->setObjectName(QStringLiteral("checkBoxRFU9"));

        verticalLayout_3->addWidget(checkBoxRFU9);

        checkBoxRFU10 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU10->setObjectName(QStringLiteral("checkBoxRFU10"));

        verticalLayout_3->addWidget(checkBoxRFU10);

        checkBoxRFU11 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU11->setObjectName(QStringLiteral("checkBoxRFU11"));

        verticalLayout_3->addWidget(checkBoxRFU11);


        horizontalLayout_2->addLayout(verticalLayout_3);


        verticalLayout_6->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        checkBoxPrint = new QCheckBox(AdditionalTerminal);
        checkBoxPrint->setObjectName(QStringLiteral("checkBoxPrint"));

        verticalLayout_2->addWidget(checkBoxPrint);

        checkBoxCardholder = new QCheckBox(AdditionalTerminal);
        checkBoxCardholder->setObjectName(QStringLiteral("checkBoxCardholder"));

        verticalLayout_2->addWidget(checkBoxCardholder);

        checkBoxAttendant = new QCheckBox(AdditionalTerminal);
        checkBoxAttendant->setObjectName(QStringLiteral("checkBoxAttendant"));

        verticalLayout_2->addWidget(checkBoxAttendant);

        checkBoxDisplayCard = new QCheckBox(AdditionalTerminal);
        checkBoxDisplayCard->setObjectName(QStringLiteral("checkBoxDisplayCard"));

        verticalLayout_2->addWidget(checkBoxDisplayCard);

        checkBoxRFU12 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU12->setObjectName(QStringLiteral("checkBoxRFU12"));

        verticalLayout_2->addWidget(checkBoxRFU12);

        checkBoxRFU13 = new QCheckBox(AdditionalTerminal);
        checkBoxRFU13->setObjectName(QStringLiteral("checkBoxRFU13"));

        verticalLayout_2->addWidget(checkBoxRFU13);

        checkBoxCode10 = new QCheckBox(AdditionalTerminal);
        checkBoxCode10->setObjectName(QStringLiteral("checkBoxCode10"));

        verticalLayout_2->addWidget(checkBoxCode10);

        checkBoxCode9 = new QCheckBox(AdditionalTerminal);
        checkBoxCode9->setObjectName(QStringLiteral("checkBoxCode9"));

        verticalLayout_2->addWidget(checkBoxCode9);


        horizontalLayout_3->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        checkBoxCode8 = new QCheckBox(AdditionalTerminal);
        checkBoxCode8->setObjectName(QStringLiteral("checkBoxCode8"));

        verticalLayout->addWidget(checkBoxCode8);

        checkBoxCode7 = new QCheckBox(AdditionalTerminal);
        checkBoxCode7->setObjectName(QStringLiteral("checkBoxCode7"));

        verticalLayout->addWidget(checkBoxCode7);

        checkBoxCode6 = new QCheckBox(AdditionalTerminal);
        checkBoxCode6->setObjectName(QStringLiteral("checkBoxCode6"));

        verticalLayout->addWidget(checkBoxCode6);

        checkBoxCode5 = new QCheckBox(AdditionalTerminal);
        checkBoxCode5->setObjectName(QStringLiteral("checkBoxCode5"));

        verticalLayout->addWidget(checkBoxCode5);

        checkBoxCod4 = new QCheckBox(AdditionalTerminal);
        checkBoxCod4->setObjectName(QStringLiteral("checkBoxCod4"));

        verticalLayout->addWidget(checkBoxCod4);

        checkBoxCode3 = new QCheckBox(AdditionalTerminal);
        checkBoxCode3->setObjectName(QStringLiteral("checkBoxCode3"));

        verticalLayout->addWidget(checkBoxCode3);

        checkBoxCode2 = new QCheckBox(AdditionalTerminal);
        checkBoxCode2->setObjectName(QStringLiteral("checkBoxCode2"));

        verticalLayout->addWidget(checkBoxCode2);

        checkBoxCode1 = new QCheckBox(AdditionalTerminal);
        checkBoxCode1->setObjectName(QStringLiteral("checkBoxCode1"));

        verticalLayout->addWidget(checkBoxCode1);


        horizontalLayout_3->addLayout(verticalLayout);


        verticalLayout_6->addLayout(horizontalLayout_3);


        gridLayout->addLayout(verticalLayout_6, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        pushButtonOK = new QPushButton(AdditionalTerminal);
        pushButtonOK->setObjectName(QStringLiteral("pushButtonOK"));

        horizontalLayout->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(AdditionalTerminal);
        pushButtonCancel->setObjectName(QStringLiteral("pushButtonCancel"));

        horizontalLayout->addWidget(pushButtonCancel);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);


        retranslateUi(AdditionalTerminal);

        QMetaObject::connectSlotsByName(AdditionalTerminal);
    } // setupUi

    void retranslateUi(QDialog *AdditionalTerminal)
    {
        AdditionalTerminal->setWindowTitle(QApplication::translate("AdditionalTerminal", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("AdditionalTerminal", "Additional Terminal Capabilities", Q_NULLPTR));
        checkBoxCash->setText(QApplication::translate("AdditionalTerminal", "Cash", Q_NULLPTR));
        checkBoxGoods->setText(QApplication::translate("AdditionalTerminal", "Goods", Q_NULLPTR));
        checkBoxService->setText(QApplication::translate("AdditionalTerminal", "Services", Q_NULLPTR));
        checkBoxCashback->setText(QApplication::translate("AdditionalTerminal", "Cashback", Q_NULLPTR));
        checkBoxInquiry->setText(QApplication::translate("AdditionalTerminal", "Inquiry", Q_NULLPTR));
        checkBoxTransfer->setText(QApplication::translate("AdditionalTerminal", "Transfer", Q_NULLPTR));
        checkBoxPayment->setText(QApplication::translate("AdditionalTerminal", "Payment", Q_NULLPTR));
        checkBoxAdmin->setText(QApplication::translate("AdditionalTerminal", "Administrative", Q_NULLPTR));
        checkBoxCashDepo->setText(QApplication::translate("AdditionalTerminal", "Cash Deposit", Q_NULLPTR));
        checkBoxRFU1->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU2->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU3->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU4->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU5->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU6->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU7->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxNumeric->setText(QApplication::translate("AdditionalTerminal", "Numeric keys", Q_NULLPTR));
        checkBoxAlpha->setText(QApplication::translate("AdditionalTerminal", "Alphabetical and special character keys", Q_NULLPTR));
        checkBoxCommand->setText(QApplication::translate("AdditionalTerminal", "Command keys", Q_NULLPTR));
        checkBoxFunction->setText(QApplication::translate("AdditionalTerminal", "Function keys", Q_NULLPTR));
        checkBoxRFU8->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU9->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU10->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU11->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxPrint->setText(QApplication::translate("AdditionalTerminal", "Print, attendant", Q_NULLPTR));
        checkBoxCardholder->setText(QApplication::translate("AdditionalTerminal", "Print, cardholder", Q_NULLPTR));
        checkBoxAttendant->setText(QApplication::translate("AdditionalTerminal", "Display, attendant", Q_NULLPTR));
        checkBoxDisplayCard->setText(QApplication::translate("AdditionalTerminal", "Display, cardholder", Q_NULLPTR));
        checkBoxRFU12->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxRFU13->setText(QApplication::translate("AdditionalTerminal", "RFU", Q_NULLPTR));
        checkBoxCode10->setText(QApplication::translate("AdditionalTerminal", "Code table 10", Q_NULLPTR));
        checkBoxCode9->setText(QApplication::translate("AdditionalTerminal", "Code table 9", Q_NULLPTR));
        checkBoxCode8->setText(QApplication::translate("AdditionalTerminal", "Code table 8", Q_NULLPTR));
        checkBoxCode7->setText(QApplication::translate("AdditionalTerminal", "Code table 7", Q_NULLPTR));
        checkBoxCode6->setText(QApplication::translate("AdditionalTerminal", "Code table 6", Q_NULLPTR));
        checkBoxCode5->setText(QApplication::translate("AdditionalTerminal", "Code table 5", Q_NULLPTR));
        checkBoxCod4->setText(QApplication::translate("AdditionalTerminal", "Code table 4", Q_NULLPTR));
        checkBoxCode3->setText(QApplication::translate("AdditionalTerminal", "Code table 3", Q_NULLPTR));
        checkBoxCode2->setText(QApplication::translate("AdditionalTerminal", "Code table 2", Q_NULLPTR));
        checkBoxCode1->setText(QApplication::translate("AdditionalTerminal", "Code table 1", Q_NULLPTR));
        pushButtonOK->setText(QApplication::translate("AdditionalTerminal", "OK", Q_NULLPTR));
        pushButtonCancel->setText(QApplication::translate("AdditionalTerminal", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AdditionalTerminal: public Ui_AdditionalTerminal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDITIONALTERMINAL_H
