/********************************************************************************
** Form generated from reading UI file 'securitycapabilities.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SECURITYCAPABILITIES_H
#define UI_SECURITYCAPABILITIES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SecurityCapabilities
{
public:
    QLabel *label;
    QLineEdit *lineEdit;
    QCheckBox *checkBox_0;
    QCheckBox *checkBox_1;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_7;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *SecurityCapabilities)
    {
        if (SecurityCapabilities->objectName().isEmpty())
            SecurityCapabilities->setObjectName(QStringLiteral("SecurityCapabilities"));
        SecurityCapabilities->resize(395, 357);
        SecurityCapabilities->setStyleSheet(QLatin1String("#QDialog{\n"
"background-color: rgb(247, 247, 247);\n"
"}"));
        label = new QLabel(SecurityCapabilities);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 20, 101, 16));
        lineEdit = new QLineEdit(SecurityCapabilities);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(140, 20, 61, 20));
        lineEdit->setStyleSheet(QLatin1String("border: 1px solid black;\n"
"border-radius: 2px"));
        checkBox_0 = new QCheckBox(SecurityCapabilities);
        checkBox_0->setObjectName(QStringLiteral("checkBox_0"));
        checkBox_0->setGeometry(QRect(80, 60, 70, 17));
        checkBox_0->setChecked(false);
        checkBox_1 = new QCheckBox(SecurityCapabilities);
        checkBox_1->setObjectName(QStringLiteral("checkBox_1"));
        checkBox_1->setGeometry(QRect(80, 90, 70, 17));
        checkBox_2 = new QCheckBox(SecurityCapabilities);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(80, 120, 111, 17));
        checkBox_3 = new QCheckBox(SecurityCapabilities);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(80, 150, 71, 20));
        checkBox_4 = new QCheckBox(SecurityCapabilities);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(80, 180, 70, 17));
        checkBox_5 = new QCheckBox(SecurityCapabilities);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(80, 210, 70, 20));
        checkBox_6 = new QCheckBox(SecurityCapabilities);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setGeometry(QRect(80, 240, 70, 17));
        checkBox_7 = new QCheckBox(SecurityCapabilities);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setGeometry(QRect(80, 270, 70, 17));
        widget = new QWidget(SecurityCapabilities);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(200, 310, 158, 25));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"background-color: rgb(42, 42, 42);"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"background-color: rgb(36, 36, 36);"));

        horizontalLayout->addWidget(pushButton_2);


        retranslateUi(SecurityCapabilities);

        QMetaObject::connectSlotsByName(SecurityCapabilities);
    } // setupUi

    void retranslateUi(QDialog *SecurityCapabilities)
    {
        SecurityCapabilities->setWindowTitle(QApplication::translate("SecurityCapabilities", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("SecurityCapabilities", "Security Capabilities", Q_NULLPTR));
        checkBox_0->setText(QApplication::translate("SecurityCapabilities", "SDA", Q_NULLPTR));
        checkBox_1->setText(QApplication::translate("SecurityCapabilities", "DDA", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("SecurityCapabilities", "CardCapture", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("SecurityCapabilities", "RFU", Q_NULLPTR));
        checkBox_4->setText(QApplication::translate("SecurityCapabilities", "CDA", Q_NULLPTR));
        checkBox_5->setText(QApplication::translate("SecurityCapabilities", "RFU", Q_NULLPTR));
        checkBox_6->setText(QApplication::translate("SecurityCapabilities", "RFU", Q_NULLPTR));
        checkBox_7->setText(QApplication::translate("SecurityCapabilities", "RFU", Q_NULLPTR));
        pushButton->setText(QApplication::translate("SecurityCapabilities", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("SecurityCapabilities", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SecurityCapabilities: public Ui_SecurityCapabilities {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SECURITYCAPABILITIES_H
