/********************************************************************************
** Form generated from reading UI file 'terminalcapabilities.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TERMINALCAPABILITIES_H
#define UI_TERMINALCAPABILITIES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TerminalCapabilities
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *lineEdit;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *checkBoxManual;
    QCheckBox *checkBoxMagnetic;
    QCheckBox *checkBoxICC;
    QCheckBox *checkBoxRFU1;
    QCheckBox *checkBoxRFU2;
    QCheckBox *checkBoxRFU3;
    QCheckBox *checkBoxRFU4;
    QCheckBox *checkBoxRFU5;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *checkBoxPlaintext;
    QCheckBox *checkBoxEncOnline;
    QCheckBox *checkBoxSingature;
    QCheckBox *checkBoxEncOffline;
    QCheckBox *checkBoxNoCVM;
    QCheckBox *checkBoxRFU6;
    QCheckBox *checkBoxRFU7;
    QCheckBox *checkBoxRFU8;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBoxSDA;
    QCheckBox *checkBoxDDA;
    QCheckBox *checkBoxCard;
    QCheckBox *checkBoxRFU9;
    QCheckBox *checkBoxCDA;
    QCheckBox *checkBoxRFU10;
    QCheckBox *checkBoxRFU11;
    QCheckBox *checkBoxRFU12;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButtonOkConfig;
    QPushButton *pushButtonCancelConfig;

    void setupUi(QDialog *TerminalCapabilities)
    {
        if (TerminalCapabilities->objectName().isEmpty())
            TerminalCapabilities->setObjectName(QStringLiteral("TerminalCapabilities"));
        TerminalCapabilities->resize(455, 323);
        TerminalCapabilities->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        gridLayout = new QGridLayout(TerminalCapabilities);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(TerminalCapabilities);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        lineEdit = new QLineEdit(TerminalCapabilities);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy);
        lineEdit->setStyleSheet(QStringLiteral("border: 1px solid black;"));

        horizontalLayout_3->addWidget(lineEdit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout_5->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        checkBoxManual = new QCheckBox(TerminalCapabilities);
        checkBoxManual->setObjectName(QStringLiteral("checkBoxManual"));

        verticalLayout_4->addWidget(checkBoxManual);

        checkBoxMagnetic = new QCheckBox(TerminalCapabilities);
        checkBoxMagnetic->setObjectName(QStringLiteral("checkBoxMagnetic"));

        verticalLayout_4->addWidget(checkBoxMagnetic);

        checkBoxICC = new QCheckBox(TerminalCapabilities);
        checkBoxICC->setObjectName(QStringLiteral("checkBoxICC"));

        verticalLayout_4->addWidget(checkBoxICC);

        checkBoxRFU1 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU1->setObjectName(QStringLiteral("checkBoxRFU1"));

        verticalLayout_4->addWidget(checkBoxRFU1);

        checkBoxRFU2 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU2->setObjectName(QStringLiteral("checkBoxRFU2"));

        verticalLayout_4->addWidget(checkBoxRFU2);

        checkBoxRFU3 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU3->setObjectName(QStringLiteral("checkBoxRFU3"));

        verticalLayout_4->addWidget(checkBoxRFU3);

        checkBoxRFU4 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU4->setObjectName(QStringLiteral("checkBoxRFU4"));

        verticalLayout_4->addWidget(checkBoxRFU4);

        checkBoxRFU5 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU5->setObjectName(QStringLiteral("checkBoxRFU5"));

        verticalLayout_4->addWidget(checkBoxRFU5);


        horizontalLayout->addLayout(verticalLayout_4);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        checkBoxPlaintext = new QCheckBox(TerminalCapabilities);
        checkBoxPlaintext->setObjectName(QStringLiteral("checkBoxPlaintext"));

        verticalLayout_3->addWidget(checkBoxPlaintext);

        checkBoxEncOnline = new QCheckBox(TerminalCapabilities);
        checkBoxEncOnline->setObjectName(QStringLiteral("checkBoxEncOnline"));

        verticalLayout_3->addWidget(checkBoxEncOnline);

        checkBoxSingature = new QCheckBox(TerminalCapabilities);
        checkBoxSingature->setObjectName(QStringLiteral("checkBoxSingature"));

        verticalLayout_3->addWidget(checkBoxSingature);

        checkBoxEncOffline = new QCheckBox(TerminalCapabilities);
        checkBoxEncOffline->setObjectName(QStringLiteral("checkBoxEncOffline"));

        verticalLayout_3->addWidget(checkBoxEncOffline);

        checkBoxNoCVM = new QCheckBox(TerminalCapabilities);
        checkBoxNoCVM->setObjectName(QStringLiteral("checkBoxNoCVM"));

        verticalLayout_3->addWidget(checkBoxNoCVM);

        checkBoxRFU6 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU6->setObjectName(QStringLiteral("checkBoxRFU6"));

        verticalLayout_3->addWidget(checkBoxRFU6);

        checkBoxRFU7 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU7->setObjectName(QStringLiteral("checkBoxRFU7"));

        verticalLayout_3->addWidget(checkBoxRFU7);

        checkBoxRFU8 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU8->setObjectName(QStringLiteral("checkBoxRFU8"));

        verticalLayout_3->addWidget(checkBoxRFU8);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        checkBoxSDA = new QCheckBox(TerminalCapabilities);
        checkBoxSDA->setObjectName(QStringLiteral("checkBoxSDA"));

        verticalLayout_2->addWidget(checkBoxSDA);

        checkBoxDDA = new QCheckBox(TerminalCapabilities);
        checkBoxDDA->setObjectName(QStringLiteral("checkBoxDDA"));

        verticalLayout_2->addWidget(checkBoxDDA);

        checkBoxCard = new QCheckBox(TerminalCapabilities);
        checkBoxCard->setObjectName(QStringLiteral("checkBoxCard"));

        verticalLayout_2->addWidget(checkBoxCard);

        checkBoxRFU9 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU9->setObjectName(QStringLiteral("checkBoxRFU9"));

        verticalLayout_2->addWidget(checkBoxRFU9);

        checkBoxCDA = new QCheckBox(TerminalCapabilities);
        checkBoxCDA->setObjectName(QStringLiteral("checkBoxCDA"));

        verticalLayout_2->addWidget(checkBoxCDA);

        checkBoxRFU10 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU10->setObjectName(QStringLiteral("checkBoxRFU10"));

        verticalLayout_2->addWidget(checkBoxRFU10);

        checkBoxRFU11 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU11->setObjectName(QStringLiteral("checkBoxRFU11"));

        verticalLayout_2->addWidget(checkBoxRFU11);

        checkBoxRFU12 = new QCheckBox(TerminalCapabilities);
        checkBoxRFU12->setObjectName(QStringLiteral("checkBoxRFU12"));

        verticalLayout_2->addWidget(checkBoxRFU12);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout_5->addLayout(horizontalLayout);


        gridLayout->addLayout(verticalLayout_5, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButtonOkConfig = new QPushButton(TerminalCapabilities);
        pushButtonOkConfig->setObjectName(QStringLiteral("pushButtonOkConfig"));

        horizontalLayout_2->addWidget(pushButtonOkConfig);

        pushButtonCancelConfig = new QPushButton(TerminalCapabilities);
        pushButtonCancelConfig->setObjectName(QStringLiteral("pushButtonCancelConfig"));

        horizontalLayout_2->addWidget(pushButtonCancelConfig);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);


        retranslateUi(TerminalCapabilities);

        QMetaObject::connectSlotsByName(TerminalCapabilities);
    } // setupUi

    void retranslateUi(QDialog *TerminalCapabilities)
    {
        TerminalCapabilities->setWindowTitle(QApplication::translate("TerminalCapabilities", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("TerminalCapabilities", "Terminal Capabilites", Q_NULLPTR));
        checkBoxManual->setText(QApplication::translate("TerminalCapabilities", "Manual Key Entry", Q_NULLPTR));
        checkBoxMagnetic->setText(QApplication::translate("TerminalCapabilities", "Magnetic Stripe", Q_NULLPTR));
        checkBoxICC->setText(QApplication::translate("TerminalCapabilities", "ICC with contacts", Q_NULLPTR));
        checkBoxRFU1->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU2->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU3->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU4->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU5->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxPlaintext->setText(QApplication::translate("TerminalCapabilities", "Plaintext for ICC verification", Q_NULLPTR));
        checkBoxEncOnline->setText(QApplication::translate("TerminalCapabilities", "Enciphered PIN for online verification", Q_NULLPTR));
        checkBoxSingature->setText(QApplication::translate("TerminalCapabilities", "Signature (paper)", Q_NULLPTR));
        checkBoxEncOffline->setText(QApplication::translate("TerminalCapabilities", "Enciphered PIN for offline verification", Q_NULLPTR));
        checkBoxNoCVM->setText(QApplication::translate("TerminalCapabilities", "No CVM required", Q_NULLPTR));
        checkBoxRFU6->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU7->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU8->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxSDA->setText(QApplication::translate("TerminalCapabilities", "SDA", Q_NULLPTR));
        checkBoxDDA->setText(QApplication::translate("TerminalCapabilities", "DDA", Q_NULLPTR));
        checkBoxCard->setText(QApplication::translate("TerminalCapabilities", "Card Capture", Q_NULLPTR));
        checkBoxRFU9->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxCDA->setText(QApplication::translate("TerminalCapabilities", "CDA", Q_NULLPTR));
        checkBoxRFU10->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU11->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        checkBoxRFU12->setText(QApplication::translate("TerminalCapabilities", "RFU", Q_NULLPTR));
        pushButtonOkConfig->setText(QApplication::translate("TerminalCapabilities", "OK", Q_NULLPTR));
        pushButtonCancelConfig->setText(QApplication::translate("TerminalCapabilities", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class TerminalCapabilities: public Ui_TerminalCapabilities {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TERMINALCAPABILITIES_H
