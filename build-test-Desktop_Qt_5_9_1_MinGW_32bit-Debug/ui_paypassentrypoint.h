/********************************************************************************
** Form generated from reading UI file 'paypassentrypoint.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PAYPASSENTRYPOINT_H
#define UI_PAYPASSENTRYPOINT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_PayPassEntryPoint
{
public:
    QGroupBox *groupBox;
    QCheckBox *checkBox0;
    QCheckBox *checkBoxcombo1;
    QCheckBox *checkBox1;
    QCheckBox *checkBox2;
    QCheckBox *checkBoxCombo2;
    QCheckBox *checkBox3;
    QCheckBox *checkBox4;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QComboBox *comboBox;
    QComboBox *comboBox_2;
    QGroupBox *groupBox_2;
    QCheckBox *checkBox5;
    QCheckBox *checkBox8;
    QCheckBox *checkBox9;
    QCheckBox *checkBox10;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_7;
    QLineEdit *lineEdit_8;
    QLineEdit *lineEdit_9;
    QGroupBox *groupBox_3;
    QCheckBox *checkBox13;
    QCheckBox *checkBox15;
    QCheckBox *checkBox16;
    QCheckBox *checkBox14;
    QCheckBox *checkBox12;
    QCheckBox *checkBox11;
    QCheckBox *checkBox17;
    QCheckBox *checkBox20;
    QCheckBox *checkBox22;
    QCheckBox *checkBox21;
    QCheckBox *checkBox19;
    QCheckBox *checkBox18;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_11;
    QLineEdit *lineEdit_12;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_15;
    QLineEdit *lineEdit_16;
    QLineEdit *lineEdit_17;
    QLineEdit *lineEdit_18;
    QLineEdit *lineEdit_19;
    QLineEdit *lineEdit_20;
    QLineEdit *lineEdit_21;
    QSpinBox *spinBox;
    QGroupBox *groupBox_4;
    QCheckBox *checkBox23;
    QCheckBox *checkBox25;
    QCheckBox *checkBox26;
    QCheckBox *checkBox24;
    QLineEdit *lineEdit_22;
    QLineEdit *lineEdit_23;
    QLineEdit *lineEdit_24;
    QLineEdit *lineEdit_25;
    QGroupBox *groupBox_5;
    QLineEdit *lineEdit_26;

    void setupUi(QDialog *PayPassEntryPoint)
    {
        if (PayPassEntryPoint->objectName().isEmpty())
            PayPassEntryPoint->setObjectName(QStringLiteral("PayPassEntryPoint"));
        PayPassEntryPoint->resize(1109, 596);
        PayPassEntryPoint->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        groupBox = new QGroupBox(PayPassEntryPoint);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 331, 231));
        groupBox->setStyleSheet(QLatin1String("#groupBox{\n"
"border: 1px solid black;\n"
"}"));
        checkBox0 = new QCheckBox(groupBox);
        checkBox0->setObjectName(QStringLiteral("checkBox0"));
        checkBox0->setGeometry(QRect(10, 20, 131, 17));
        checkBoxcombo1 = new QCheckBox(groupBox);
        checkBoxcombo1->setObjectName(QStringLiteral("checkBoxcombo1"));
        checkBoxcombo1->setGeometry(QRect(10, 50, 111, 17));
        checkBox1 = new QCheckBox(groupBox);
        checkBox1->setObjectName(QStringLiteral("checkBox1"));
        checkBox1->setGeometry(QRect(10, 80, 171, 17));
        checkBox2 = new QCheckBox(groupBox);
        checkBox2->setObjectName(QStringLiteral("checkBox2"));
        checkBox2->setGeometry(QRect(10, 110, 151, 17));
        checkBoxCombo2 = new QCheckBox(groupBox);
        checkBoxCombo2->setObjectName(QStringLiteral("checkBoxCombo2"));
        checkBoxCombo2->setGeometry(QRect(10, 140, 70, 17));
        checkBox3 = new QCheckBox(groupBox);
        checkBox3->setObjectName(QStringLiteral("checkBox3"));
        checkBox3->setGeometry(QRect(10, 170, 121, 17));
        checkBox4 = new QCheckBox(groupBox);
        checkBox4->setObjectName(QStringLiteral("checkBox4"));
        checkBox4->setGeometry(QRect(10, 200, 121, 17));
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(190, 20, 81, 20));
        lineEdit->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        lineEdit_2 = new QLineEdit(groupBox);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(192, 80, 131, 20));
        lineEdit_2->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        lineEdit_3 = new QLineEdit(groupBox);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(190, 110, 131, 20));
        lineEdit_3->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        lineEdit_4 = new QLineEdit(groupBox);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(190, 170, 81, 20));
        lineEdit_5 = new QLineEdit(groupBox);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(190, 200, 81, 20));
        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(190, 50, 69, 22));
        comboBox_2 = new QComboBox(groupBox);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(190, 140, 121, 22));
        groupBox_2 = new QGroupBox(PayPassEntryPoint);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 250, 331, 151));
        groupBox_2->setStyleSheet(QLatin1String("#groupBox_2{\n"
"border: 1px solid black;\n"
"}"));
        checkBox5 = new QCheckBox(groupBox_2);
        checkBox5->setObjectName(QStringLiteral("checkBox5"));
        checkBox5->setGeometry(QRect(10, 20, 151, 17));
        checkBox8 = new QCheckBox(groupBox_2);
        checkBox8->setObjectName(QStringLiteral("checkBox8"));
        checkBox8->setGeometry(QRect(10, 50, 121, 17));
        checkBox9 = new QCheckBox(groupBox_2);
        checkBox9->setObjectName(QStringLiteral("checkBox9"));
        checkBox9->setGeometry(QRect(10, 80, 171, 17));
        checkBox10 = new QCheckBox(groupBox_2);
        checkBox10->setObjectName(QStringLiteral("checkBox10"));
        checkBox10->setGeometry(QRect(10, 100, 191, 31));
        lineEdit_6 = new QLineEdit(groupBox_2);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(210, 20, 61, 20));
        lineEdit_7 = new QLineEdit(groupBox_2);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));
        lineEdit_7->setGeometry(QRect(210, 50, 113, 20));
        lineEdit_8 = new QLineEdit(groupBox_2);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        lineEdit_8->setGeometry(QRect(210, 80, 71, 20));
        lineEdit_9 = new QLineEdit(groupBox_2);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));
        lineEdit_9->setGeometry(QRect(210, 110, 71, 20));
        groupBox_3 = new QGroupBox(PayPassEntryPoint);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(350, 10, 381, 391));
        groupBox_3->setStyleSheet(QLatin1String("#groupBox_3{\n"
"border: 1px solid black;\n"
"}"));
        checkBox13 = new QCheckBox(groupBox_3);
        checkBox13->setObjectName(QStringLiteral("checkBox13"));
        checkBox13->setGeometry(QRect(20, 80, 161, 17));
        checkBox15 = new QCheckBox(groupBox_3);
        checkBox15->setObjectName(QStringLiteral("checkBox15"));
        checkBox15->setGeometry(QRect(20, 140, 191, 17));
        checkBox16 = new QCheckBox(groupBox_3);
        checkBox16->setObjectName(QStringLiteral("checkBox16"));
        checkBox16->setGeometry(QRect(20, 170, 181, 17));
        checkBox14 = new QCheckBox(groupBox_3);
        checkBox14->setObjectName(QStringLiteral("checkBox14"));
        checkBox14->setGeometry(QRect(20, 110, 171, 17));
        checkBox12 = new QCheckBox(groupBox_3);
        checkBox12->setObjectName(QStringLiteral("checkBox12"));
        checkBox12->setGeometry(QRect(20, 50, 161, 17));
        checkBox11 = new QCheckBox(groupBox_3);
        checkBox11->setObjectName(QStringLiteral("checkBox11"));
        checkBox11->setGeometry(QRect(20, 20, 161, 17));
        checkBox17 = new QCheckBox(groupBox_3);
        checkBox17->setObjectName(QStringLiteral("checkBox17"));
        checkBox17->setGeometry(QRect(20, 200, 171, 17));
        checkBox20 = new QCheckBox(groupBox_3);
        checkBox20->setObjectName(QStringLiteral("checkBox20"));
        checkBox20->setGeometry(QRect(20, 290, 171, 17));
        checkBox22 = new QCheckBox(groupBox_3);
        checkBox22->setObjectName(QStringLiteral("checkBox22"));
        checkBox22->setGeometry(QRect(20, 350, 161, 17));
        checkBox21 = new QCheckBox(groupBox_3);
        checkBox21->setObjectName(QStringLiteral("checkBox21"));
        checkBox21->setGeometry(QRect(20, 320, 171, 17));
        checkBox19 = new QCheckBox(groupBox_3);
        checkBox19->setObjectName(QStringLiteral("checkBox19"));
        checkBox19->setGeometry(QRect(20, 260, 171, 16));
        checkBox18 = new QCheckBox(groupBox_3);
        checkBox18->setObjectName(QStringLiteral("checkBox18"));
        checkBox18->setGeometry(QRect(20, 230, 171, 17));
        lineEdit_10 = new QLineEdit(groupBox_3);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));
        lineEdit_10->setGeometry(QRect(230, 20, 71, 20));
        lineEdit_11 = new QLineEdit(groupBox_3);
        lineEdit_11->setObjectName(QStringLiteral("lineEdit_11"));
        lineEdit_11->setGeometry(QRect(230, 50, 91, 20));
        lineEdit_12 = new QLineEdit(groupBox_3);
        lineEdit_12->setObjectName(QStringLiteral("lineEdit_12"));
        lineEdit_12->setGeometry(QRect(230, 110, 91, 20));
        lineEdit_13 = new QLineEdit(groupBox_3);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(230, 80, 91, 20));
        lineEdit_15 = new QLineEdit(groupBox_3);
        lineEdit_15->setObjectName(QStringLiteral("lineEdit_15"));
        lineEdit_15->setGeometry(QRect(230, 230, 141, 20));
        lineEdit_16 = new QLineEdit(groupBox_3);
        lineEdit_16->setObjectName(QStringLiteral("lineEdit_16"));
        lineEdit_16->setGeometry(QRect(230, 170, 91, 20));
        lineEdit_17 = new QLineEdit(groupBox_3);
        lineEdit_17->setObjectName(QStringLiteral("lineEdit_17"));
        lineEdit_17->setGeometry(QRect(230, 140, 91, 20));
        lineEdit_18 = new QLineEdit(groupBox_3);
        lineEdit_18->setObjectName(QStringLiteral("lineEdit_18"));
        lineEdit_18->setGeometry(QRect(230, 350, 141, 20));
        lineEdit_19 = new QLineEdit(groupBox_3);
        lineEdit_19->setObjectName(QStringLiteral("lineEdit_19"));
        lineEdit_19->setGeometry(QRect(230, 320, 141, 20));
        lineEdit_20 = new QLineEdit(groupBox_3);
        lineEdit_20->setObjectName(QStringLiteral("lineEdit_20"));
        lineEdit_20->setGeometry(QRect(230, 260, 141, 20));
        lineEdit_21 = new QLineEdit(groupBox_3);
        lineEdit_21->setObjectName(QStringLiteral("lineEdit_21"));
        lineEdit_21->setGeometry(QRect(230, 290, 141, 20));
        spinBox = new QSpinBox(groupBox_3);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(230, 200, 81, 22));
        groupBox_4 = new QGroupBox(PayPassEntryPoint);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(740, 10, 361, 391));
        groupBox_4->setStyleSheet(QLatin1String("#groupBox_4{\n"
"border: 1px solid black;\n"
"}"));
        checkBox23 = new QCheckBox(groupBox_4);
        checkBox23->setObjectName(QStringLiteral("checkBox23"));
        checkBox23->setGeometry(QRect(20, 20, 171, 17));
        checkBox25 = new QCheckBox(groupBox_4);
        checkBox25->setObjectName(QStringLiteral("checkBox25"));
        checkBox25->setGeometry(QRect(20, 80, 171, 17));
        checkBox26 = new QCheckBox(groupBox_4);
        checkBox26->setObjectName(QStringLiteral("checkBox26"));
        checkBox26->setGeometry(QRect(20, 110, 171, 17));
        checkBox24 = new QCheckBox(groupBox_4);
        checkBox24->setObjectName(QStringLiteral("checkBox24"));
        checkBox24->setGeometry(QRect(20, 50, 171, 17));
        lineEdit_22 = new QLineEdit(groupBox_4);
        lineEdit_22->setObjectName(QStringLiteral("lineEdit_22"));
        lineEdit_22->setGeometry(QRect(202, 80, 151, 20));
        lineEdit_23 = new QLineEdit(groupBox_4);
        lineEdit_23->setObjectName(QStringLiteral("lineEdit_23"));
        lineEdit_23->setGeometry(QRect(202, 110, 151, 20));
        lineEdit_24 = new QLineEdit(groupBox_4);
        lineEdit_24->setObjectName(QStringLiteral("lineEdit_24"));
        lineEdit_24->setGeometry(QRect(202, 50, 151, 20));
        lineEdit_25 = new QLineEdit(groupBox_4);
        lineEdit_25->setObjectName(QStringLiteral("lineEdit_25"));
        lineEdit_25->setGeometry(QRect(202, 20, 151, 20));
        groupBox_5 = new QGroupBox(PayPassEntryPoint);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 420, 1091, 121));
        groupBox_5->setStyleSheet(QLatin1String("#groupBox_5{\n"
"border: 1px solid black;\n"
"}"));
        lineEdit_26 = new QLineEdit(groupBox_5);
        lineEdit_26->setObjectName(QStringLiteral("lineEdit_26"));
        lineEdit_26->setGeometry(QRect(10, 20, 1071, 91));
        lineEdit_26->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        lineEdit_26->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        retranslateUi(PayPassEntryPoint);

        QMetaObject::connectSlotsByName(PayPassEntryPoint);
    } // setupUi

    void retranslateUi(QDialog *PayPassEntryPoint)
    {
        PayPassEntryPoint->setWindowTitle(QApplication::translate("PayPassEntryPoint", "Dialog", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("PayPassEntryPoint", "Common", Q_NULLPTR));
        checkBox0->setText(QApplication::translate("PayPassEntryPoint", "Terminl Country Code", Q_NULLPTR));
        checkBoxcombo1->setText(QApplication::translate("PayPassEntryPoint", "Terminal Type", Q_NULLPTR));
        checkBox1->setText(QApplication::translate("PayPassEntryPoint", "Additional Terminal Capabilities", Q_NULLPTR));
        checkBox2->setText(QApplication::translate("PayPassEntryPoint", "Mobile Support Indicator", Q_NULLPTR));
        checkBoxCombo2->setText(QApplication::translate("PayPassEntryPoint", "Kernal ID", Q_NULLPTR));
        checkBox3->setText(QApplication::translate("PayPassEntryPoint", "Kernel Configuration", Q_NULLPTR));
        checkBox4->setText(QApplication::translate("PayPassEntryPoint", "Message Hold Time", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("PayPassEntryPoint", "Mastripe", Q_NULLPTR));
        checkBox5->setText(QApplication::translate("PayPassEntryPoint", "Application Version Number", Q_NULLPTR));
        checkBox8->setText(QApplication::translate("PayPassEntryPoint", "Default UDOL", Q_NULLPTR));
        checkBox9->setText(QApplication::translate("PayPassEntryPoint", "CVM Capabilities- CVM Required", Q_NULLPTR));
        checkBox10->setText(QApplication::translate("PayPassEntryPoint", "CVM Capabilities - No CVM Required", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("PayPassEntryPoint", "EMV", Q_NULLPTR));
        checkBox13->setText(QApplication::translate("PayPassEntryPoint", "Card Data Input Capabilities", Q_NULLPTR));
        checkBox15->setText(QApplication::translate("PayPassEntryPoint", "CVM Capabilitites No CVM required", Q_NULLPTR));
        checkBox16->setText(QApplication::translate("PayPassEntryPoint", "Max Lifetime Torn Transaction", Q_NULLPTR));
        checkBox14->setText(QApplication::translate("PayPassEntryPoint", "CVM Capabilities CVM Required", Q_NULLPTR));
        checkBox12->setText(QApplication::translate("PayPassEntryPoint", "Security Capabilities", Q_NULLPTR));
        checkBox11->setText(QApplication::translate("PayPassEntryPoint", "Application Version Number", Q_NULLPTR));
        checkBox17->setText(QApplication::translate("PayPassEntryPoint", "Max Number Torn Transaction", Q_NULLPTR));
        checkBox20->setText(QApplication::translate("PayPassEntryPoint", "TAC Online", Q_NULLPTR));
        checkBox22->setText(QApplication::translate("PayPassEntryPoint", "Balance Read After GenAC", Q_NULLPTR));
        checkBox21->setText(QApplication::translate("PayPassEntryPoint", "Balance Read Before GenAC", Q_NULLPTR));
        checkBox19->setText(QApplication::translate("PayPassEntryPoint", "TAC Denial", Q_NULLPTR));
        checkBox18->setText(QApplication::translate("PayPassEntryPoint", "TAC Default", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("PayPassEntryPoint", "Limits", Q_NULLPTR));
        checkBox23->setText(QApplication::translate("PayPassEntryPoint", "Reader Contactless Floor Limit", Q_NULLPTR));
        checkBox25->setText(QApplication::translate("PayPassEntryPoint", "RCTL (On- device CVM)", Q_NULLPTR));
        checkBox26->setText(QApplication::translate("PayPassEntryPoint", "Reader CVM Required Limit", Q_NULLPTR));
        checkBox24->setText(QApplication::translate("PayPassEntryPoint", "RCTL (No On-Device CVM)", Q_NULLPTR));
        groupBox_5->setTitle(QApplication::translate("PayPassEntryPoint", "Other TLVs", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PayPassEntryPoint: public Ui_PayPassEntryPoint {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PAYPASSENTRYPOINT_H
