/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../test/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[21];
    char stringdata0[421];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 22), // "ButtonTerminal_Clicked"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 22), // "ButtonAddition_Clicked"
QT_MOC_LITERAL(4, 58, 21), // "ButtonDefault_Clicked"
QT_MOC_LITERAL(5, 80, 20), // "ButtonOnline_Clicked"
QT_MOC_LITERAL(6, 101, 16), // "Button_7_Clicked"
QT_MOC_LITERAL(7, 118, 13), // "UpdatePaypass"
QT_MOC_LITERAL(8, 132, 16), // "GroupButtonClick"
QT_MOC_LITERAL(9, 149, 5), // "Index"
QT_MOC_LITERAL(10, 155, 17), // "Additional_Update"
QT_MOC_LITERAL(11, 173, 15), // "Terminal_Update"
QT_MOC_LITERAL(12, 189, 14), // "Default_Update"
QT_MOC_LITERAL(13, 204, 13), // "OpenNewWindow"
QT_MOC_LITERAL(14, 218, 29), // "on_toolButtonAddition_clicked"
QT_MOC_LITERAL(15, 248, 28), // "on_toolButtonDefault_clicked"
QT_MOC_LITERAL(16, 277, 27), // "on_toolButtonOnline_clicked"
QT_MOC_LITERAL(17, 305, 23), // "on_toolButton_7_clicked"
QT_MOC_LITERAL(18, 329, 26), // "on_checkBoxDefault_clicked"
QT_MOC_LITERAL(19, 356, 29), // "on_toolButtonTerminal_clicked"
QT_MOC_LITERAL(20, 386, 34) // "on_toolButtonAddProcessing_cl..."

    },
    "MainWindow\0ButtonTerminal_Clicked\0\0"
    "ButtonAddition_Clicked\0ButtonDefault_Clicked\0"
    "ButtonOnline_Clicked\0Button_7_Clicked\0"
    "UpdatePaypass\0GroupButtonClick\0Index\0"
    "Additional_Update\0Terminal_Update\0"
    "Default_Update\0OpenNewWindow\0"
    "on_toolButtonAddition_clicked\0"
    "on_toolButtonDefault_clicked\0"
    "on_toolButtonOnline_clicked\0"
    "on_toolButton_7_clicked\0"
    "on_checkBoxDefault_clicked\0"
    "on_toolButtonTerminal_clicked\0"
    "on_toolButtonAddProcessing_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x06 /* Public */,
       3,    0,  105,    2, 0x06 /* Public */,
       4,    0,  106,    2, 0x06 /* Public */,
       5,    0,  107,    2, 0x06 /* Public */,
       6,    0,  108,    2, 0x06 /* Public */,
       7,    0,  109,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,  110,    2, 0x0a /* Public */,
      10,    0,  113,    2, 0x0a /* Public */,
      11,    0,  114,    2, 0x0a /* Public */,
      12,    0,  115,    2, 0x0a /* Public */,
      13,    0,  116,    2, 0x0a /* Public */,
      14,    0,  117,    2, 0x08 /* Private */,
      15,    0,  118,    2, 0x08 /* Private */,
      16,    0,  119,    2, 0x08 /* Private */,
      17,    0,  120,    2, 0x08 /* Private */,
      18,    0,  121,    2, 0x08 /* Private */,
      19,    0,  122,    2, 0x08 /* Private */,
      20,    0,  123,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ButtonTerminal_Clicked(); break;
        case 1: _t->ButtonAddition_Clicked(); break;
        case 2: _t->ButtonDefault_Clicked(); break;
        case 3: _t->ButtonOnline_Clicked(); break;
        case 4: _t->Button_7_Clicked(); break;
        case 5: _t->UpdatePaypass(); break;
        case 6: _t->GroupButtonClick((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->Additional_Update(); break;
        case 8: _t->Terminal_Update(); break;
        case 9: _t->Default_Update(); break;
        case 10: _t->OpenNewWindow(); break;
        case 11: _t->on_toolButtonAddition_clicked(); break;
        case 12: _t->on_toolButtonDefault_clicked(); break;
        case 13: _t->on_toolButtonOnline_clicked(); break;
        case 14: _t->on_toolButton_7_clicked(); break;
        case 15: _t->on_checkBoxDefault_clicked(); break;
        case 16: _t->on_toolButtonTerminal_clicked(); break;
        case 17: _t->on_toolButtonAddProcessing_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonTerminal_Clicked)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonAddition_Clicked)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonDefault_Clicked)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::ButtonOnline_Clicked)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::Button_7_Clicked)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::UpdatePaypass)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::ButtonTerminal_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void MainWindow::ButtonAddition_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void MainWindow::ButtonDefault_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void MainWindow::ButtonOnline_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void MainWindow::Button_7_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void MainWindow::UpdatePaypass()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
