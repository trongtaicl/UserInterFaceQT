/****************************************************************************
** Meta object code from reading C++ file 'terminalcapabilities.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../test/terminalcapabilities.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'terminalcapabilities.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TerminalCapabilities_t {
    QByteArrayData data[30];
    char stringdata0[721];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TerminalCapabilities_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TerminalCapabilities_t qt_meta_stringdata_TerminalCapabilities = {
    {
QT_MOC_LITERAL(0, 0, 20), // "TerminalCapabilities"
QT_MOC_LITERAL(1, 21, 16), // "ButtonOk_Clicked"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 14), // "ChangeTextEdit"
QT_MOC_LITERAL(4, 54, 29), // "on_pushButtonOkConfig_clicked"
QT_MOC_LITERAL(5, 84, 33), // "on_pushButtonCancelConfig_cli..."
QT_MOC_LITERAL(6, 118, 25), // "on_checkBoxManual_clicked"
QT_MOC_LITERAL(7, 144, 27), // "on_checkBoxMagnetic_clicked"
QT_MOC_LITERAL(8, 172, 22), // "on_checkBoxICC_clicked"
QT_MOC_LITERAL(9, 195, 23), // "on_checkBoxRFU1_clicked"
QT_MOC_LITERAL(10, 219, 23), // "on_checkBoxRFU2_clicked"
QT_MOC_LITERAL(11, 243, 23), // "on_checkBoxRFU3_clicked"
QT_MOC_LITERAL(12, 267, 23), // "on_checkBoxRFU4_clicked"
QT_MOC_LITERAL(13, 291, 23), // "on_checkBoxRFU5_clicked"
QT_MOC_LITERAL(14, 315, 22), // "on_checkBoxSDA_clicked"
QT_MOC_LITERAL(15, 338, 22), // "on_checkBoxDDA_clicked"
QT_MOC_LITERAL(16, 361, 23), // "on_checkBoxCard_clicked"
QT_MOC_LITERAL(17, 385, 23), // "on_checkBoxRFU9_clicked"
QT_MOC_LITERAL(18, 409, 22), // "on_checkBoxCDA_clicked"
QT_MOC_LITERAL(19, 432, 24), // "on_checkBoxRFU10_clicked"
QT_MOC_LITERAL(20, 457, 24), // "on_checkBoxRFU11_clicked"
QT_MOC_LITERAL(21, 482, 24), // "on_checkBoxRFU12_clicked"
QT_MOC_LITERAL(22, 507, 28), // "on_checkBoxPlaintext_clicked"
QT_MOC_LITERAL(23, 536, 28), // "on_checkBoxEncOnline_clicked"
QT_MOC_LITERAL(24, 565, 28), // "on_checkBoxSingature_clicked"
QT_MOC_LITERAL(25, 594, 29), // "on_checkBoxEncOffline_clicked"
QT_MOC_LITERAL(26, 624, 24), // "on_checkBoxNoCVM_clicked"
QT_MOC_LITERAL(27, 649, 23), // "on_checkBoxRFU6_clicked"
QT_MOC_LITERAL(28, 673, 23), // "on_checkBoxRFU7_clicked"
QT_MOC_LITERAL(29, 697, 23) // "on_checkBoxRFU8_clicked"

    },
    "TerminalCapabilities\0ButtonOk_Clicked\0"
    "\0ChangeTextEdit\0on_pushButtonOkConfig_clicked\0"
    "on_pushButtonCancelConfig_clicked\0"
    "on_checkBoxManual_clicked\0"
    "on_checkBoxMagnetic_clicked\0"
    "on_checkBoxICC_clicked\0on_checkBoxRFU1_clicked\0"
    "on_checkBoxRFU2_clicked\0on_checkBoxRFU3_clicked\0"
    "on_checkBoxRFU4_clicked\0on_checkBoxRFU5_clicked\0"
    "on_checkBoxSDA_clicked\0on_checkBoxDDA_clicked\0"
    "on_checkBoxCard_clicked\0on_checkBoxRFU9_clicked\0"
    "on_checkBoxCDA_clicked\0on_checkBoxRFU10_clicked\0"
    "on_checkBoxRFU11_clicked\0"
    "on_checkBoxRFU12_clicked\0"
    "on_checkBoxPlaintext_clicked\0"
    "on_checkBoxEncOnline_clicked\0"
    "on_checkBoxSingature_clicked\0"
    "on_checkBoxEncOffline_clicked\0"
    "on_checkBoxNoCVM_clicked\0"
    "on_checkBoxRFU6_clicked\0on_checkBoxRFU7_clicked\0"
    "on_checkBoxRFU8_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TerminalCapabilities[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  154,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  155,    2, 0x0a /* Public */,
       4,    0,  156,    2, 0x08 /* Private */,
       5,    0,  157,    2, 0x08 /* Private */,
       6,    0,  158,    2, 0x08 /* Private */,
       7,    0,  159,    2, 0x08 /* Private */,
       8,    0,  160,    2, 0x08 /* Private */,
       9,    0,  161,    2, 0x08 /* Private */,
      10,    0,  162,    2, 0x08 /* Private */,
      11,    0,  163,    2, 0x08 /* Private */,
      12,    0,  164,    2, 0x08 /* Private */,
      13,    0,  165,    2, 0x08 /* Private */,
      14,    0,  166,    2, 0x08 /* Private */,
      15,    0,  167,    2, 0x08 /* Private */,
      16,    0,  168,    2, 0x08 /* Private */,
      17,    0,  169,    2, 0x08 /* Private */,
      18,    0,  170,    2, 0x08 /* Private */,
      19,    0,  171,    2, 0x08 /* Private */,
      20,    0,  172,    2, 0x08 /* Private */,
      21,    0,  173,    2, 0x08 /* Private */,
      22,    0,  174,    2, 0x08 /* Private */,
      23,    0,  175,    2, 0x08 /* Private */,
      24,    0,  176,    2, 0x08 /* Private */,
      25,    0,  177,    2, 0x08 /* Private */,
      26,    0,  178,    2, 0x08 /* Private */,
      27,    0,  179,    2, 0x08 /* Private */,
      28,    0,  180,    2, 0x08 /* Private */,
      29,    0,  181,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TerminalCapabilities::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TerminalCapabilities *_t = static_cast<TerminalCapabilities *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ButtonOk_Clicked(); break;
        case 1: _t->ChangeTextEdit(); break;
        case 2: _t->on_pushButtonOkConfig_clicked(); break;
        case 3: _t->on_pushButtonCancelConfig_clicked(); break;
        case 4: _t->on_checkBoxManual_clicked(); break;
        case 5: _t->on_checkBoxMagnetic_clicked(); break;
        case 6: _t->on_checkBoxICC_clicked(); break;
        case 7: _t->on_checkBoxRFU1_clicked(); break;
        case 8: _t->on_checkBoxRFU2_clicked(); break;
        case 9: _t->on_checkBoxRFU3_clicked(); break;
        case 10: _t->on_checkBoxRFU4_clicked(); break;
        case 11: _t->on_checkBoxRFU5_clicked(); break;
        case 12: _t->on_checkBoxSDA_clicked(); break;
        case 13: _t->on_checkBoxDDA_clicked(); break;
        case 14: _t->on_checkBoxCard_clicked(); break;
        case 15: _t->on_checkBoxRFU9_clicked(); break;
        case 16: _t->on_checkBoxCDA_clicked(); break;
        case 17: _t->on_checkBoxRFU10_clicked(); break;
        case 18: _t->on_checkBoxRFU11_clicked(); break;
        case 19: _t->on_checkBoxRFU12_clicked(); break;
        case 20: _t->on_checkBoxPlaintext_clicked(); break;
        case 21: _t->on_checkBoxEncOnline_clicked(); break;
        case 22: _t->on_checkBoxSingature_clicked(); break;
        case 23: _t->on_checkBoxEncOffline_clicked(); break;
        case 24: _t->on_checkBoxNoCVM_clicked(); break;
        case 25: _t->on_checkBoxRFU6_clicked(); break;
        case 26: _t->on_checkBoxRFU7_clicked(); break;
        case 27: _t->on_checkBoxRFU8_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TerminalCapabilities::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TerminalCapabilities::ButtonOk_Clicked)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject TerminalCapabilities::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TerminalCapabilities.data,
      qt_meta_data_TerminalCapabilities,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TerminalCapabilities::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TerminalCapabilities::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TerminalCapabilities.stringdata0))
        return static_cast<void*>(const_cast< TerminalCapabilities*>(this));
    return QDialog::qt_metacast(_clname);
}

int TerminalCapabilities::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 28;
    }
    return _id;
}

// SIGNAL 0
void TerminalCapabilities::ButtonOk_Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
