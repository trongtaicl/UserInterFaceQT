#include "cPaywaveEntryPoint.h"
#include "ui_cPaywaveEntryPoint.h"
#include <QDebug>
#include <QTextEdit>
#include <QTextOption>

cPaywaveEntryPoint::cPaywaveEntryPoint(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cPaywaveEntryPoint)
{
    ui->setupUi(this);
    this->setWindowTitle("Paywave Entrypoint");
    ListCheckbox << ui->checkBox_1 << ui->checkBox_2 << ui->checkBox_3 << ui->checkBox_4
                 << ui->checkBox_5 << ui->checkBox_6 << ui->checkBox_7 << ui->checkBox_8
                 << ui->checkBox_11 << ui->checkBox_12 << ui->checkBox_13;

    LineEditCheck << ui->lineEdit_1 << ui->lineEdit_2 << ui->lineEdit_3 << ui->lineEdit_4
                  << ui->lineEdit_5 << ui->lineEdit_6 << ui->lineEdit_7 << ui->lineEdit_8
                  << ui->lineEdit_9 << ui->lineEdit_10 << ui->lineEdit_11;

    for (int i =0; i < LineEditCheck.count(); i++){
        LineEditCheck.at(i)->setStyleSheet("border: 1px solid black");
    }

    listCheckboxCombo << ui->checkBox_0 << ui->checkBox_9;
    listCheckboxSpin << ui->checkBox_10;
    listCombo << ui->comboBox << ui->comboBox_2;
    listSpinbox << ui->spinBox;

    InitializePaywaveEntryPoint();
    InitializeToolButton();

}

cPaywaveEntryPoint::~cPaywaveEntryPoint()
{
    delete ui;
}

void cPaywaveEntryPoint::InitializePaywaveEntryPoint()
{
    int frameWidth =0;

    for (int i =0; i< ListCheckbox.count(); i++){
        ListCheckbox.at(i)->setChecked(false);
        ListCheckbox.at(i)->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
        LineEditCheck.at(i)->setEnabled(false);
        LineEditCheck.at(i)->setStyleSheet("border: 1px solid rgb(160, 160, 160); border-radius: 2px");
    }

    for (int i =0; i < listCheckboxCombo.count(); i++){
        listCheckboxCombo.at(i)->setChecked(false);
        listCheckboxCombo.at(i)->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
        listCombo.at(i)->setEnabled(false);
    }
    for (int i =0; i< listSpinbox.count(); i++){
        listCheckboxSpin.at(i)->setChecked(false);
        listCheckboxSpin.at(i)->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
        listSpinbox.at(i)->setEnabled(false);
    }

    for (int i = 8; i<LineEditCheck.count(); i++){
        LineEditCheck.at(i)->setInputMask("hhhhhhhhhhhh");
        LineEditCheck.at(i)->setText("000000000000");

    }

    ui->lineEdit_1->setInputMask("hhhh");
    ui->lineEdit_1->setText("0000");
    ui->lineEdit_1->setToolTip("Tag 9F1A");
    ui->checkBox_1->setToolTip("Tag 9F1A");

    ui->lineEdit_2->setInputMask("hhhhhh");
    ui->lineEdit_2->setText("000000");
    ui->lineEdit_2->setToolTip("Tag 9F33");
    ui->checkBox_2->setToolTip("Tag 9F33");

    ui->lineEdit_3->setInputMask("hhhhhhhh");
    ui->lineEdit_3->setText("0000");
    ui->lineEdit_3->setToolTip("Tag 9F40");
    ui->checkBox_3->setToolTip("Tag 9F40");

    ui->lineEdit_4->setInputMask("hhhhhhhh");
    ui->lineEdit_4->setText("00000000");
    ui->lineEdit_4->setToolTip("Tag 9F66");
    ui->checkBox_4->setToolTip("Tag 9F66");

    ui->lineEdit_5->setInputMask("hhhhhh");
    ui->lineEdit_5->setText("000000");
    ui->lineEdit_5->setToolTip("Tag DF1B");
    ui->checkBox_5->setToolTip("Tag DF1B");

    ui->lineEdit_6->setInputMask("hhhhhh");
    ui->lineEdit_6->setText("000000");
    ui->checkBox_6->setToolTip("Tag DF2D");
    ui->lineEdit_6->setToolTip("Tag DF2D");

    ui->lineEdit_7->setInputMask("hhhh");
    ui->lineEdit_7->setText("0000");
    ui->checkBox_7->setToolTip("Tag 9F09");
    ui->lineEdit_7->setToolTip("Tag 9F09");

    ui->lineEdit_8->setInputMask("hh");
    ui->lineEdit_8->setText("00");
    ui->checkBox_8->setToolTip("Tag DF30");
    ui->lineEdit_8->setToolTip("Tag DF30");

    ui->checkBox_11->setToolTip("Tag DF23");
    ui->lineEdit_9->setToolTip("Tag DF23");

    ui->checkBox_12->setToolTip("Tag DF24");
    ui->lineEdit_10->setToolTip("Tag DF24");

    ui->checkBox_13->setToolTip("Tag DF26");
    ui->lineEdit_11->setToolTip("Tag DF26");

    ui->checkBox_0->setToolTip("Tag 9F35");
    ui->comboBox->setToolTip("Tag 9F35");

    ui->checkBox_9->setToolTip("Tag DF32");
    ui->comboBox_2->setToolTip("Tag DF32");

    ui->checkBox_10->setToolTip(" Tag 9F1B");
    ui->spinBox->setToolTip("Tag 9F1B");
    ComboTerType << "00" << "11" << "12" << "13" << "14" << "15" << "16" << "21" << "22" << "23"
                 << "24" << "25" <<"26" << "34" << "35" << "36";

    for (int i =0; i< ComboTerType.count(); i++){
        ui->comboBox->addItem(ComboTerType.at(i));
    }

    ComboState << "not allowed (0x00)" << "online cryptogram request" << "not allowed (0x02)";

    for (int i =0; i< ComboState.count() ; i++){
        ui->comboBox_2->addItem(ComboState.at(i));
    }

    mTLV.initializeList(ListCheckbox,LineEditCheck,listCheckboxCombo,listCheckboxSpin,
                        listCombo,listSpinbox,ComboTerType,QStringList(),ComboState);

    ui->comboBox->setEnabled(false);
    ui->comboBox_2->setEnabled(false);
    ui->spinBox->setEnabled(false);

    frameWidth = ui->lineEdit_2->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_2->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-right: %1px; } ").arg(ui->toolButtonTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_2->setAlignment(Qt::AlignLeft);

    frameWidth = ui->lineEdit_3->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_3->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-right: %1px; } ").arg(ui->toolButtonTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_3->setAlignment(Qt::AlignLeft);

    frameWidth = ui->lineEdit_4->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_4->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-right: %1px; } ").arg(ui->toolButtonTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_4->setAlignment(Qt::AlignLeft);

    frameWidth = ui->lineEdit_5->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_5->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-right: %1px; } ").arg(ui->toolButtonTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_5->setAlignment(Qt::AlignLeft);

    frameWidth = ui->lineEdit_8->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_8->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-right: %1px; } ").arg(ui->toolButtonTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_8->setAlignment(Qt::AlignLeft);

    ui->textEdit->setAlignment(Qt::AlignTop);
    ui->textEdit->setAlignment(Qt::AlignLeft);

}

void cPaywaveEntryPoint::InitializeToolButton()
{
    QList<QToolButton*> listToolButton;

    listToolButton << ui->toolButtonTerCap << ui->toolButtonAddTerCap << ui->toolButtonTerTranQual
                   << ui->toolButtonKerConfig << ui->toolButtonBitEntrPoint;

    for (int i =0; i< listToolButton.count(); i++){
    listToolButton[i]->setStyleSheet("QToolButton{border:none; padding: 0px}");
    listToolButton[i]->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    listToolButton[i]->setIconSize(QSize(listToolButton[i]->width(), listToolButton[i]->height()));
    listToolButton[i]->setEnabled(false);
    }
}

void cPaywaveEntryPoint::CheckTagAvai(uint8_t *Buffertam, uint16_t LengTotal)
{
    int Length =0, Sobyte =0;
    if (LengTotal == 0){
        return ;
    }
    for (int i =0; i < LengTotal; i = i+ Sobyte +1 + Length){

        if ((Buffertam[i] & 0x1F) == 0x1F){
            if ((Buffertam[i+1] & 0x80) == 0x80){
                Sobyte = 3;
            }

            else {
                Sobyte = 2;
            }
        }

        else {
            Sobyte = 1;
        }

        if ((Buffertam[i + Sobyte ]== 0x81)){
            Sobyte += 1;
        }

        Length = Buffertam[i + Sobyte];
        UpdateData(i, Sobyte, Buffertam, Length);
    }
}

void cPaywaveEntryPoint::UpdateData(uint8_t Index, uint8_t Sobyte, uint8_t *Buffertam, uint8_t Length)
{
    qDebug() << "den day van on";
    if (Sobyte == 2){
        switch (Buffertam[Index]){
        case 0x9F:
            switch (Buffertam[Index+1]){
            case 0x1A:
                if (Length ==2){
                  qDebug() << "reader to update text";
                  mTLV.Updatetext(inpwTerCounCode, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    LineEditCheck[inpwTerCounCode]->setText("0000");
                    mTLV.UncheckCheckbox(inpwTerCounCode, splitBoolLineEdit);

                }
            break;

            case 0x33:
                if (Length ==3){
                  qDebug() << "reader to update text";
                  mTLV.Updatetext(inpwTerCap, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                  ui->toolButtonTerCap->setEnabled(true);
                }
                else{
                    LineEditCheck[inpwTerCap]->setText("000000");
                    mTLV.UncheckCheckbox(inpwTerCap, splitBoolLineEdit);

                }
            break;

            case 0x40:
                if (Length ==5){
                  qDebug() << "reader to update text";
                  mTLV.Updatetext(inpwAddTerCap, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                  ui->toolButtonAddTerCap->setEnabled(true);
                }
                else{
                    LineEditCheck[inpwAddTerCap]->setText("0000000000");
                    mTLV.UncheckCheckbox(inpwAddTerCap, splitBoolLineEdit);

                }
            break;

            case 0x66:
                if (Length ==4){
                  qDebug() << "reader to update text";
                  mTLV.Updatetext(inpwTerTranQual, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                  ui->toolButtonTerTranQual->setEnabled(true);
                }
                else{
                    LineEditCheck[inpwTerTranQual]->setText("00000000");
                    mTLV.UncheckCheckbox(inpwTerTranQual, splitBoolLineEdit);

                }
            break;
            case 0x09:
                if (Length ==2){
                   mTLV.Updatetext(inpwAppVerNum, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    LineEditCheck[inpwAppVerNum]->setText("0000");
                    mTLV.UncheckCheckbox(inpwAppVerNum, splitBoolLineEdit);
                }
            break;

            case 0x35:
                if (Length ==1){
                  qDebug() << "reader to update text";
                  mTLV.Updatetext(0, splitBoolComboBox, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    listCombo[0]->setCurrentText("00");
                    mTLV.UncheckCheckbox(0, splitBoolComboBox);
                }
            break;

            case 0x1B:
                if (Length ==1){
                  qDebug() << "reader to update text";
                  mTLV.Updatetext(0, splitBoolSpinBox, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    listSpinbox[0]->setValue(0);
                    mTLV.UncheckCheckbox(0, splitBoolSpinBox);

                }
            break;

            default:
                mTLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
             break;
            }
        break;

        case 0xDF:
            switch(Buffertam[Index+1]){

            case 0x1B:
                if (Length ==1){
                   mTLV.Updatetext(inpwKerConfig, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   ui->toolButtonKerConfig->setEnabled(true);
                }
                else{
                    LineEditCheck[inpwKerConfig]->setText("00");
                    mTLV.UncheckCheckbox(inpwKerConfig, splitBoolLineEdit);
                }
            break;

                case 0x30:
                   if (Length ==1){
                      mTLV.Updatetext(inpwBitmap, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                      ui->toolButtonBitEntrPoint->setEnabled(true);
                   }
                   else{
                      LineEditCheck[inpwBitmap]->setText("00");
                      mTLV.UncheckCheckbox(inpwBitmap, splitBoolLineEdit);
                   }
                break;

                case 0x2D:
                   if (Length ==3){
                     mTLV.Updatetext(inpwMessHoldTime, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       LineEditCheck[inpwMessHoldTime]->setText("000000");
                       mTLV.UncheckCheckbox(inpwMessHoldTime, splitBoolLineEdit);
                   }
                break;

                case 0x32:
                   if (Length == 1){
                     mTLV.updateComboType(ctStat);
                     mTLV.Updatetext(1, splitBoolComboBox, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       listCombo[1]->setCurrentText("not allowed (0x00)");
                       mTLV.UncheckCheckbox(1, splitBoolComboBox);
                   }
                break;

                case 0x23:
                  if (Length ==6){
                    mTLV.Updatetext(inpwReadContFloLimit, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                  }
                  else{
                      LineEditCheck[inpwReadContFloLimit]->setText("000000000000");
                      mTLV.UncheckCheckbox(inpwReadContFloLimit, splitBoolLineEdit);
                  }
                break;

                case 0x24:
                  if (Length ==6){
                    mTLV.Updatetext(inpwRCTL, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                  }
                  else{
                      LineEditCheck[inpwRCTL]->setText("000000000000");
                      mTLV.UncheckCheckbox(inpwRCTL, splitBoolLineEdit);
                  }
                break;

                case 0x26:
                    if (Length ==6){
                      mTLV.Updatetext(inpwReadCVMLimit, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                    }
                    else{
                        LineEditCheck[inpwReadCVMLimit]->setText("0000000000");
                        mTLV.UncheckCheckbox(inpwReadCVMLimit, splitBoolLineEdit);
                    }
                break;

                default:
                    mTLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
                break;

            }
            break;

        default:
            mTLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
            break;
        }
    }
}

void cPaywaveEntryPoint::UpdatePaywave()
{
    qDebug() << "emit ";
    CheckTagAvai(Buffertam, Length);
    ui->textEdit->setText(mTLV.text);
    qDebug() << "pass step Checktag Avai";
    mTLV.UncheckTag();
    qDebug() << "ok paywave";
}

