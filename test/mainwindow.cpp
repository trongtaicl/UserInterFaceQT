#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTabWidget>
#include <QTabBar>
#include <QToolButton>
#include <QDebug>
#include <QPixmap>
#include <QButtonGroup>
#include <QStyle>
#include <QListWidget>
#include <QComboBox>
#include <QLineEdit>
#include "cSha1.h"

/* task:
 * resize window khi them cac tool button
 * */

QString MainWindow::m_dir1 = "C:/Amadis/ACE-Client/Data/MASTERCARD/CL-MCHIP1/PROCESSING";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("SEPOSS");
    this->resize(1300, 800);
    this->setMinimumSize(1300, 800);
    ui->tabWidget->resize(1280, 780);

    m_dirDataStorage = "C:/";

    // define resource from class
    TermCap      = new cTerminalCapabilities(this);
    AddTerm      = new cAdditionalTerCapabilities(this);
    Def          = new cTerminalActionCode(this);
    PayPass      = new cPaypassEntryPoint(this);
    Paywave      = new cPaywaveEntryPoint(this);
    aceExit      = new cAceExit(this);
    admin        = new cAdministrative(this);
    helpShow     = new cHelpInfo(this);

    // tool button
    ButtonWelcome = new QToolButton(this);
    ButtonConfiguration = new QToolButton(this);
    ButtonBitmap = new QToolButton(this);
    ButtonTransaction = new QToolButton(this);
    ButtonTool = new QToolButton(this);
    ButtonTerminal = new QToolButton(this);
    ButtonSetup = new QToolButton(this);
    actionClose = new QAction(this);


    actionClose->setShortcut(Qt::Key_Q | Qt::CTRL);
    connect(actionClose, SIGNAL(triggered()), this, SLOT(close()));
    this->addAction(actionClose);

    // setting tabWidget
    ui->tabWidget->setTabPosition(QTabWidget::West);
    ui->tabWidget->setStyleSheet("QTabBar::tab{height:70px; width:80px;border: 0px;} QTabWidget::pane{border:none}");
    ui->tabWidget->setCurrentIndex(0);

    // setting tabwidget_2_Configuration
    ui->tabWidget_2->setStyleSheet("QTabBar::tab{height: 30px;width:140px; "
                                   "color: rgb(240, 240, 240); background-color: rgb(60, 60, 60);margin-left:2px}");

    // create a button group to do a loop
    group = new QButtonGroup(this);
    group->addButton(ButtonWelcome, 0);
    group->addButton(ButtonConfiguration, 1);
    group->addButton(ButtonTransaction, 2);
    group->addButton(ButtonBitmap, 3);
    group->addButton(ButtonTool, 4);
    group->addButton(ButtonTerminal, 5);
    group->addButton(ButtonSetup, 6);
    // get number of elements in group
    QList<QAbstractButton*> ListButton = group->buttons();
    Count = ListButton.count();

    ui->label_2->resize(1140, 780);
    // image for welcome
    QPixmap p(":/image/image/background1.jpg");
    ui->label_2->setPixmap(p);
    ui->label_2->setPixmap(p.scaled(1140, 780, Qt::KeepAspectRatio));

    ui->labelDir->setText(m_dir1);
    m_dir1 = "C:/";

    InitializeProcessingConfiguration();
    InitializeTerminalConfiguration();
    InitializeMainWindow();
    InitializeEntryPoint();
    InitializeDataStorage();
    InitializeSetup();
    InitializeDRLVisa();

    ui->spinBox->setEnabled(false);

    m_textAdmin = "000000000000000000000000000000"
                  "000000000000000000000000000001";

    qDebug() << this->width();
    qDebug() << ui->tabWidget_2->width();

    m_tagConfiguration << "9F1A" << "DF79" << "9F35" << "DF0A" << "9F33" << "9F40" << "DF55"
                       << "DF0B" << "DF27" << "DF06" << "DF08" << "DF7A" << "DF0D" << "DF10"
                       << "DF7B" << "DF07" << "DF09" << "DF73" << "DF74" << "DF75" << "DF53"
                       << "DF54" << "DF7C";


    // which button is clicked, go to that tab
    connect(group, SIGNAL(buttonClicked(int)), this, SLOT(GroupButtonClick(int)));

    // Button Terminal Capabilities is clicked, open new window
    connect(this, SIGNAL(ButtonTerminal_Clicked(QString)), TermCap, SLOT(ChangeTextEdit(QString)));

    //button additional is clicked, open new window
    connect(this, SIGNAL(ButtonAddition_Clicked(QString,mlsWinAdd_t)), AddTerm, SLOT(change_text_edit(QString,mlsWinAdd_t)));

    // button default is clicked, open new window
    connect(this, SIGNAL(ButtonDefault_Clicked(QString,mlsWindowDef_t)), Def,
            SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    // button online is clicked, open new window
    connect(this, SIGNAL(ButtonOnline_Clicked(QString,mlsWindowDef_t)), Def,
            SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(this, SIGNAL(Button_7_Clicked(QString,mlsWindowDef_t)), Def, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    // Ok button in new additional window is clicked, update text in additional
    connect(AddTerm, SIGNAL(ButtonOk_MainWindow_Clicked(QString)), this, SLOT(Additional_Update(QString)));

  // Ok button in new terminal window is clicked, update text in terminal capabilities
    connect(TermCap, SIGNAL(ButtonOk_Clicked(QString)), this, SLOT(Terminal_Update(QString)));

    connect(Def, SIGNAL(ButtonOk_Clicked(QString)), this, SLOT(Default_Update(QString)));

    connect(this, SIGNAL(UpdatePaypass(qint32)), PayPass, SLOT(UpdateData(qint32)));

    connect(this, SIGNAL(UpdatePaywave()), Paywave, SLOT(UpdatePaywave()));

   connect(PayPass, SIGNAL(buttonOk_clicked(qint32,QString)), this, SLOT(updateLineEditEntryPointConfig(qint32,QString)));

    connect(aceExit, SIGNAL(buttonOK_clicked()), this, SLOT(aceExitClicked()));

    connect(this, SIGNAL(adminShow(QString)), admin, SLOT(updateTextLineEdit(QString)));

    connect(admin, SIGNAL(closeButtonClicked(QString)), this, SLOT(closeAdmin(QString)));

    unsigned char buffer[2] = {0x70, 0x71};
    unsigned char result[20] = {0};
    tSHA     ct;
    ShaInit(&ct);
    ShaUpdate(&ct, buffer, 2);
    ShaFinal(&ct, result);
    for (unsigned char i : result)
    {
        qDebug() << QString::number(result[i], 16);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::resizeEvent(QResizeEvent *event)
{
    // initialize window and tabwidget
    ui->label_2->resize(ui->tabWidget->width(), ui->tabWidget->height());
    QPixmap p(":/image/image/background1.jpg");
    ui->label_2->setPixmap(p.scaled(ui->label_2->width(), ui->label_2->height(), Qt::IgnoreAspectRatio));

}

void MainWindow::GroupButtonClick(int Index)
{
    ui->tabWidget->setCurrentIndex(Index);
    group->button(Index)->setStyleSheet("QToolButton{border:1px solid gray; background-color: rgb(240, 240, 240)}");
    for (int i =0; i< Count ; i++){
        if (i != Index){
            group->button(i)->setStyleSheet("QToolButton{border:none}");
        }
    }
}

void MainWindow::Additional_Update(const QString &text)
{
    ui->lineEditAddition->setText(text);
}

void MainWindow::Terminal_Update(const QString &text)
{
    ui->lineEditTerminal->setText(text);
}

void MainWindow::Default_Update(const QString &text)
{  // use a new window for three buttons: default button, online button,
    switch (LineEditChosed){
     case mproTACDef:
        ui->lineEditDefaultProcessing->setText(text);
        break;
     case mproTACOnl:
        ui->lineEditOnlineProcessing->setText(text);
        break;
     case mproTACDen:
        ui->lineEditDenialProcessing->setText(text);
        break;
     case mterTACDef:
        ui->lineEditDefault->setText(text);
        break;
    case mterTACDen:
        ui->lineEditDenial->setText(text);
        break;
    case mterTACOnl:
        ui->lineEditOnline->setText(text);
        break;
    }
}

void MainWindow::OpenNewWindow(int Index)
{
    QString stringKerID = listComboKernelID[Index]->currentText();
    uint8_t bufferTemp[200];
    uint16_t length =0;
     if (stringKerID == "MasterCard"){
        for (int i =0; i< 200; i++){
            bufferTemp[i] =0;
        }

        length = CheckTag(ListLineEdit.at(Index)->text(), bufferTemp);
        PayPass->updateBuffer(length, bufferTemp);
        emit UpdatePaypass(Index);
        PayPass->show();
    }

    else if (stringKerID == "Visa"){
        for (int i =0; i< 200; i++){
            Paywave->Buffertam[i] =0;
        }
        Paywave->Length = CheckTag(ListLineEdit.at(Index)->text(), Paywave->Buffertam);
        emit UpdatePaywave();
        Paywave->show();
    }

}

void MainWindow::updateLineEditEntryPointConfig(qint32 index, QString text)
{
    ListLineEdit.at(index)->setText(text);
}

void MainWindow::listWidgetItemChanged()
{

}

void MainWindow::aceExitClicked()
{
    this->close();
}

void MainWindow::closeAdmin(const QString &text)
{
    m_textAdmin = text;
}

void MainWindow::on_toolButtonAddition_clicked()
{
    AddTerm->show();
    emit ButtonAddition_Clicked(ui->lineEditAddition->text(), waMainWindow);
}

void MainWindow::on_toolButtonDefault_clicked()
{
    Def->show();
    LineEditChosed = mterTACDef;
    emit ButtonDefault_Clicked(ui->lineEditDefault->text(), wdMainWindow);
}

void MainWindow::on_toolButtonOnline_clicked()
{
    Def->show();
    LineEditChosed = mterTACOnl;
    emit ButtonOnline_Clicked(ui->lineEditOnline->text(), wdMainWindow);
}

void MainWindow::on_toolButton_7_clicked()
{
    Def->show();
    LineEditChosed = mterTACDen;
    emit Button_7_Clicked(ui->lineEditDenial->text(), wdMainWindow);
}

void MainWindow::on_checkBoxDefault_clicked()
{
    bool State =false;

    State = ui->checkBoxDefault->checkState();
    ui->toolButtonDefault->setEnabled(State);
    ui->lineEditDefault->setEnabled(State);
    ui->toolButtonOnline->setEnabled(State);
    ui->lineEditOnline->setEnabled(State);
    ui->toolButton_7->setEnabled(State);
    ui->lineEditDenial->setEnabled(State);
}



void MainWindow::on_toolButtonTerminal_clicked()
{
    TermCap->show();
    emit ButtonTerminal_Clicked(ui->lineEditTerminal->text());
}

void MainWindow::InitializeTerminalConfiguration()
{
    // add button open
    ui->ButtonOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonOpen->setFixedSize(QSize(60, 70));
    ui->ButtonOpen->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->ButtonOpen->setIconSize(QSize(50, 50));
    ui->ButtonOpen->setText("Open");
    ui->ButtonOpen->setToolTip("Open");

    // add button save
    ui->ButtonSave->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSave->setFixedSize(QSize(60, 70));
    ui->ButtonSave->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSave->setIconSize(QSize(50, 50));
    ui->ButtonSave->setText("Save");
    ui->ButtonSave->setToolTip("Save");
    ui->ButtonSave->setEnabled(false);

    // add button save as
    ui->ButtonSaveAs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSaveAs->setFixedSize(QSize(60, 70));
    ui->ButtonSaveAs->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSaveAs->setIconSize(QSize(50, 50));
    ui->ButtonSaveAs->setText("Save As");
    ui->ButtonSaveAs->setToolTip("Save As");

    // add button send
    ui->ButtonSend->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSend->setFixedSize(QSize(60, 70));
    ui->ButtonSend->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->ButtonSend->setIconSize(QSize(50, 50));
    ui->ButtonSend->setText("Send");
    ui->ButtonSend->setToolTip("Send");

    // initialize checkbox that is check already
    ui->checkBoxEMV1->setChecked(true);
    ui->checkBoxOnline->setChecked(true);
    ui->checkBoxReferral->setChecked(true);
    ui->checkBoxAdvice->setChecked(true);
    ui->checkBoxEMV2->setChecked(true);
    ui->checkBoxCardHolder->setChecked(true);

        // combo box
    QStringList ListTerminal;
    ListTerminal << "00" << "11" << "12" <<
                    "13" << "14" << "15" << "16" << "21" << "22" << "23" <<
                    "24" << "25" << "26" << "34" << "35" << "36";

    for (int i =0; i <ListTerminal.count(); i++){
        ui->comboBoxTerminal->addItem(ListTerminal.at(i));
    }
    QList<QString> ListCDA;
    ListCDA <<"UNDEFINED" << "MODE_1" << "MODE_2" << "MODE_3" << "MODE_4";
    for (int i =0; i < ListCDA.count(); i++){
    ui->comboBoxCDA->addItem(ListCDA.at(i));
    }

    // insert button to line edit Terminal Capabilities, Additional Terminal

    // button terminal capabilities
    ui->toolButtonTerminal->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonTerminal->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonTerminal->setIconSize(QSize(ui->toolButtonTerminal->width(), ui->toolButtonTerminal->height()));
    ui->toolButtonTerminal->setToolTip("Tag 9F33");

     int frameWidth;
    //line edit terminal capabilitie
    frameWidth = ui->lineEditTerminal->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditTerminal->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonTerminal->sizeHint().width() + frameWidth + 3));
    ui->lineEditTerminal->setMaxLength(6);
    ui->lineEditTerminal->setText("000000");
    ui->lineEditTerminal->setInputMask("hhhhhh");
    TermCap->TextLineEdit = "000000";
    ui->lineEditTerminal->setAlignment(Qt::AlignRight);

    // button additional
    ui->toolButtonAddition->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonAddition->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonAddition->setIconSize(QSize(ui->toolButtonAddition->width(), ui->toolButtonAddition->height()));
    ui->toolButtonAddition->setToolTip("Tag 9F40");

    // line edit additional
    frameWidth = ui->lineEditAddition->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditAddition->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonAddition->sizeHint().width() + frameWidth + 3));
    ui->lineEditAddition->setMaxLength(10);
    ui->lineEditAddition->setText("0000000000");
    ui->lineEditAddition->setInputMask("hhhhhhhhhh");
    ui->lineEditAddition->setAlignment(Qt::AlignRight);

    // button default
    ui->toolButtonDefault->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonDefault->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonDefault->setIconSize(QSize(ui->toolButtonDefault->width(), ui->toolButtonDefault->height()));
    ui->toolButtonDefault->setToolTip("Tag DF74");

    // line edit default
    frameWidth = ui->lineEditDefault->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDefault->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEditDefault->setMaxLength(10);;
    ui->lineEditDefault->setText("0000000000");
    ui->lineEditDefault->setInputMask("hhhhhhhhhh");
    ui->lineEditDefault->setAlignment(Qt::AlignRight);

    // button online
    ui->toolButtonOnline->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonOnline->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonOnline->setIconSize(QSize(ui->toolButtonOnline->width(), ui->toolButtonOnline->height()));
    ui->toolButtonOnline->setToolTip("Tag DF73");

    // line edit online
    frameWidth = ui->lineEditOnline->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditOnline->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonOnline->sizeHint().width() + frameWidth + 3));
    ui->lineEditOnline->setMaxLength(10);
    ui->lineEditOnline->setText("0000000000");
    ui->lineEditOnline->setInputMask("hhhhhhhhhh");
    ui->lineEditOnline->setAlignment(Qt::AlignRight);

    // button 7
    ui->toolButton_7->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButton_7->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButton_7->setIconSize(QSize(ui->toolButton_7->width(), ui->toolButton_7->height()));
    ui->toolButton_7->setToolTip("Tag DF75");

    // line edit 7
    frameWidth = ui->lineEditDenial->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDenial->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButton_7->sizeHint().width() + frameWidth + 3));
    ui->lineEditDenial->setMaxLength(10);
    ui->lineEditDenial->setText("0000000000");
    ui->lineEditDenial->setInputMask("hhhhhhhhhh");
    ui->lineEditDenial->setAlignment(Qt::AlignRight);

    // line country code and Pintimeout
    ui->lineEditTerCounCode->setText("0000");
    ui->lineEditPINTimeout->setText("00");

    ui->toolButtonDefault->setEnabled(false);
    ui->lineEditDefault->setEnabled(false);
    ui->toolButtonOnline->setEnabled(false);
    ui->lineEditOnline->setEnabled(false);
    ui->toolButton_7->setEnabled(false);
    ui->lineEditDenial->setEnabled(false);
}

void MainWindow::InitializeMainWindow()
{
    // create a tool button welcome

        ButtonWelcome->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 32));
        ButtonWelcome->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonWelcome->setFixedSize(QSize(80, 80));
        ButtonWelcome->setIcon(QIcon(":/image/image/welcome.png"));
        ButtonWelcome->setIconSize(QSize(50, 70));
        ButtonWelcome->setText("Welcome");
        ButtonWelcome->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonWelcome->setToolTip("Welcome");

        // create a tool button configuration

        ButtonConfiguration->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, ButtonWelcome->height()) + QPoint(0, 32));
        ButtonConfiguration->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonConfiguration->setFixedSize(QSize(80, 80));
        ButtonConfiguration->setIcon(QIcon(":/image/image/configuration.png"));
        ButtonConfiguration->setIconSize(QSize(50, 70));
        ButtonConfiguration->setText("Configuration");
        ButtonConfiguration->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonConfiguration->setToolTip("Configuration");

        // create a tool button transaction

        ButtonTransaction->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 2*ButtonConfiguration->height()) + QPoint(0, 32));
        ButtonTransaction->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonTransaction->setFixedSize(QSize(80, 80));
        ButtonTransaction->setIcon(QIcon(":/image/image/transaction.png"));
        ButtonTransaction->setIconSize(QSize(50, 70));
        ButtonTransaction->setText("Transaction");
        ButtonTransaction->setStyleSheet("QToolButton{border:none} QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonTransaction->setToolTip("Display transaction page");

        // creat a bitmap toolbutton

        ButtonBitmap->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 3*ButtonTransaction->height()) + QPoint(0, 32));
        ButtonBitmap->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonBitmap->setFixedSize(QSize(80, 80));
        ButtonBitmap->setIcon(QIcon(":/image/image/bitmap.png"));
        ButtonBitmap->setIconSize(QSize(50, 70));
        ButtonBitmap->setText("Bitmap");
        ButtonBitmap->setStyleSheet("QToolButton{border: none} QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonBitmap->setToolTip("Display bitmap page");

        // create a tools button

        ButtonTool->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 4*ButtonBitmap->height()) + QPoint(0, 32));
        ButtonTool->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonTool->setFixedSize(QSize(80, 80));
        ButtonTool->setIcon(QIcon(":/image/image/tool.jpg"));
        ButtonTool->setIconSize(QSize(50, 70));
        ButtonTool->setText("Tools");
        ButtonTool->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonTool->setToolTip("Display tool page");

        // create terminal button

        ButtonTerminal->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 5*ButtonTool->height()) + QPoint(0, 32));
        ButtonTerminal->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonTerminal->setFixedSize(QSize(80, 80));
        ButtonTerminal->setIcon(QIcon(":/image/image/terminal.png"));
        ButtonTerminal->setIconSize(QSize(50, 60));
        ButtonTerminal->setText("Terminal");
        ButtonTerminal->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonTerminal->setToolTip("Terminal UI");

        // create setup button

        ButtonSetup->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 6*ButtonTerminal->height()) + QPoint(0, 35));
        ButtonSetup->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonSetup->setFixedSize(QSize(80, 80));
        ButtonSetup->setIcon(QIcon(":/image/image/setup.jpg"));
        ButtonSetup->setIconSize(QSize(50, 70));
        ButtonSetup->setText("Setup");
        ButtonSetup->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonSetup->setToolTip("Display setup page");
}

void MainWindow::InitializeProcessingConfiguration()
{
    // add button open
    ui->ButtonOpenProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonOpenProcessing->setFixedSize(QSize(60, 70));
    ui->ButtonOpenProcessing->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->ButtonOpenProcessing->setIconSize(QSize(50, 50));
    ui->ButtonOpenProcessing->setText("Open");
    ui->ButtonOpenProcessing->setToolTip("open configuration");

    // add button save
    ui->ButtonSaveProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSaveProcessing->setFixedSize(QSize(60, 70));
    ui->ButtonSaveProcessing->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSaveProcessing->setIconSize(QSize(50, 50));
    ui->ButtonSaveProcessing->setText("Save");
    ui->ButtonSaveProcessing->setToolTip("save configuration");
    ui->ButtonSaveProcessing->setEnabled(false);

    // add button save as
    ui->ButtonSaveAsProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSaveAsProcessing->setFixedSize(QSize(60, 70));
    ui->ButtonSaveAsProcessing->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSaveAsProcessing->setIconSize(QSize(50, 50));
    ui->ButtonSaveAsProcessing->setText("Save As");
    ui->ButtonSaveAsProcessing->setToolTip("Save As configuration");

    // add button send
    ui->ButtonSendProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSendProcessing->setFixedSize(QSize(60, 70));
    ui->ButtonSendProcessing->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->ButtonSendProcessing->setIconSize(QSize(50, 50));
    ui->ButtonSendProcessing->setText("Send");
    ui->ButtonSendProcessing->setToolTip("Send configuration");

    // add button ACE
    ui->ButtonACEProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonACEProcessing->setFixedSize(QSize(60, 70));
    ui->ButtonACEProcessing->setIcon(QIcon(":/image_open_save/image/ace_icon.png"));
    ui->ButtonACEProcessing->setIconSize(QSize(50, 50));
    ui->ButtonACEProcessing->setText("ACE");
    ui->ButtonACEProcessing->setToolTip("Get processing from ACE");

    // add button plus
    ui->toolButtonAddProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonAddProcessing->setIcon(QIcon(":/image_open_save/image/plus1.png"));
    ui->toolButtonAddProcessing->setIconSize(QSize(30, 30));

    // add button minus
    ui->toolButtonRemoveProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonRemoveProcessing->setIcon(QIcon(":/image_open_save/image/minus1.png"));
    ui->toolButtonRemoveProcessing->setIconSize(QSize(30, 30));

    // add button up
    ui->toolButtonUpProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonUpProcessing->setIcon(QIcon(":/image_open_save/image/up1.png"));
    ui->toolButtonUpProcessing->setIconSize(QSize(30, 30));

    // add button down
    ui->toolButtonDownProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonDownProcessing->setIcon(QIcon(":/image_open_save/image/down.png"));
    ui->toolButtonDownProcessing->setIconSize(QSize(30, 30));

    // button Denial
    ui->toolButtonDenialProcessing->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonDenialProcessing->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonDenialProcessing->setIconSize(QSize(ui->toolButtonDenialProcessing->width(), ui->toolButtonDenialProcessing->height()));
    ui->toolButtonDenialProcessing->setToolTip("Tag DF21");

    // line edit Denial
    int frameWidth = ui->lineEditDenialProcessing->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDenialProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; padding-right: %1px; } ").arg(ui->toolButtonDenialProcessing->sizeHint().width() + frameWidth + 3));
    ui->lineEditDenialProcessing->setMaxLength(10);
    ui->lineEditDenialProcessing->setText("0000000000");
    ui->lineEditDenialProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditDenialProcessing->setAlignment(Qt::AlignRight);

    // button Online
    ui->toolButtonOnlineProcessing->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonOnlineProcessing->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonOnlineProcessing->setIconSize(QSize(ui->toolButtonOnlineProcessing->width(), ui->toolButtonOnlineProcessing->height()));
    ui->toolButtonOnlineProcessing->setToolTip("Tag DF22");

    // line edit Online
    frameWidth = ui->lineEditOnlineProcessing->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditOnlineProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; padding-right: %1px; } ").arg(ui->toolButtonDenialProcessing->sizeHint().width() + frameWidth + 3));
    ui->lineEditOnlineProcessing->setMaxLength(10);
    ui->lineEditOnlineProcessing->setText("0000000000");
    ui->lineEditOnlineProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditOnlineProcessing->setAlignment(Qt::AlignRight);

    // button Default
    ui->toolButtonDefaultProcessing->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonDefaultProcessing->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonDefaultProcessing->setIconSize(QSize(ui->toolButtonDefaultProcessing->width(), ui->toolButtonDefaultProcessing->height()));
    ui->toolButtonDefaultProcessing->setToolTip("Tag DF20");

    // line edit Default
    frameWidth = ui->lineEditDefaultProcessing->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDefaultProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonDefaultProcessing->sizeHint().width() + frameWidth + 3));
    ui->lineEditDefaultProcessing->setMaxLength(10);
    ui->lineEditDefaultProcessing->setText("0000000000");
    ui->lineEditDefaultProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditDefaultProcessing->setAlignment(Qt::AlignRight);

    ui->lineEditAcquireIDProcessing->setText("0000000000");
    ui->lineEditAcquireIDProcessing->setMaxLength(10);
    ui->lineEditAcquireIDProcessing->setInputMask("hhhhhhhhhh");

    ui->lineEditAVNProcessing->setText("0000");
    ui->lineEditAVNProcessing->setInputMask("hhhh");

    ui->lineEditProcessingFloor->setText("0");
    ui->lineEditProcessingThresh->setText("0");


    QRegExp rx("[A-Fa-f1-9]{0,256}");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->lineEditMainProcessing->setValidator(validator);
    ui->lineEditMainProcessing->setCursorPosition(0);
    ui->lineEditProcessingTCC->setText("0000");
    ui->lineEditProcessingExpo->setText("0");
    ui->lineEditProcessingThresh->setText("0");

    ui->lineEdit_DDOL->setValidator(validator);
    ui->lineEdit_TDOL->setValidator(validator);

    ui->lineEditDenialProcessing->setEnabled(false);
    ui->lineEditDefaultProcessing->setEnabled(false);
    ui->lineEditOnlineProcessing->setEnabled(false);
    ui->toolButtonDefaultProcessing->setEnabled(false);
    ui->toolButtonOnlineProcessing->setEnabled(false);
    ui->toolButtonDenialProcessing->setEnabled(false);

}

void MainWindow::InitializeEntryPoint()
{
    int colCount = 4;
    int rowCount = 12;
    numRowTable = rowCount;

    // add button open
    ui->ButtonEntryOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntryOpen->setFixedSize(QSize(60, 70));
    ui->ButtonEntryOpen->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->ButtonEntryOpen->setIconSize(QSize(50, 50));
    ui->ButtonEntryOpen->setText("Open");
    ui->ButtonEntryOpen->setToolTip("open configuration");

    // add button save
    ui->ButtonEntrySave->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntrySave->setFixedSize(QSize(60, 70));
    ui->ButtonEntrySave->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonEntrySave->setIconSize(QSize(50, 50));
    ui->ButtonEntrySave->setText("Save");
    ui->ButtonEntrySave->setToolTip("save configuration");
    ui->ButtonEntrySave->setEnabled(false);

    // add button save as
    ui->ButtonEntrySaveAs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntrySaveAs->setFixedSize(QSize(60, 70));
    ui->ButtonEntrySaveAs->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonEntrySaveAs->setIconSize(QSize(50, 50));
    ui->ButtonEntrySaveAs->setText("Save As");
    ui->ButtonEntrySaveAs->setToolTip("Save As configuration");

    // add button send
    ui->ButtonEntrySend->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntrySend->setFixedSize(QSize(60, 70));
    ui->ButtonEntrySend->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->ButtonEntrySend->setIconSize(QSize(50, 50));
    ui->ButtonEntrySend->setText("Send");
    ui->ButtonEntrySend->setToolTip("Send configuration");

    // add button ACE
    ui->ButtonEntryACE->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntryACE->setFixedSize(QSize(60, 70));
    ui->ButtonEntryACE->setIcon(QIcon(":/image_open_save/image/ace_icon.png"));
    ui->ButtonEntryACE->setIconSize(QSize(50, 50));
    ui->ButtonEntryACE->setText("ACE");
    ui->ButtonEntryACE->setToolTip("Get processing from ACE");

    // add button plus
    ui->toolButtonEntryAdd->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryAdd->setIcon(QIcon(":/image_open_save/image/plus1.png"));
    ui->toolButtonEntryAdd->setIconSize(QSize(30, 30));

    // add button minus
    ui->toolButtonEntryRemove->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryRemove->setIcon(QIcon(":/image_open_save/image/minus1.png"));
    ui->toolButtonEntryRemove->setIconSize(QSize(30, 30));

    // add button up
    ui->toolButtonEntryUp->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryUp->setIcon(QIcon(":/image_open_save/image/up1.png"));
    ui->toolButtonEntryUp->setIconSize(QSize(30, 30));

    // add button down
    ui->toolButtonEntryDown->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryDown->setIcon(QIcon(":/image_open_save/image/down.png"));
    ui->toolButtonEntryDown->setIconSize(QSize(30, 30));

    ui->tableWidget->setColumnCount(colCount);
    ui->tableWidget->setRowCount(rowCount);

    QTableWidgetItem *headerKernelID = new QTableWidgetItem("Kernel ID");
    ui->tableWidget->setHorizontalHeaderItem(0, headerKernelID);

    QTableWidgetItem *headerAID = new QTableWidgetItem("AID Id");
    ui->tableWidget->setHorizontalHeaderItem(1, headerAID);

    QTableWidgetItem *headerTranType = new QTableWidgetItem("Transaction Type");
    ui->tableWidget->setHorizontalHeaderItem(2, headerTranType);

    ui->tableWidget->setColumnWidth(0, 130);
    ui->tableWidget->setColumnWidth(1, 60);
    ui->tableWidget->setColumnWidth(2, 150);
    ui->tableWidget->setColumnWidth(3, 2000);
    ui->tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    for (int row =0; row< rowCount; row++){
        QComboBox *comboBoxKernelID = new QComboBox(ui->tableWidget);
        QStringList kernelIDName;
        kernelIDName << "KERNEL01" << "MasterCard" << "Visa" << "American Express" << "JCB" << "Discover" << "C.U.P"
                     << "Interac FWM 3.1 " << "KERNEL09" << "KERNEL0A" << "KERNEL0B" << "KERNEL0C" << "KERNEL0D"
                     << "KERNEL0E" <<"KERNEL0F";

        for (int i =16; i<=64; i++){
            kernelIDName <<"KERNEL" + QString::number(i, 16).toUpper();
        }

        kernelIDName << "Interac (FMW 3.2) " << "Eftpos (FMW 3.2)" ;

        for (int i = 67; i<= 255; i++){
            kernelIDName << "KERNEL" + QString::number(i, 16).toUpper();
        }

        for (int i =0; i < kernelIDName.count(); i++){
            comboBoxKernelID->addItem(QString(kernelIDName.at(i)));
        }

        listComboKernelID << comboBoxKernelID;
        ui->tableWidget->setCellWidget(row, 0, comboBoxKernelID);

    }
    for (int row =0; row< rowCount; row++){
        QComboBox *comboBoxTransaction = new QComboBox(ui->tableWidget);
        QStringList transactionName;

        transactionName << "PURCHASE" << "CASH" << "PURCHASE ...CASHBACK" << "REFUND" << "MANUAL CASH"
                        << "QUASI CASH" << "DEPOSIT" << "INQUIRY" << "PAYMENT" << "TRANSFER" << "ADMINISTRATIVE"
                        << "CLEAN" << "RETRIEVAL" << "UPDATE" << "AUTHENTICATION" << "CASH DISBURSEMENT" << "PRE AUTHORIZATION";

        for (int i =0; i < transactionName.count(); i++){
            comboBoxTransaction->addItem(QString(transactionName.at(i)));
        }

        listComboTranType << comboBoxTransaction;
        ui->tableWidget->setCellWidget(row, 2, comboBoxTransaction);
    }

    for (int row =0; row < rowCount; row++){
        QComboBox *comboBoxAID = new QComboBox(ui->tableWidget);
        comboBoxAID->addItem("1");
        comboBoxAID->addItem("2");
        comboBoxAID->addItem("3");

        listComboAID << comboBoxAID;
        ui->tableWidget->setCellWidget(row, 1, comboBoxAID);
    }

    /**/
    sigMapp = new QSignalMapper();

    for (int i =0; i < 12; i++){
    QLineEdit *LineEdit = new QLineEdit(ui->tableWidget);
    LineEdit->setText("DF1A039F6A04DF0C0102DF1E0110DF2C0100DF2406000000030000DF2506000000050000"
                      "DF26060000000010009F1A0200569F7E009F400500000000009F09020002DF170100DF180160"
                      "DF190108DF1B0120DF1C020000DF1D0100DF2306000000010000DF030108DF20050000000000"
                      "DF21050000000000DF220500000000009F3501225F57009F01009F150201309F16009F4E009F3300"
                      "9F1C00");

    ui->tableWidget->setCellWidget(i, 3, LineEdit);

    QToolButton *Button = new QToolButton(LineEdit);

    ListToolButton << Button;
    ListLineEdit << LineEdit;

    if (i <2){
    Button->move(QPoint(5,i+3));
    }
    else if (i <7){
        Button->move(QPoint(5, i));
    }
    else {
        Button->move(QPoint(5, i-3));
    }

    Button->setFixedSize(30, 20);
    Button->setStyleSheet("QToolButton{border:none; padding: 0px}");
    Button->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    Button->setIconSize(QSize(Button->width(), Button->height()));
    Button->setToolTip("Tag FF35");

    int frameWidth = LineEdit->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    LineEdit->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; padding-left: %1px; } ").arg(Button->sizeHint().width() + frameWidth + 3));

    sigMapp->setMapping(ListToolButton[i], i);
    connect(ListToolButton[i], SIGNAL(clicked()), sigMapp, SLOT(map()));
  }

    ListToolButton[6]->move(QPoint(ListToolButton[6]->x(), ListToolButton[6]->y()-2));
    ListToolButton[10]->move(QPoint(ListToolButton[10]->x(), ListToolButton[10]->y() -3));
    ListToolButton[11]->move(QPoint(ListToolButton[11]->x(), ListToolButton[11]->y() -3));

    connect(sigMapp, SIGNAL(mapped(int)), this, SLOT(OpenNewWindow(int)));


}

void MainWindow::InitializeDataStorage()
{
    // add button load DEK
    ui->toolButtonLoadDEK->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButtonLoadDEK->setFixedSize(QSize(70, 70));
    ui->toolButtonLoadDEK->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->toolButtonLoadDEK->setIconSize(QSize(50, 50));
    ui->toolButtonLoadDEK->setText("Load DEK DET");
    ui->toolButtonLoadDEK->setToolTip("Load DEK DET");

    // add button clear DEK
    ui->toolButtonClearDEK->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButtonClearDEK->setFixedSize(QSize(70, 70));
    ui->toolButtonClearDEK->setIcon(QIcon(":/image_open_save/image/xicon.png"));
    ui->toolButtonClearDEK->setIconSize(QSize(50, 50));
    ui->toolButtonClearDEK->setText("Clear DEK DET");
    ui->toolButtonClearDEK->setToolTip("Clear DEK DET");

}

void MainWindow::InitializeSetup()
{
    ui->checkBox_SHA1->setChecked(true);
    ui->toolButton_TTQ->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButton_TTQ->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButton_TTQ->setIconSize(QSize(ui->toolButton_TTQ->width(), ui->toolButton_TTQ->height()));

    ui->lineEdit_defaultTTQ->setInputMask("hhhhhhhh");
    ui->lineEdit_defaultTTQ->setText("00000000");
    ui->lineEdit_defaultTTQ->setEnabled(false);
    ui->toolButton_TTQ->setEnabled(false);

    ui->checkBox_autoClear->setChecked(true);

    QList<int> ListReferral;
    ListReferral << 3031 << 3032;

    for (int i =0; i <ListReferral.count(); i++){
        ui->comboBox_ManageACE->addItem(QString::number(ListReferral.at(i)));
    }

    ui->comboBox_ManageACE->setEnabled(false);
    ui->lineEdit_defaultPIN->setText("1234");

}

void MainWindow::InitializeDRLVisa()
{
    // add button open
    ui->toolButton_DRLOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLOpen->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLOpen->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->toolButton_DRLOpen->setIconSize(QSize(50, 50));
    ui->toolButton_DRLOpen->setText("Open");
    ui->toolButton_DRLOpen->setToolTip("open configuration");

    // add button save
    ui->toolButton_DRLSave->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLSave->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLSave->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->toolButton_DRLSave->setIconSize(QSize(50, 50));
    ui->toolButton_DRLSave->setText("Save");
    ui->toolButton_DRLSave->setToolTip("save configuration");
    ui->toolButton_DRLSave->setEnabled(false);

    // add button save as
    ui->toolButton_DRLSaveAs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLSaveAs->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLSaveAs->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->toolButton_DRLSaveAs->setIconSize(QSize(50, 50));
    ui->toolButton_DRLSaveAs->setText("Save As");
    ui->toolButton_DRLSaveAs->setToolTip("Save As configuration");

    // add button send
    ui->toolButton_DRLSend->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLSend->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLSend->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->toolButton_DRLSend->setIconSize(QSize(50, 50));
    ui->toolButton_DRLSend->setText("Send");
    ui->toolButton_DRLSend->setToolTip("Send configuration");

    // add button ACE
    ui->toolButton_DRLACE->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLACE->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLACE->setIcon(QIcon(":/image_open_save/image/ace_icon.png"));
    ui->toolButton_DRLACE->setIconSize(QSize(50, 50));
    ui->toolButton_DRLACE->setText("ACE");
    ui->toolButton_DRLACE->setToolTip("Get processing from ACE");

    // add button plus
    ui->toolButton_DRLAdd->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLAdd->setIcon(QIcon(":/image_open_save/image/plus1.png"));
    ui->toolButton_DRLAdd->setIconSize(QSize(30, 30));

    // add button minus
    ui->toolButton_DRLRemove->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLRemove->setIcon(QIcon(":/image_open_save/image/minus1.png"));
    ui->toolButton_DRLRemove->setIconSize(QSize(30, 30));

    // add button up
    ui->toolButton_DRLUp->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLUp->setIcon(QIcon(":/image_open_save/image/up1.png"));
    ui->toolButton_DRLUp->setIconSize(QSize(30, 30));

    // add button down
    ui->toolButton_DRLDown->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLDown->setIcon(QIcon(":/image_open_save/image/down.png"));
    ui->toolButton_DRLDown->setIconSize(QSize(30, 30));

    QRegExp rx("[A-Fa-f1-9]{0,256}");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->lineEdit_DRL->setValidator(validator);
    ui->lineEdit_DRL->setCursorPosition(0);

    ui->lineEdit_DRLFloorLimit->setInputMask("hhhhhhhhhhhh");
    ui->lineEdit_DRLFloorLimit->setText("000000000000");

    ui->lineEdit_DRLTransLimit->setInputMask("hhhhhhhhhhhh");
    ui->lineEdit_DRLTransLimit->setText("000000000000");

    ui->lineEdit_DRLRequiredLimit->setInputMask("hhhhhhhhhhhh");
    ui->lineEdit_DRLRequiredLimit->setText("000000000000");

    QStringList listItem;

    listItem << "not allowed (0x00)" << "online cryptogram request "
             << "not allowed (0x02)";

    for (qint32 i =0; i< listItem.length(); i++)
    {
        ui->comboBox_DRLZero->addItem(listItem.at(i));
    }
}

void MainWindow::updateDir(QString dir)
{
    qint32 index =0;

    index = dir.lastIndexOf("/");

    m_dir1 = dir.remove(index+1, dir.length()-index-1);
}

/**
 * @brief MainWindow::CheckTag
 * a buffer contains all the correct TLV and length of that buffer
 * @param text
 * @param Buffertam
 * @return
 */
uint16_t MainWindow::CheckTag(const QString &text, uint8_t *Buffertam)
{
    uint16_t LengTotal =0;
    int Sobyte =0, Length =0;

    int StringSize =0;

    StringSize = text.size();

    if (StringSize ==0){
        return 0;
    }
    if ((StringSize %2)!= 0){
        StringSize -= 1;
    }
    uint8_t TextToHex[BUFFER_SIZE];

    /* convert two charater into number */
    for (int i =0; i< StringSize; i +=2)
    {
        TextToHex[i/2] = text.mid(i,2).toInt(NULL, 16);
    }

    for (int i =0 ; i< StringSize/2; i = i+ Sobyte + 1 + Length){

        if ((i+1) < StringSize/2){

            if ((TextToHex[i] & 0x1F) == 0x1F){

                if ((TextToHex[i+1] & 0x80) == 0x80){
                Sobyte = 3;
            }
            else Sobyte = 2;
        }

        else Sobyte = 1;
        }
        else Sobyte =0;

        if ((Sobyte != 0) && ((Sobyte+i) < StringSize/2)){

        if ((TextToHex[i+ Sobyte]) == 0x81){
            Sobyte += 1;
        }
        if ((Sobyte + i) < StringSize/2){
             Length = TextToHex[Sobyte+i];

          if ((Sobyte + i + 1 + Length) <= StringSize/2){

             for (int j =0; j<Sobyte+1; j++){
                 *Buffertam++ = TextToHex[i+j];
              }

             for (int k =0; k < Length; k++){
                 *Buffertam++ = TextToHex[k+Sobyte+i+1];
             }
             }
          LengTotal += Length + 1 + Sobyte;
          }
    }
}

    return LengTotal;
}

bool MainWindow::CheckTagAvai(quint8 *Buffertam, quint16 LengTotal, QString &error)
{
    int length =0, numByte =0;
    QString err;
    QStringList listTag;
    bool state;
    for (int i =0; i < LengTotal; i = i+ numByte +1 + length)
    {
        QString stringTemp ="";
        if ((Buffertam[i] & 0x1F) == 0x1F)
        {
            if ((Buffertam[i+1] & 0x80) == 0x80)
            {
                numByte = 3;
            }
            else
            {
                numByte = 2;
            }
        }
        else
        {
            numByte = 1;
        }

        length = Buffertam[i + numByte];
        state = updateCheckBoxLineSpin(i, numByte, Buffertam, length, err);
        if (!state)
        {
            error = err;
            return false;
        }

        if (numByte >1)
        {
            if (Buffertam[i] < 0x0F)
            {
                stringTemp += "0";
            }
            stringTemp += QString::number(Buffertam[i], 16);
            if (Buffertam[i+1] < 0x0F)
            {
                stringTemp += "0";
            }
            stringTemp+= QString::number(Buffertam[i+1], 16);
            listTag << stringTemp;
        }
    }


    return true;
}

bool MainWindow::updateCheckBoxLineSpin(quint8 index, quint8 numByte, quint8 *Buffertam,
                                        quint8 Length, QString &error)
{
    QString stringNeeded;
    quint64  valueSpin =0;
    bool    state;

    if (numByte == 2){
        switch (Buffertam[index]){
        case 0x9F:
            switch (Buffertam[index+1]){
            case 0x1A:
                stringNeeded ="";
                if (Length !=2)
                {
                    error = "9F1A";
                    return false;
                }

                for (qint32 i =0; i < Length; i++)
                {
                    if (Buffertam[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(Buffertam[index+numByte+1+i], 16));
                }
                ui->lineEditTerCounCode->setText(stringNeeded);
                break;

            case 0x35:
                stringNeeded ="";
                if (Length !=1)
                {
                    error = "9F35";
                    return false;
                }

                if (Buffertam[index+numByte+1] < 0x0F)
                {
                    stringNeeded.append("0");
                }

                stringNeeded = QString::number(Buffertam[index+numByte+1], 16);
                state = false;
                for (qint32 i =0; i< ui->comboBoxTerminal->count(); i++)
                {
                    if (stringNeeded == ui->comboBoxTerminal->itemText(i))
                    {
                        state = true;
                        break;
                    }
                }

                if (!state)
                {
                    error = "9F35";
                    return false;
                }
                ui->comboBoxTerminal->setCurrentText(stringNeeded);
                break;

            case 0x33:
                stringNeeded ="";
                if (Length !=3)
                {
                    error = "9F33";
                    return false;
                }

                for (qint32 i =0; i < Length; i++)
                {
                    if (Buffertam[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(Buffertam[index+numByte+1+i], 16));
                }
                ui->lineEditTerminal->setText(stringNeeded);
                break;

            case 0x40:
                stringNeeded = "";
                if (Length !=5)
                {
                    error = "9F40";
                    return false;
                }

                for (qint32 i =0; i < Length; i++)
                {
                    if (Buffertam[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(Buffertam[index+numByte+1+i], 16));
                }
                ui->lineEditAddition->setText(stringNeeded);
                break;
            }
            break;
          case 0xDF:
            switch (Buffertam[index+1])
            {
              case 0x79:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF79";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                qDebug() << " check " <<stringNeeded << state;
                ui->checkBoxCardHolder->setChecked(state);
                break;

              case 0x0A:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF0A";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                qDebug() << "check 2 " << stringNeeded << state;
                ui->checkBoxEMV1->setChecked(state);
                break;

              case 0x55:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF55";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxEMV2->setChecked(state);
                break;

              case 0x0B:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF0B";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxMastripe->setChecked(state);
                break;
              case 0x27:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF27";
                    return false;
                }

                if (Buffertam[index+numByte+1] < 0x0F)
                {
                    stringNeeded.append("0");
                }
                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                ui->lineEditPINTimeout->setText(stringNeeded);
                break;

              case 0x06:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF06";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxOnline->setChecked(state);
                break;

              case 0x08:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF08";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxAdvice->setChecked(state);
                break;

              case 0x7A:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF7A";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxPSE->setChecked(state);
                break;

              case 0x0D:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF0D";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxAutorun->setChecked(state);
                break;

              case 0x10:
                if (Length !=3)
                {
                    error = "DF10";
                    return false;
                }

                for (qint32 i =0; i < Length; i++)
                {
                    valueSpin += Buffertam[index+numByte+1+i] << 8*(Length-i-1);
                }
                ui->spinBox->setValue(valueSpin);
                break;

              case 0x7B:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF7B";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxPinBypass->setChecked(state);
                break;

              case 0x07:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF07";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxReferral->setChecked(state);
                break;

              case 0x09:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF09";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxDefault->setChecked(state);
                break;

            case 0x73:
                stringNeeded = "";
                if (Length !=5)
                {
                    error = "DF73";
                    return false;
                }
                for (qint32 i =0; i < Length; i++)
                {
                    if (Buffertam[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(Buffertam[index+numByte+1+i], 16));
                }
                ui->lineEditDefault->setText(stringNeeded);
                break;

            case 0x74:
                stringNeeded = "";
                if (Length !=5)
                {
                    error = "DF74";
                    return false;
                }

                for (qint32 i =0; i < Length; i++)
                {
                    if (Buffertam[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(Buffertam[index+numByte+1+i], 16));
                }
                ui->lineEditDenial->setText(stringNeeded);
                break;

            case 0x75:
                stringNeeded = "";
                if (Length !=5)
                {
                    error = "DF75";
                    return false;
                }

                for (qint32 i =0; i < Length; i++)
                {
                    if (Buffertam[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(Buffertam[index+numByte+1+i], 16));
                }
                ui->lineEditOnline->setText(stringNeeded);
                break;

            case 0x53:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF53";
                    return false;
                }
                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxRTSNot->setChecked(state);
                break;

            case 0x54:
                stringNeeded = "";
                if (Length !=1)
                {
                    error = "DF54";
                    return false;
                }

                stringNeeded.append(QString::number(Buffertam[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxVelocity->setChecked(state);
                break;

            case 0x7C:
                if (Length !=1)
                {
                    error = "DF7C";
                    return false;
                }

                switch (Buffertam[index+1+numByte])
                {
                 case 0x00:
                    ui->comboBoxCDA->setCurrentText("UNDEFINED");
                    break;
                case 0x01:
                    ui->comboBoxCDA->setCurrentText("MODE_1");
                    break;
                case 0x02:
                    ui->comboBoxCDA->setCurrentText("MODE_2");
                    break;
                case 0x03:
                    ui->comboBoxCDA->setCurrentText("MODE_3");
                    break;
                case 0x04:
                    ui->comboBoxCDA->setCurrentText("MODE_4");
                    break;
                default:
                    ui->comboBoxCDA->setCurrentText("UNDEFINED");
                    break;
               }
               break;

            }
            break;
        }
    }
    return true;
}



void MainWindow::on_toolButtonAddProcessing_clicked()
{
    QString Task = ui->lineEditMainProcessing->text().toUpper();
    if (!QString(Task).isEmpty())
    {
    QListWidgetItem *item1 = new QListWidgetItem(Task);
    ui->listWidgetProcessing->addItem(item1);
    ui->lineEditMainProcessing->clear();
    }
}

void MainWindow::on_toolButtonDenialProcessing_clicked()
{
    Def->show();
    LineEditChosed = mproTACDen;
    emit Button_7_Clicked(ui->lineEditDenialProcessing->text(), wdMainWindow);
}

void MainWindow::on_toolButtonOnlineProcessing_clicked()
{
    Def->show();
    LineEditChosed = mproTACOnl;
    emit ButtonOnline_Clicked(ui->lineEditOnlineProcessing->text(), wdMainWindow);
}

void MainWindow::on_toolButtonDefaultProcessing_2_clicked()
{

}

void MainWindow::on_toolButtonEntryUp_clicked()
{
    int selectRow = ui->tableWidget->currentRow();
    if (selectRow >0){
        QComboBox *comboKernelID;
        QComboBox *comboKernelIDAbove;
        QComboBox *comboAID;
        QComboBox *comboAIDAbove;
        QComboBox *comboTranType;
        QComboBox *comboTranTypeAbove;
        QLineEdit *lineEdit;
        QLineEdit *lineEditAbove;
        QString   text;
        QString   textAbove;

        comboKernelID = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 0);
        comboAID      = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 1);
        comboTranType = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 2);
        lineEdit      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow, 3);

        comboKernelIDAbove = (QComboBox*)ui->tableWidget->cellWidget(selectRow-1, 0);
        comboAIDAbove      = (QComboBox*)ui->tableWidget->cellWidget(selectRow-1, 1);
        comboTranTypeAbove = (QComboBox*)ui->tableWidget->cellWidget(selectRow-1, 2);
        lineEditAbove      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow-1, 3);

        text      = comboKernelID->currentText();
        textAbove = comboKernelIDAbove->currentText();

        comboKernelID->setCurrentText(textAbove);
        comboKernelIDAbove->setCurrentText(text);

        text      = comboAID->currentText();
        textAbove = comboAIDAbove->currentText();

        comboAID->setCurrentText(textAbove);
        comboAIDAbove->setCurrentText(text);

        text      = comboTranType->currentText();
        textAbove = comboTranTypeAbove->currentText();

        comboTranType->setCurrentText(textAbove);
        comboTranTypeAbove->setCurrentText(text);

        text      = lineEdit->text();
        textAbove = lineEditAbove->text();

        lineEdit->setText(textAbove);
        lineEditAbove->setText(text);

    }
}

void MainWindow::on_toolButtonEntryDown_clicked()
{
    int selectRow = ui->tableWidget->currentRow();
    if (selectRow <ui->tableWidget->rowCount()-1){
        QComboBox *comboKernelID;
        QComboBox *comboKernelIDBelow;
        QComboBox *comboAID;
        QComboBox *comboAIDBelow;
        QComboBox *comboTranType;
        QComboBox *comboTranTypeBelow;
        QLineEdit *lineEdit;
        QLineEdit *lineEditBelow;
        QString   text;
        QString   textBelow;

        comboKernelID = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 0);
        comboAID      = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 1);
        comboTranType = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 2);
        lineEdit      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow, 3);

        comboKernelIDBelow = (QComboBox*)ui->tableWidget->cellWidget(selectRow+1, 0);
        comboAIDBelow      = (QComboBox*)ui->tableWidget->cellWidget(selectRow+1, 1);
        comboTranTypeBelow = (QComboBox*)ui->tableWidget->cellWidget(selectRow+1, 2);
        lineEditBelow      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow+1, 3);

        text      = comboKernelID->currentText();
        textBelow = comboKernelIDBelow->currentText();

        comboKernelID->setCurrentText(textBelow);
        comboKernelIDBelow->setCurrentText(text);

        text      = comboAID->currentText();
        textBelow = comboAIDBelow->currentText();

        comboAID->setCurrentText(textBelow);
        comboAIDBelow->setCurrentText(text);

        text      = comboTranType->currentText();
        textBelow = comboTranTypeBelow->currentText();

        comboTranType->setCurrentText(textBelow);
        comboTranTypeBelow->setCurrentText(text);

        text      = lineEdit->text();
        textBelow = lineEditBelow->text();

        lineEdit->setText(textBelow);
        lineEditBelow->setText(text);

    }
}

void MainWindow::on_toolButtonEntryAdd_clicked()
{
    int insertRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(insertRow);
    qDebug() <<"row insert " << insertRow;
    QComboBox *comboBoxKernelID = new QComboBox(ui->tableWidget);
    QStringList kernelIDName;
    kernelIDName << "KERNEL01" << "MasterCard" << "Visa" << "American Express" << "JCB" << "Discover" << "C.U.P"
                 << "Interac FWM 3.1 " << "KERNEL09" << "KERNEL0A" << "KERNEL0B" << "KERNEL0C" << "KERNEL0D"
                 << "KERNEL0E" <<"KERNEL0F";

    for (int i =16; i<=64; i++){
        kernelIDName <<"KERNEL" + QString::number(i, 16).toUpper();
    }

    kernelIDName << "Interac (FMW 3.2) " << "Eftpos (FMW 3.2)" ;

    for (int i = 67; i<= 255; i++){
        kernelIDName << "KERNEL" + QString::number(i, 16).toUpper();
    }

    for (int i =0; i < kernelIDName.count(); i++){
        comboBoxKernelID->addItem(QString(kernelIDName.at(i)));
    }

    ui->tableWidget->setCellWidget(insertRow, 0, comboBoxKernelID);

    QComboBox *comboBoxTransaction = new QComboBox(ui->tableWidget);
    QStringList transactionName;

    transactionName << "PURCHASE" << "CASH" << "PURCHASE ...CASHBACK" << "REFUND" << "MANUAL CASH"
                    << "QUASI CASH" << "DEPOSIT" << "INQUIRY" << "PAYMENT" << "TRANSFER" << "ADMINISTRATIVE"
                    << "CLEAN" << "RETRIEVAL" << "UPDATE" << "AUTHENTICATION" << "CASH DISBURSEMENT" << "PRE AUTHORIZATION";

    for (int i =0; i < transactionName.count(); i++){
        comboBoxTransaction->addItem(QString(transactionName.at(i)));
    }

    ui->tableWidget->setCellWidget(insertRow, 2, comboBoxTransaction);

    QComboBox *comboBoxAID = new QComboBox(ui->tableWidget);
    comboBoxAID->addItem("1");
    comboBoxAID->addItem("2");
    comboBoxAID->addItem("3");

    ui->tableWidget->setCellWidget(insertRow, 1, comboBoxAID);

    QLineEdit *LineEdit = new QLineEdit(ui->tableWidget);
    LineEdit->setText("");

    ui->tableWidget->setCellWidget(insertRow, 3, LineEdit);

    QToolButton *Button = new QToolButton(LineEdit);
    Button->move(QPoint(5,insertRow-7));
    Button->setVisible(true);
    Button->setFixedSize(30, 20);
    Button->setStyleSheet("QToolButton{border: none; padding: 0px}");
    Button->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    Button->setIconSize(QSize(Button->width(), Button->height()));
    Button->setToolTip("Tag FF35");

    qDebug() << "position x " << Button->x();
    qDebug() << "position y  " << Button->y();
    int frameWidth = LineEdit->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    LineEdit->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; padding-left: %1px; } ").arg(Button->sizeHint().width() + frameWidth + 3));

    ListToolButton << Button;
    ListLineEdit   << LineEdit;
    listComboKernelID << comboBoxKernelID;
    listComboAID    << comboBoxAID;
    listComboTranType << comboBoxTransaction;

    sigMapp->setMapping(ListToolButton[insertRow], insertRow);
    connect(ListToolButton[insertRow], SIGNAL(clicked()), sigMapp, SLOT(map()));
    connect(sigMapp, SIGNAL(mapped(int)), this, SLOT(OpenNewWindow(int)));

}

void MainWindow::on_toolButtonEntryRemove_clicked()
{
    QString         textAsk;
    int currentRow = ui->tableWidget->currentRow();
    QMessageBox::StandardButton reply;

    textAsk = "Do you want to remove the entry point in line ";
    textAsk += QString::number(currentRow +1) + " ?";
    reply = QMessageBox::question(this, "Remove entry point", textAsk, QMessageBox::Yes|
                          QMessageBox::Cancel);

    if (reply == QMessageBox::Yes)
    {
        ui->tableWidget->removeRow(currentRow);

        listComboAID.removeAt(currentRow);
        listComboKernelID.removeAt(currentRow);
        listComboTranType.removeAt(currentRow);
        ListToolButton.removeAt(currentRow);
        ListLineEdit.removeAt(currentRow);
    }


}

void MainWindow::on_ButtonEntrySave_clicked()
{
    QString fileName ;

    fileName = ui->labelDirEntry->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_ButtonOpen_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open TERMINAL binary file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelDir->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();

}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_ButtonOpenProcessing_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open PROCESSING binary file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelDirProcessing->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();

}

void MainWindow::on_ButtonEntryOpen_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open ENTRY POINT binary file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelDirEntry->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();
}

void MainWindow::on_ButtonSaveAs_clicked()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save TERMINAL binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

void MainWindow::on_ButtonSaveAsProcessing_clicked()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save PROCESSING binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

void MainWindow::on_ButtonEntrySaveAs_clicked()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save ENTRY POINT binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

void MainWindow::on_checkBoxAutorun_clicked()
{
    if (1 == ui->checkBoxAutorun->isChecked())
    {
        ui->spinBox->setEnabled(true);
    }
    else
    {
        ui->spinBox->setEnabled(false);
    }
}

void MainWindow::on_toolButtonRemoveProcessing_clicked()
{
    QListWidgetItem* item = ui->listWidgetProcessing->currentItem();
    qDebug() << "nhay vao day";
    delete ui->listWidgetProcessing->takeItem(ui->listWidgetProcessing->row(item));

}

void MainWindow::on_toolButtonUpProcessing_clicked()
{
    QListWidgetItem* itemCurrent = ui->listWidgetProcessing->currentItem();
    int currentIndex             = ui->listWidgetProcessing->row(itemCurrent);

    if (currentIndex >0)
    {
        QListWidgetItem* itemPrev = ui->listWidgetProcessing->item(currentIndex -1);

        QListWidgetItem* itemTemp = ui->listWidgetProcessing->takeItem(currentIndex -1);
        ui->listWidgetProcessing->insertItem(currentIndex -1, itemCurrent);
        ui->listWidgetProcessing->insertItem(currentIndex, itemTemp);
    }
}

void MainWindow::on_toolButtonDownProcessing_clicked()
{
    int numRows = ui->listWidgetProcessing->count();
    QListWidgetItem *itemCurrent = ui->listWidgetProcessing->currentItem();
    int currentIndex = ui->listWidgetProcessing->row(itemCurrent);

    if (currentIndex < numRows-1)
    {
        QListWidgetItem *itemAfter = ui->listWidgetProcessing->takeItem(currentIndex +1);

        ui->listWidgetProcessing->insertItem(currentIndex +1, itemCurrent);
        ui->listWidgetProcessing->insertItem(currentIndex, itemAfter);
    }
}

void MainWindow::on_checkBox_clicked()
{
    if (ui->checkBox->isChecked())
    {
        ui->lineEditDenialProcessing->setEnabled(true);
        ui->lineEditDefaultProcessing->setEnabled(true);
        ui->lineEditOnlineProcessing->setEnabled(true);
        ui->toolButtonDefaultProcessing->setEnabled(true);
        ui->toolButtonOnlineProcessing->setEnabled(true);
        ui->toolButtonDenialProcessing->setEnabled(true);
    }
    else
    {
        ui->lineEditDenialProcessing->setEnabled(false);
        ui->lineEditDefaultProcessing->setEnabled(false);
        ui->lineEditOnlineProcessing->setEnabled(false);
        ui->toolButtonDefaultProcessing->setEnabled(false);
        ui->toolButtonOnlineProcessing->setEnabled(false);
        ui->toolButtonDenialProcessing->setEnabled(false);
    }
}

void MainWindow::on_toolButtonDefaultProcessing_clicked()
{
    Def->show();
    LineEditChosed = mproTACDef;
    emit ButtonDefault_Clicked(ui->lineEditDefaultProcessing->text(), wdMainWindow);
}

void MainWindow::on_listWidgetProcessing_itemChanged(QListWidgetItem *item)
{

}

void MainWindow::on_listWidgetProcessing_itemClicked(QListWidgetItem *item)
{
    int              index = ui->listWidgetProcessing->row(item);
    qDebug() << "index2 " << index;
}

void MainWindow::on_toolButtonLoadDEK_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToDataStorage();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open DEK DET xml file", dirOpen, "Files(*.xml)");
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelXMLFile->setText(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    ui->textEditData->setText(text);
    file.close();
}

void MainWindow::on_toolButtonClearDEK_clicked()
{
    ui->textEditData->clear();
    ui->labelXMLFile->clear();
}

void MainWindow::on_ButtonSave_clicked()
{
    QString fileName ;

    fileName = ui->labelDir->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_checkBox_TTQ_clicked()
{
    if (ui->checkBox_TTQ->checkState())
    {
        ui->lineEdit_defaultTTQ->setEnabled(true);
        ui->toolButton_TTQ->setEnabled(true);
    }
    else
    {
        ui->lineEdit_defaultTTQ->setEnabled(false);
        ui->toolButton_TTQ->setEnabled(false);
    }
}

void MainWindow::on_checkBox_Referral_clicked()
{
    if (ui->checkBox_Referral->checkState())
    {
        ui->comboBox_ManageACE->setEnabled(true);
    }
    else
    {
        ui->comboBox_ManageACE->setEnabled(false);
    }
}

void MainWindow::on_toolButton_DRLAdd_clicked()
{
    QString Task = ui->lineEdit_DRL->text().toUpper();
    if (!QString(Task).isEmpty()){
    QListWidgetItem *item1 = new QListWidgetItem(Task);
    ui->listWidget_DRL->addItem(item1);
    ui->lineEdit_DRL->clear();
    }
}

void MainWindow::on_toolButton_DRLRemove_clicked()
{
    QListWidgetItem* item = ui->listWidget_DRL->currentItem();
    delete ui->listWidget_DRL->takeItem(ui->listWidget_DRL->row(item));
}

void MainWindow::on_toolButton_DRLUp_clicked()
{
    QListWidgetItem* itemCurrent = ui->listWidget_DRL->currentItem();
    int currentIndex             = ui->listWidget_DRL->row(itemCurrent);

    if (currentIndex >0)
    {
        QListWidgetItem* itemPrev = ui->listWidget_DRL->item(currentIndex -1);

        QListWidgetItem* itemTemp = ui->listWidget_DRL->takeItem(currentIndex -1);
        ui->listWidget_DRL->insertItem(currentIndex -1, itemCurrent);
        ui->listWidget_DRL->insertItem(currentIndex, itemTemp);
    }
}

void MainWindow::on_toolButton_DRLDown_clicked()
{
    int numRows = ui->listWidget_DRL->count();
    QListWidgetItem *itemCurrent = ui->listWidget_DRL->currentItem();
    int currentIndex = ui->listWidget_DRL->row(itemCurrent);

    if (currentIndex < numRows-1)
    {
        QListWidgetItem *itemAfter = ui->listWidget_DRL->takeItem(currentIndex +1);

        ui->listWidget_DRL->insertItem(currentIndex +1, itemCurrent);
        ui->listWidget_DRL->insertItem(currentIndex, itemAfter);
    }
}

void MainWindow::on_toolButton_DRLOpen_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open DRL set file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->label_DRLDir->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();

}

void MainWindow::on_toolButton_DRLSaveAs_clicked()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save DRL set file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

void MainWindow::on_ButtonSaveProcessing_clicked()
{
    QString fileName ;

    fileName = ui->labelDirProcessing->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_toolButton_DRLSave_clicked()
{
    QString fileName ;

    fileName = ui->label_DRLDir->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_actionExit_Ctrl_Q_triggered()
{
   aceExit->show();
}

void MainWindow::on_actionExport_triggered()
{

}

void MainWindow::on_actionSend_CA_Keys_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "CA Keys Download",
                                                    "C:/");

    QFile file(fileName);

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    file.close();
}

void MainWindow::on_actionSend_Revocated_CA_Keys_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "CA Keys Download",
                                                    "C:/");

    QFile file(fileName);

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    file.close();
}

void MainWindow::on_actionAdministrative_triggered()
{
    emit adminShow(m_textAdmin);
    admin->show();
}

void MainWindow::on_actionAbout_triggered()
{
    helpShow->show();
}

void MainWindow::on_actionLoad_triggered()
{
    QString dirOpen;
    QString fileName;
    QString tempString, stringCheck;
    QByteArray dataRead;
    bool    state;
    QString err;

    dirOpen = m_dir1;
    fileName = QFileDialog::getOpenFileName(this,
                                              "Open TERMINAL binary file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        QMessageBox::warning(this, "warning","Cannnot open file!");
        return;
    }

    ui->labelDir->setText(fileName);
    updateDir(fileName);

    dataRead = file.readAll();

    if (dataRead.length() == 0)
    {
        QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "size error on 00");
        return ;
    }
    if ((dataRead.length() < 21)| ((quint8)dataRead.at(0) != 0xaa))
    {
        QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "size error on 00");
        return;
    }

    unsigned char* bufferTemp = new unsigned char[dataRead.length()-21];

    for (qint32 i =21; i < dataRead.length(); i++)
    {
       bufferTemp[i-21] = (unsigned char)dataRead.at(i);
    }

    tSHA  ct;
    unsigned char resultSHA[20] ={0};
    ShaInit(&ct);
    ShaUpdate(&ct, bufferTemp,dataRead.length()-21);
    ShaFinal(&ct,resultSHA);

    for (qint32 i =0; i< 20; i++)
    {
        stringCheck += QString::number(resultSHA[i], 16);
        tempString  += QString::number((unsigned char)dataRead.at(i+1), 16);
    }

    if (stringCheck != tempString)
    {
        QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n "
                                                 "wrong sha on 00");
        return;
    }

    state = checkTagOtherTLV(bufferTemp, dataRead.length()-21);

    if (!state)
    {
        QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "wrong parse data on 00");
        return;
    }

    state = CheckTagAvai(bufferTemp, dataRead.length()-21, err);
    delete[]bufferTemp;
    if (!state)
    {
        QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "wrong read data " + err + "\n " + err.toLower());
        return;
    }


    file.close();
}

void MainWindow::on_actionSave_triggered()
{
    QString fileName ;

    fileName = ui->labelDir->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_actionSave_As_triggered()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save TERMINAL binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

void MainWindow::on_actionLoad_2_triggered()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open PROCESSING binary file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelDirProcessing->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();
}

void MainWindow::on_actionSave_2_triggered()
{
    QString fileName ;

    fileName = ui->labelDirProcessing->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_actionSave_As_2_triggered()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save PROCESSING binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

void MainWindow::on_actionLoad_3_triggered()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open ENTRY POINT binary file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelDirEntry->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();
}

void MainWindow::on_actionSave_3_triggered()
{
    QString fileName ;

    fileName = ui->labelDirEntry->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_actionSave_As_3_triggered()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save ENTRY POINT binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}


void MainWindow::on_checkBoxAutorun_stateChanged(int arg1)
{
    bool state =false;
    state = (ui->checkBoxAutorun->checkState())?true:false;
    ui->spinBox->setEnabled(state);
}

void MainWindow::on_checkBoxDefault_stateChanged(int arg1)
{
    bool state = false;
    state = (ui->checkBoxDefault->checkState())?true:false;
    ui->lineEditDenial->setEnabled(state);
    ui->toolButtonDefault->setEnabled(state);
    ui->lineEditDefault->setEnabled(state);
    ui->toolButtonOnline->setEnabled(state);
    ui->lineEditOnline->setEnabled(state);
    ui->toolButton_7->setEnabled(state);
}
