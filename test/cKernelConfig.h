#ifndef cKernelConfig_H
#define cKernelConfig_H

#include <QDialog>
#include <QString>
#include <QSignalMapper>
#include <QCheckBox>
#include <QList>
#include "stdint.h"

namespace Ui {
class cKernelConfig;
}

class cKernelConfig : public QDialog
{
    Q_OBJECT

signals:
   void ButtonOK_KerConfig_clicked(const QString &text);

public:
    explicit cKernelConfig(QWidget *parent = 0);
    ~cKernelConfig();

    QString kcTextLineEdit;

public slots:
    void CheckboxUpdate(int index);

    void kcChangeTextEdit(const QString &text);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_textEdited(const QString &arg1);

private:
    Ui::cKernelConfig *ui;

    QList<QCheckBox*> m_listCheckBoxKerConfig;

    QSignalMapper *m_sigMapp;
    QString       m_textLineEdit;

    uint8_t m_num =0;
};

#endif // cKernelConfig_H
