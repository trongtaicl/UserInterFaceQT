// NAME.......  sha1.h
// PURPOSE....  Generic Platform Interface - SHA-1 Hash (or Digest) Algorithm Functions Simulated
// PROJECT....  GPI / Linux Platform
// REFERENCES.
//
// Copyright ©2005-2016 - 9164-4187 QUEBEC INC (“AMADIS”), All Rights Reserved
//

#ifndef SHA_H_
#define SHA_H_

//---------------------------------------------------------
//			Definitions
//---------------------------------------------------------
//
typedef struct
{	// SHA context
	int		hashLen;                // Length of Hash Result
	unsigned int	totLen;			// Total Length of Message in Bits
	unsigned int	W[80];			// Word Input Sequence
	unsigned int	H[5];			// 160 Bits Message Digest
	unsigned int	A,B,C,D,E;		// A, B, C, D, E Registers for 80 Rounds
	unsigned char	buf[128];		// Temporary Buffer to Store Incomplete Block (512 bits)
	unsigned int	bufLen;			// Length
}tSHA;

//---------------------------------------------------------
//			tSHA gpiShaInit() gpiShaUpdate() gpiShaFinal()
//---------------------------------------------------------
//  SHA-1 Computation
//
//  Usage:  To compute an SHA-1 digest (20 bytes):
//          a) call ShaInit()
//          b) call ShaUpdate() for each piece of data
//          c) call ShaFinal() to get the digest of this sequence of data
//          The link is made by a common SHA context tSHA, you provide for these functions
//
void ShaInit(tSHA* ctx);
void ShaUpdate(tSHA* ctx,unsigned char* msg,int len);
void ShaFinal(tSHA* ctx,unsigned char* out);

#endif /* SHA_H_ */
