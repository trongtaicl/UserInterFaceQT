#include "cKernelConfig.h"
#include "ui_cKernelConfig.h"

cKernelConfig::cKernelConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cKernelConfig)
{
    ui->setupUi(this);
    this->setWindowTitle("Kernel Configuration");
    m_listCheckBoxKerConfig << ui->checkBox << ui->checkBox_2 << ui->checkBox_3 << ui->checkBox_4
                         << ui->checkBox_5 << ui->checkBox_6 << ui->checkBox_7 << ui->checkBox_8;

    ui->lineEdit->setInputMask("hh");
    ui->lineEdit->setText("00");
    ui->lineEdit->setAlignment(Qt::AlignRight);

    m_sigMapp = new QSignalMapper(this);

    for (int i =0; i< 8; i++){
        m_sigMapp->setMapping(m_listCheckBoxKerConfig[i], i);
        connect(m_listCheckBoxKerConfig[i], SIGNAL(clicked()), m_sigMapp, SLOT(map()));
    }
    connect(m_sigMapp, SIGNAL(mapped(int)), this, SLOT(CheckboxUpdate(int)));
}

cKernelConfig::~cKernelConfig()
{
    delete ui;
}

void cKernelConfig::CheckboxUpdate(int index)
{
    bool State =false;
    for (int i =0; i< 8; i++){
        State = m_listCheckBoxKerConfig[i]->checkState();

        if (State){
            m_num |= (1<<i);
        }
        else {
            m_num &= ~(1<<i);
        }
    }
    ui->lineEdit->setText(QString("%1").arg(m_num, 2, 16, QChar('0')).toUpper());
}

void cKernelConfig::kcChangeTextEdit(const QString &text)
{
    m_textLineEdit = text;
    uint8_t m_numTemp = m_textLineEdit.toUInt(NULL, 16);
        ui->lineEdit->setText(m_textLineEdit);
        for (int i =0; i< 8; i++){
            m_listCheckBoxKerConfig[i]->setChecked((m_numTemp & (1 << i)));
        }
}

void cKernelConfig::on_pushButton_clicked()
{
    m_textLineEdit = ui->lineEdit->text();
    emit ButtonOK_KerConfig_clicked(m_textLineEdit);
    this->close();
}

void cKernelConfig::on_pushButton_2_clicked()
{
    this->close();
}

void cKernelConfig::on_lineEdit_textChanged(const QString &arg1)
{

}

void cKernelConfig::on_lineEdit_textEdited(const QString &arg1)
{
    qint32  lengthString;
    qint32  cursorPos;
    qint32  numTemp;
    QString stringTemp;
    bool    insert = false;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 2)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }

    if (true == insert)
    {
        ui->lineEdit->setText(stringTemp);
        ui->lineEdit->setCursorPosition(cursorPos-1);
        insert = false;
    }

    numTemp = stringTemp.toInt(NULL, 16);
    for (qint32 i =0; i < m_listCheckBoxKerConfig.length(); i++)
    {
        m_listCheckBoxKerConfig.at(i)->setChecked(numTemp &(1 << i));
    }
}
