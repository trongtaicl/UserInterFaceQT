#include "cSplitTag.h"
#include <QDebug>

cSplitTag::cSplitTag()
{
   for (qint32 i =0; i < 25; i++)
   {
       m_stateCheckboxLineEdit[i] = false;
   }

   for (qint32 i =0; i< 5; i++)
   {
       m_stateCheckboxCombo[i] = false;
       m_stateCheckboxSpin[i]  = false;
   }


}

void cSplitTag::initializeList(QList<QCheckBox*> listCheckboxLine, QList<QLineEdit*> listLine,
                               QList<QCheckBox*> listCheckBoxCombo, QList<QCheckBox*> listCheckboxSpin,
                               QList<QComboBox*> listComboBox, QList<QSpinBox*> listSpinBox,
                               QStringList stringComboTerType, QStringList stringComboKerID, QStringList stringComboState)
{
    m_listCheckBoxLineEdit = listCheckboxLine;
    m_listLineEdit         = listLine;
    m_listComboBox         = listComboBox;
    m_listSpinBox          = listSpinBox;
    m_listCheckBoxCombo    = listCheckBoxCombo;
    m_listCheckBoxSpin     = listCheckboxSpin;
    m_stringComboTerType   = stringComboTerType;
    m_stringComboKerID     = stringComboKerID;
    m_stringComboState     = stringComboState;

}

void cSplitTag::updateComboType(mls_comboType_t comboType)
{
    m_comboType = comboType;
}

void cSplitTag::UncheckTag()
{
    text = "";
    for (int i =0; i< m_listCheckBoxCombo.count(); i++){
        if (m_stateCheckboxCombo[i] == false){
                m_listCheckBoxCombo[i]->setChecked(false);
                m_listCheckBoxCombo[i]->setStyleSheet("QCheckBox{color: rgb(170, 170, 170)}");
                m_listComboBox[i]->setEnabled(false);
              }

        m_stateCheckboxCombo[i] = false;
    }

    for (int i =0; i< m_listCheckBoxSpin.count(); i++){
        if (m_stateCheckboxSpin[i] == false){
            m_listCheckBoxSpin[i]->setChecked(false);
            m_listCheckBoxSpin[i]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
            m_listSpinBox[i]->setValue(0);
            m_listSpinBox[i]->setEnabled(false);
            }
        m_stateCheckboxSpin[i] = false;
    }
    for (int i =0; i< m_listLineEdit.count(); i++){
         if (m_stateCheckboxLineEdit[i]== false){
            m_listCheckBoxLineEdit.at(i)->setChecked(false);
            m_listCheckBoxLineEdit.at(i)->setStyleSheet("QCheckBox{color: rgb(170, 170, 170)}");
            if (6!= i)
            {
                m_listLineEdit.at(i)->setText(QString("%1").arg(0, 12, 16, QChar('0')));
            }
            else
            {
                m_listLineEdit.at(i)->clear();
            }

            m_listLineEdit.at(i)->setStyleSheet("border: 1px solid gray; border-radius: 2px");
            m_listLineEdit.at(i)->setEnabled(false);
            }
         m_stateCheckboxLineEdit[i] = false;
        }
     qDebug() << "ok";
}

void cSplitTag::Updatetext(int Index, mlsBoolWidgetSplitTag_t Widget, uint8_t *Buffertam,
                           uint8_t Pos, uint8_t Length)
{
        QString Text;
        bool flagComboTerType = false;
        bool flagComboKerID   = false;
        bool flagComboStat    = false;

        if ((Widget == splitBoolLineEdit)|| (Widget == splitBoolComboBox)){
              for (int i = 0; i < Length; i++){
                  Text += QString("%1").arg(Buffertam[Pos+i], 2, 16, QChar('0')).toUpper();
              }

              if (Widget == splitBoolLineEdit){
                  m_stateCheckboxLineEdit[Index] = true;
                  m_listLineEdit[Index]->setText(Text);
                  m_listCheckBoxLineEdit[Index]->setChecked(true);
                  m_listCheckBoxLineEdit[Index]->setStyleSheet("QCheckBox{color: black}");
                  m_listLineEdit[Index]->setEnabled(true);
                  m_listLineEdit[Index]->setStyleSheet("border: 1px solid black; border-radius: 3px;");
              }

              else if (m_comboType == ctTerType){
                  m_stateCheckboxCombo[Index] = true;
                  for (int i =0; i<m_stringComboTerType.count(); i++){
                      if (Text == m_stringComboTerType[i]){
                          flagComboTerType = true;

                          m_listComboBox[Index]->setCurrentText(m_stringComboTerType[i]);
                          m_listComboBox[Index]->setEnabled(true);
                          m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{color:black}");
                          m_listCheckBoxCombo[Index]->setChecked(true);
                      }
              }

                  if (!flagComboTerType){
                      m_listComboBox[Index]->setCurrentText("00");
                      m_listComboBox[Index]->setEnabled(false);
                      m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
                      m_listCheckBoxCombo[Index]->setChecked(false);
                  }
              }

              else if (m_comboType == ctKerID){
                  m_stateCheckboxCombo[Index] = true;
                  for (int i =0; i< m_stringComboKerID.count(); i++){
                      if ((i+1) == Buffertam[Pos]){

                          m_listComboBox[Index]->setCurrentText(m_stringComboKerID[i]);
                          m_listComboBox[Index]->setEnabled(true);
                          m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{color:black}");
                          m_listCheckBoxCombo[Index]->setChecked(true);
                          flagComboKerID = true;
                      }
                  }
                  if (!flagComboKerID){
                      m_listComboBox[Index]->setCurrentText("MasterCard");
                      m_listComboBox[Index]->setEnabled(false);
                      m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
                      m_listCheckBoxCombo[Index]->setChecked(false);
                  }
              }
              else if (m_comboType == ctStat){
                  m_stateCheckboxCombo[Index] = true;
                  for (int i =0; i< m_stringComboState.count(); i++){
                      qDebug() << "buffertam pos  " << Buffertam[Pos];
                      if (i == Buffertam[Pos]){

                          m_listComboBox[Index]->setCurrentText(m_stringComboState[i]);
                          m_listComboBox[Index]->setEnabled(true);
                          m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{color:black}");
                          m_listCheckBoxCombo[Index]->setChecked(true);
                          flagComboStat = true;
                      }
                  }
                  if (!flagComboStat){
                      m_listComboBox[Index]->setCurrentText("not allowed (0x00)");
                      m_listComboBox[Index]->setEnabled(false);
                      m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
                      m_listCheckBoxCombo[Index]->setChecked(false);
                  }
        }
        }

        if (Widget == splitBoolSpinBox){
            if (Length <2){
                m_stateCheckboxSpin[Index] = true;
                m_listSpinBox[Index]->setValue(Buffertam[Pos]);
                m_listCheckBoxSpin[Index]->setChecked(true);
                m_listCheckBoxSpin[Index]->setStyleSheet("QCheckBox{color:black}");
            }
            else{
                m_listSpinBox[Index]->setValue(0);
                m_listSpinBox[Index]->setEnabled(false);
                m_listCheckBoxSpin[Index]->setChecked(false);
                m_listCheckBoxSpin[Index]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
            }
        }
}

void cSplitTag::UpdateOtherTLV(uint8_t *Buffertam, uint8_t Pos, uint8_t Sobyte, uint8_t Length)
{ 
    for (int i =0; i< Length + Sobyte +1; i++)
    {
        text += QString("%1").arg(Buffertam[Pos+i], 2, 16, QChar('0')).toUpper();
    }
}

void cSplitTag::UncheckCheckbox(int Index, mlsBoolWidgetSplitTag_t Widget)
{
    if (Widget == splitBoolLineEdit){
        m_listCheckBoxLineEdit[Index]->setChecked(false);
        m_listCheckBoxLineEdit[Index]->setStyleSheet("QCheckBox{color: rgb(170, 170, 170)}");
        m_listLineEdit[Index]->setEnabled(false);
        m_listLineEdit[Index]->setStyleSheet("border: 1px solid gray; border-radius: 3px;");
    }
    else if (Widget == splitBoolComboBox){
        if (m_comboType == ctTerType){
            m_listCheckBoxCombo[Index]->setChecked(false);
            m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
            m_listComboBox[Index]->setEnabled(false);
            m_listComboBox[Index]->setCurrentText("00");
        }
        else if (m_comboType == ctKerID){
            m_listCheckBoxCombo[Index]->setChecked(false);
            m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
            m_listComboBox[Index]->setEnabled(false);
            m_listComboBox[Index]->setCurrentText("MasterCard");
        }
        else if (m_comboType == ctStat){
            m_listCheckBoxCombo[Index]->setChecked(false);
            m_listCheckBoxCombo[Index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
            m_listComboBox[Index]->setEnabled(false);
            m_listComboBox[Index]->setCurrentText("not allowed (0x00)");
        }
    }
    else {

        m_listCheckBoxSpin[Index]->setChecked(false);
        m_listCheckBoxSpin[Index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
        m_listSpinBox[Index]->setEnabled(false);

    }

}
