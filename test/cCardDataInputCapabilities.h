#ifndef cCardDataInputCapabilities_H
#define cCardDataInputCapabilities_H

#include <QDialog>
#include <QSignalMapper>
#include <QCheckBox>
#include <QList>
#include <QSignalMapper>
#include <QString>

namespace Ui {
class cCardDataInputCapabilities;
}

class cCardDataInputCapabilities : public QDialog
{
    Q_OBJECT

signals:
    void ButtonOk_CardData_clicked(const QString &text);  /* Button Ok is pressed, it will emit this signal */

public:
    explicit cCardDataInputCapabilities(QWidget *parent = 0);
    ~cCardDataInputCapabilities();


private slots:
    void on_pushButton_clicked(); /* button Ok is pressed */

    void on_pushButton_2_clicked(); /*button cancel is pressed */

    void on_lineEdit_textEdited(const QString &arg1);

public slots:
    void check_box_update(int index);

    void cd_change_text_edit(const QString &text);

private:

    Ui::cCardDataInputCapabilities *ui;

    QList<QCheckBox*> m_listCheckBoxCardData;   /* list of checkbox in this window */

    QSignalMapper *m_sigMapp;  /* sigMapp is used to connect multiple signal from listcheckbox with a slot  */
    QString m_textLineEdit;

};

#endif // cCardDataInputCapabilities_H
