#ifndef CCREATESTATICVARIABLE_H
#define CCREATESTATICVARIABLE_H
#include <QString>

class cCreateStaticVariable
{
public:
    cCreateStaticVariable();

    QString getDirToOpenProcessing();

    QString getDirToDataStorage();

    void    updateValueDirTerProcess(QString text);
    void    updateValueDirDataStorage(QString text);

private:

    static QString m_dirToTerProcess;
    static QString m_dirToDataStorage;
};

#endif // CCREATESTATICVARIABLE_H
