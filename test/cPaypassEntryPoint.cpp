#include "cPaypassEntryPoint.h"
#include "ui_cPaypassEntryPoint.h"
#include "cSplitTag.h"

cPaypassEntryPoint::cPaypassEntryPoint(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cPaypassEntryPoint)
{
    ui->setupUi(this);
    this->setWindowTitle("Paypass Entrypoint");
    m_comboTerType << "00" << "11" << "12" << "13" << "14" << "15" << "16" << "21" << "22" << "23"
                 << "24" << "25" <<"26" << "34" << "35" << "36";
   // all these kernelID,
    m_comboKerID << "KERNEL01" << "MasterCard" << "Visa" << "American Express" << "JCB" << "JCB" << "Discover"
                << "C.U.P" << "Interact FMW 3.1";

    for (int i =0x09; i <= 0x40 ;i++){
        m_comboKerID << "KERNEL" + QString::number(i, 16).toUpper();
    }
    m_comboKerID << "Interac FMW 3.2" << "Eftpos FMW 3.2";
    for (int i = 0x43; i<= 0xFF; i++){
        m_comboKerID << "KERNEL" + QString::number(i, 16).toUpper();
    }

    m_listCheckBoxLine << ui->checkBox0 << ui->checkBox1 << ui->checkBox2 << ui->checkBox3 << ui->checkBox4
                 << ui->checkBox5 << ui->checkBox8 << ui->checkBox9 << ui->checkBox10 << ui->checkBox11
                 << ui->checkBox12 << ui->checkBox13 << ui->checkBox14 << ui->checkBox15 << ui->checkBox16
                 << ui->checkBox18 << ui->checkBox19 << ui->checkBox20 << ui->checkBox21 << ui->checkBox22
                 << ui->checkBox23 << ui->checkBox24 << ui->checkBox25 << ui->checkBox26;

    m_listLineEditCheck << ui->lineEdit << ui->lineEdit_2 << ui->lineEdit_3 << ui->lineEdit_4 << ui->lineEdit_5
                  << ui->lineEdit_6 << ui->lineEdit_7 << ui->lineEdit_8 << ui->lineEdit_9 << ui->lineEdit_10
                  << ui->lineEdit_11 << ui->lineEdit_13 << ui->lineEdit_12 << ui->lineEdit_17 << ui->lineEdit_16
                  << ui->lineEdit_15 << ui->lineEdit_20 << ui->lineEdit_21 << ui->lineEdit_19 << ui->lineEdit_18
                  << ui->lineEdit_25 << ui->lineEdit_24 << ui->lineEdit_22 << ui->lineEdit_23 ;

    for (int i =0; i < m_listLineEditCheck.count(); i++){
        m_listLineEditCheck.at(i)->setStyleSheet("border: 1px solid black");
    }

    m_listCheckBoxCombo << ui->checkBoxcombo1 << ui->checkBoxCombo2;

    m_listCheckBoxSpin << ui->checkBox17;

    ui->spinBox->setMaximum(255);

    m_listSpin << ui->spinBox;

    for (int i =0; i< m_comboTerType.count(); i++){
        ui->comboBox->addItem(m_comboTerType.at(i));
    }

    for(int i =0; i< m_comboKerID.count(); i++){
        ui->comboBox_2->addItem(m_comboKerID.at(i));
    }

    m_listComBoBox << ui->comboBox << ui->comboBox_2;

    InitializePaypass();
    InitializeToolButton();

    AddTer       = new cAdditionalTerCapabilities(this);
    TACWin       = new cTerminalActionCode(this);
    winCardData  = new cCardDataInputCapabilities(this);
    winSecCap    = new cSecurityCapabilities(this);
    winCVMCap    = new cCvmCapabilities(this);
    winKerConfig = new cKernelConfig(this);

    sigMappLineEdit  = new QSignalMapper();

    textFinal = "";
    m_lineEditChoseTAC = 0;
    m_lineEditChoseCVM = 0;


    for (int i =0; i< m_listCheckBoxLine.count(); i++){
        sigMappLineEdit->setMapping(m_listCheckBoxLine[i], i);
       /* connect every checkbox click signal with map() */
        connect(m_listCheckBoxLine[i], SIGNAL(clicked()), sigMappLineEdit, SLOT(map()));
    }
    connect(sigMappLineEdit, SIGNAL(mapped(int)), this, SLOT(checkbox_update_lineEdit(int)));

    connect(this, SIGNAL(ToolButtonCardData_clicked(QString)), winCardData, SLOT(cd_change_text_edit(QString)));

    connect(winCardData, SIGNAL(ButtonOk_CardData_clicked(QString)), this, SLOT(UpdateCardData(QString)));

    connect(this, SIGNAL(ToolButtonSecCap_clicked(QString)), winSecCap, SLOT(scChangeTextEdit(QString)));

    connect(winSecCap, SIGNAL(ButtonOK_SecCap_clicked(QString)), this, SLOT(UpdateSecCap(QString)));

    connect(this, SIGNAL(ToolButtonnCVMCap_clicked(QString)), winCVMCap, SLOT(cvm_change_text_edit(QString)));

    connect(winCVMCap, SIGNAL(ButtonOk_CVMCap_clicked(QString)), this, SLOT(UpdateCVMCap(QString)));

    connect(this, SIGNAL(ToolButtonKerConfig_clicked(QString)), winKerConfig, SLOT(kcChangeTextEdit(QString)));

    connect(winKerConfig, SIGNAL(ButtonOK_KerConfig_clicked(QString)), this, SLOT(UpdateKerConfig(QString)));

    connect(this, SIGNAL(ToolButtonAddTerCap_clicked(QString,mlsWinAdd_t)), AddTer, SLOT(change_text_edit(QString,mlsWinAdd_t)));

    connect(AddTer, SIGNAL(ButtonOk_PayPass_Clicked(QString)), this, SLOT(UpdateAddTerCap(QString)));

    connect(this, SIGNAL(ToolButtonTACDefault_clicked(QString,mlsWindowDef_t)), TACWin, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(this, SIGNAL(ToolButtonTACDenial_clicked(QString,mlsWindowDef_t)), TACWin, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(this, SIGNAL(ToolButtonTACOnline_clicked(QString,mlsWindowDef_t)), TACWin, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(TACWin, SIGNAL(ButtonOK_PayPass_Clicked(QString)), this, SLOT(UpdateTACData(QString)));

}

cPaypassEntryPoint::~cPaypassEntryPoint()
{
    delete ui;
}

void cPaypassEntryPoint::InitializePaypass()
{
    int frameWidth;

    for (int i =0; i< m_listCheckBox.count(); i++){
        m_listCheckBox.at(i)->setChecked(false);
        m_listCheckBox.at(i)->setStyleSheet("QCheckBox{color:rgb(200, 200, 200)}");
        m_listLineEditCheck.at(i)->setEnabled(false);
        m_listLineEditCheck.at(i)->setStyleSheet("border: 1px solid rgb(160, 160, 160); border-radius: 2px");
    }

    for (int i = 18; i<m_listLineEditCheck.count(); i++){
        m_listLineEditCheck.at(i)->setInputMask("hhhhhhhhhhhh");
        m_listLineEditCheck.at(i)->setText("000000000000");
    }

    for (int i =15; i<18; i++){
        m_listLineEditCheck.at(i)->setInputMask("hhhhhhhhhh");
        m_listLineEditCheck.at(i)->setText("0000000000");
    }

    ui->lineEdit_16->setInputMask("hhhh");
    ui->lineEdit_16->setText("0000");
    ui->lineEdit_16->setToolTip("Tag DF1C");

    for (int i = 10; i< 14; i++){
       m_listLineEditCheck.at(i)->setInputMask("hh");
       m_listLineEditCheck.at(i)->setText("00");
    }
    ui->checkBox12->setToolTip("Tag DF03");
    ui->lineEdit_11->setToolTip("Tag DF03");

    ui->checkBox13->setToolTip("Tag DF17");
    ui->lineEdit_13->setToolTip("Tag DF17");

    ui->checkBox14->setToolTip("Tag DF18");
    ui->lineEdit_12->setToolTip("Tag DF18");

    ui->checkBox15->setToolTip("Tag DF19");
    ui->lineEdit_17->setToolTip("Tag DF19");

    ui->checkBox16->setToolTip("Tag DF1C in second");
    ui->lineEdit_16->setToolTip("Tag DF1C in second");

    ui->checkBox18->setToolTip("Tag DF20");
    ui->lineEdit_15->setToolTip("Tag DF20");
    frameWidth = ui->lineEdit_15->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_15->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonTACDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_15->setAlignment(Qt::AlignLeft);

    ui->checkBox19->setToolTip("Tag DF21");
    ui->lineEdit_20->setToolTip("Tag DF21");
    frameWidth = ui->lineEdit_20->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_20->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonTACDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_20->setAlignment(Qt::AlignLeft);

    ui->checkBox20->setToolTip("Tag DF22");
    ui->lineEdit_21->setToolTip("Tag DF22");
    frameWidth = ui->lineEdit_21->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_21->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonTACDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_21->setAlignment(Qt::AlignLeft);

    ui->checkBox21->setToolTip("Tag DF04");
    ui->lineEdit_19->setToolTip("Tag DF04");

    ui->checkBox22->setToolTip("Tag DF05");
    ui->lineEdit_18->setToolTip("Tag DF05");
    for (int i = 20; i< 24; i++){
        m_listLineEditCheck.at(i)->setToolTip("Tag DF" + QString::number(i+3));
        m_listCheckBoxLine.at(i)->setToolTip("Tag DF" + QString::number(i+3));
    }

    ui->lineEdit_10->setInputMask("hhhh");
    ui->lineEdit_10->setText("0000");
    ui->lineEdit_10->setToolTip("Tag 9F09");
    ui->checkBox11->setToolTip("Tag 9F09");

    ui->lineEdit_9->setInputMask("hh");
    ui->lineEdit_9->setText("00");
    ui->lineEdit_9->setToolTip("Tag DF2C");
    ui->checkBox10->setToolTip("Tag DF2C");

    ui->lineEdit_8->setInputMask("hh");
    ui->lineEdit_8->setText("00");
    ui->lineEdit_8->setToolTip("Tag DF1E");
    ui->checkBox9->setToolTip("Tag DF1E");

    QRegExp rx("[A-Fa-f1-9]{0,32}");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->lineEdit_7->setValidator(validator);
    ui->lineEdit_7->setToolTip("Tag DF1A");
    ui->checkBox8->setToolTip("Tag DF1A");

    ui->lineEdit_6->setInputMask("hhhh");
    ui->lineEdit_6->setText("0000");
    ui->lineEdit_6->setToolTip("Tag 9F6D");
    ui->checkBox5->setToolTip("Tag 9F6D");

    ui->lineEdit_5->setInputMask("hhhhhh");
    ui->lineEdit_5->setText("000000");
    ui->lineEdit_5->setToolTip("Tag DF2D");
    ui->checkBox4->setToolTip("Tag DF2D");

    ui->lineEdit_4->setInputMask("hh");
    ui->lineEdit_4->setText("00");
    ui->lineEdit_4->setToolTip("Tag DF1B");
    ui->checkBox3->setToolTip("Tag DF1B");

    ui->lineEdit_3->setInputMask("hh");
    ui->lineEdit_3->setText("00");
    ui->lineEdit_3->setToolTip("Tag 9F7E");
    ui->checkBox2->setToolTip("Tag 9F7E");

    //line edit terminal capabilitie
    frameWidth = ui->lineEdit_2->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_2->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonAddTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_2->setAlignment(Qt::AlignLeft);
    ui->lineEdit_2->setInputMask("hhhhhhhhhh");
    ui->lineEdit_2->setText("00");
    ui->lineEdit_2->setToolTip("Tag 9F40");
    ui->checkBox1->setToolTip("Tag 9F40");

    ui->lineEdit->setInputMask("hhhh");
    ui->lineEdit->setText("0000");
    ui->lineEdit->setToolTip("Tag 9F1A");
    ui->checkBox0->setToolTip("Tag 9F1A");

    ui->checkBoxcombo1->setChecked(false);
    ui->checkBoxcombo1->setToolTip("Tag 9F35");
    ui->comboBox->setToolTip("Tag 9F35");

    ui->checkBoxCombo2->setChecked(false);
    ui->checkBoxCombo2->setToolTip("Tag DF0C");
    ui->comboBox_2->setToolTip("Tag DF0C");

    ui->checkBox17->setChecked(false);
    ui->checkBox17->setToolTip("Tag DF1D");
    ui->spinBox->setToolTip("Tag DF1D");

    qDebug() << "paypass " << m_listCheckBoxLine.length() << m_listLineEditCheck.length()
             << m_listCheckBoxCombo.length() << m_listCheckBoxSpin.length()
             << m_listComBoBox.length() << m_listSpin.length() << m_comboTerType.length();
    m_TLV.initializeList(m_listCheckBoxLine, m_listLineEditCheck,m_listCheckBoxCombo,
                         m_listCheckBoxSpin,m_listComBoBox,m_listSpin, m_comboTerType,
                         m_comboKerID,QStringList());
}

void cPaypassEntryPoint::InitializeToolButton()
{
   // button additional
   ui->toolButtonAddTerCap->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonAddTerCap->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonAddTerCap->setIconSize(QSize(ui->toolButtonAddTerCap->width(), ui->toolButtonAddTerCap->height()));
   ui->toolButtonAddTerCap->setToolTip("Tag 9F40");

   ui->toolButtonTACDefault->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonTACDefault->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonTACDefault->setIconSize(QSize(ui->toolButtonTACDefault->width(), ui->toolButtonTACDefault->height()));
   ui->toolButtonAddTerCap->setToolTip("Tag DF20");

   ui->toolButtonTACDenial->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonTACDenial->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonTACDenial->setIconSize(QSize(ui->toolButtonTACDenial->width(), ui->toolButtonTACDenial->height()));
   ui->toolButtonTACDenial->setToolTip("Tag DF21");

   ui->toolButtonTACOnline->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonTACOnline->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonTACOnline->setIconSize(QSize(ui->toolButtonTACOnline->width(), ui->toolButtonTACOnline->height()));
   ui->toolButtonTACOnline->setToolTip("Tag DF22");

   ui->toolButtonKerCon->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonKerCon->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonKerCon->setIconSize(QSize(ui->toolButtonKerCon->width(), ui->toolButtonKerCon->height()));
   ui->toolButtonKerCon->setToolTip("Tag DF1B");

   ui->toolButtonSecCap->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonSecCap->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonSecCap->setIconSize(QSize(ui->toolButtonSecCap->width(), ui->toolButtonSecCap->height()));
   ui->toolButtonSecCap->setToolTip("Tag DF03");

   ui->toolButtonCardData->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonCardData->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonCardData->setIconSize(QSize(ui->toolButtonCardData->width(), ui->toolButtonCardData->height()));
   ui->toolButtonCardData->setToolTip("Tag DF17");

   ui->toolButtonCVMCap->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonCVMCap->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonCVMCap->setIconSize(QSize(ui->toolButtonCVMCap->width(), ui->toolButtonCVMCap->height()));
   ui->toolButtonCVMCap->setToolTip("Tag DF18");

   ui->toolButtonCVMCapNo->setStyleSheet("QToolButton{border:none; padding: 0px}");
   ui->toolButtonCVMCapNo->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonCVMCapNo->setIconSize(QSize(ui->toolButtonCVMCapNo->width(), ui->toolButtonCVMCapNo->height()));
   ui->toolButtonCVMCapNo->setToolTip("Tag DF19");
}

/**
 * @brief cPaypassEntryPoint::CheckTagAvai
 * Get every TLV to update to checkbox or other TLV text edit
 * @param Buffertam
 * @param LengTotal
 */
void cPaypassEntryPoint::CheckTagAvai(uint8_t *Buffertam, uint16_t LengTotal)
{
    int Length =0, Sobyte =0;

    if (LengTotal == 0){
        return ;
    }
    for (int i =0; i < LengTotal; i = i+ Sobyte +1 + Length){

        if ((Buffertam[i] & 0x1F) == 0x1F){
            if ((Buffertam[i+1] & 0x80) == 0x80){
                Sobyte = 3;
            }

            else {
                Sobyte = 2;
            }
        }

        else {
            Sobyte = 1;
        }

        if ((Buffertam[i + Sobyte ]== 0x81)){
            Sobyte += 1;
        }

        Length = Buffertam[i + Sobyte];
        UpdateData(i, Sobyte, Buffertam, Length);
    }
}

/**
 * @brief cPaypassEntryPoint::UpdateData
 * update data, if a tag is not in the string lineEdit, the checkbox of this tag will be
 * unchecked, line Edit of this tag will be set to default.
 * @param Index
 * @param Sobyte
 * @param Buffertam
 * @param Length
 */

void cPaypassEntryPoint::UpdateData(uint8_t Index, uint8_t Sobyte, uint8_t *Buffertam, uint8_t Length)
{
    QString textLengthForUDOL;

    if (Sobyte == 2){
        switch (Buffertam[Index]){
        case 0x9F:
            switch (Buffertam[Index+1]){
            case 0x1A:
                if (Length ==2){
                  UpdateTextFinal(Length, Buffertam, "9F1A02", Index);
                  m_TLV.Updatetext(TerCounCode, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(TerCounCode, splitBoolLineEdit);
                    m_listLineEditCheck[TerCounCode]->setText("0000");
                }
            break;

            case 0x35:
                if (Length ==1){
                  m_TLV.updateComboType(ctTerType);
                  UpdateTextFinal(Length, Buffertam, "9F3501", Index);
                  m_TLV.Updatetext(TerCounCode , splitBoolComboBox, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_listComBoBox[0]->setCurrentText("00");
                    m_TLV.UncheckCheckbox(TerCounCode, splitBoolComboBox);
                }
            break;

            case 0x40:
                if (Length ==5){
                    UpdateTextFinal(Length, Buffertam, "9F4005", Index);
                  m_TLV.Updatetext(AddTerCap, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    m_listLineEditCheck[AddTerCap]->setText("000000000000");
                    m_TLV.UncheckCheckbox(AddTerCap, splitBoolLineEdit);
                }
            break;

            case 0x7E:
                if (Length ==1){
                  UpdateTextFinal(Length, Buffertam, "9F7E01", Index);
                  m_TLV.Updatetext(MobSupInd, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else if (Length >1){
                    m_listLineEditCheck[MobSupInd]->setText("00");
                    m_TLV.UncheckCheckbox(MobSupInd, splitBoolLineEdit);
                }
                else {
                    m_listLineEditCheck[MobSupInd]->setText("00");
                    m_listCheckBoxLine[MobSupInd]->setCheckState(Qt::PartiallyChecked);
                    m_listCheckBoxLine[MobSupInd]->setStyleSheet("QCheckBox{color: black}");
                }
            break;

            case 0x6D:
                if (Length ==2){
                    UpdateTextFinal(Length, Buffertam, "9F6D02", Index);
                    m_TLV.Updatetext(AppVerNumMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_listLineEditCheck[AppVerNumMastripe]->setText("0000");
                    m_TLV.UncheckCheckbox(AppVerNumMastripe, splitBoolLineEdit);
                }
            break;

            case 0x09:
                if (Length ==2){
                    UpdateTextFinal(Length, Buffertam, "9F0902", Index);
                   m_TLV.Updatetext(AppVerNumEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_listLineEditCheck[AppVerNumEMV]->setText("0000");
                    m_TLV.UncheckCheckbox(AppVerNumEMV, splitBoolLineEdit);
                }
            break;

            default:
                m_TLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
             break;
            }
        break;

        case 0xDF:
            switch(Buffertam[Index+1]){

            case 0x0C:
                 if (Length ==1){
                   UpdateTextFinal(Length, Buffertam, "DF0C01", Index);
                   m_TLV.updateComboType(ctKerID);
                   m_TLV.Updatetext(AddTerCap, splitBoolComboBox, Buffertam, Index + Sobyte +1, Length);
                 }
                 else{
                     m_listComBoBox[1]->setCurrentText("Mastercard");
                     m_TLV.UncheckCheckbox(AddTerCap, splitBoolComboBox);
                 }
            break;

            case 0x1B:
                if (Length ==1){
                    UpdateTextFinal(Length, Buffertam, "DF1B01", Index);
                    m_TLV.Updatetext(KerCon, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_listLineEditCheck[KerCon]->setText("00");
                    m_TLV.UncheckCheckbox(KerCon, splitBoolLineEdit);
                }
            break;

            case 0x2D:
                if (Length ==3){
                   UpdateTextFinal(Length, Buffertam, "DF2D03", Index);
                   m_TLV.Updatetext(MesHoldTime, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                   m_listLineEditCheck[MesHoldTime]->setText("000000");
                   m_TLV.UncheckCheckbox(MesHoldTime, splitBoolLineEdit);
                }
            break;

             case 0x1A:
                     if (Length < 0x0F)
                     {
                         textLengthForUDOL = "0" + QString::number(Length, 16).toUpper();
                     }
                     else
                     {
                         textLengthForUDOL = QString::number(Length, 16).toUpper();
                     }

                     UpdateTextFinal(Length, Buffertam, "DF1A" + textLengthForUDOL, Index);
                     m_TLV.Updatetext(DefUDOLMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
             break;

             case 0x1E:
                if (Length ==1){
                    UpdateTextFinal(Length, Buffertam, "DF1E01", Index);
                    m_TLV.Updatetext(CVMCapRequiredMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_listLineEditCheck[CVMCapRequiredMastripe]->setText("00");
                    m_TLV.UncheckCheckbox(CVMCapRequiredMastripe, splitBoolLineEdit);
                }
                break;

                case 0x2C:
                   if (Length ==1){
                     UpdateTextFinal(Length, Buffertam, "DF2C01", Index);
                     m_TLV.Updatetext(CVMCapNoRequiredMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[CVMCapNoRequiredMastripe]->setText("00");
                       m_TLV.UncheckCheckbox(CVMCapNoRequiredMastripe, splitBoolLineEdit);
                   }
                break;

                case 0x03:
                   if (Length == 1){
                     UpdateTextFinal(Length, Buffertam, "DF0301", Index);
                     m_TLV.Updatetext(SecCapEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else {
                       m_TLV.UncheckCheckbox(SecCapEMV, splitBoolLineEdit);
                   }
                break;

                case 0x17:
                   if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1701", Index);
                      m_TLV.Updatetext(CardDataEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[CardDataEMV]->setText("00");
                       m_TLV.UncheckCheckbox(CardDataEMV, splitBoolLineEdit);
                   }
                break;

                case 0x18:
                   if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1801", Index);
                      m_TLV.Updatetext(CVMCapRequiredEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[CVMCapRequiredEMV]->setText("00");
                       m_TLV.UncheckCheckbox(CVMCapRequiredEMV, splitBoolLineEdit);
                   }
                break;

                case 0x19:
                    if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1901", Index);
                      m_TLV.Updatetext(CVMCapNoRequiredEMVp, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                    }
                    else{
                        m_listLineEditCheck[CVMCapNoRequiredEMVp]->setText("00");
                        m_TLV.UncheckCheckbox(CVMCapNoRequiredEMVp, splitBoolLineEdit);
                    }
                break;

                case 0x1C:
                   if (Length ==2){
                      UpdateTextFinal(Length, Buffertam, "DF1C02", Index);
                      m_TLV.Updatetext(MaxLifeTimeEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[MaxLifeTimeEMV]->setText("0000");
                       m_TLV.UncheckCheckbox(MaxLifeTimeEMV, splitBoolLineEdit);
                   }
                break;

                case 0x1D:
                   if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1D01", Index);
                      m_TLV.Updatetext(TerCounCode, splitBoolSpinBox, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listSpin[0]->setValue(0);
                       m_TLV.UncheckCheckbox(TerCounCode, splitBoolSpinBox);
                   }
                break;

                case 0x20:
                   if (Length ==5){
                     UpdateTextFinal(Length, Buffertam, "DF2005", Index);
                     m_TLV.Updatetext(TACDefEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[TACDefEMV]->setText("0000000000");
                       m_TLV.UncheckCheckbox(TACDefEMV, splitBoolLineEdit);
                   }
                break;

                case 0x21:
                   if (Length == 5){
                     UpdateTextFinal(Length, Buffertam, "DF2105", Index);
                     m_TLV.Updatetext(TACDenialEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[TACDenialEMV]->setText("0000000000");
                       m_TLV.UncheckCheckbox(TACDenialEMV, splitBoolLineEdit);
                   }
                break;

                case 0x22:
                   if (Length ==5){
                     UpdateTextFinal(Length, Buffertam, "DF2205", Index);
                     m_TLV.Updatetext(TACOnlEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[TACOnlEMV]->setText("0000000000");
                       m_TLV.UncheckCheckbox(TACOnlEMV, splitBoolLineEdit);
                   }
                break;

                case 0x04:
                   if (Length ==6){
                     UpdateTextFinal(Length, Buffertam, "DF0406", Index);
                     m_TLV.Updatetext(BalReadBefACEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[BalReadBefACEMV]->setText("000000000000");
                       m_TLV.UncheckCheckbox(BalReadBefACEMV, splitBoolLineEdit);
                   }
                break;

                case 0x05:
                   if (Length ==6){
                     UpdateTextFinal(Length, Buffertam, "DF0506", Index);
                     m_TLV.Updatetext(BalReadAftACEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[BalReadAftACEMV]->setText("000000000000");
                       m_TLV.UncheckCheckbox(BalReadAftACEMV, splitBoolLineEdit);
                   }
                break;

                case 0x23:
                  if (Length ==6){
                    UpdateTextFinal(Length, Buffertam, "DF2306", Index);
                    m_TLV.Updatetext(ReadConFloor, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                  }
                  else{
                      m_listLineEditCheck[ReadConFloor]->setText("000000000000");
                      m_TLV.UncheckCheckbox(ReadConFloor, splitBoolLineEdit);
                  }
                break;

                case 0x24:
                  if (Length ==6){
                    UpdateTextFinal(Length, Buffertam, "DF2406", Index);
                    qDebug() << "den day van on  " << Length;
                    m_TLV.Updatetext(RCTLNo, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                  }
                  else{
                      m_listLineEditCheck[RCTLNo]->setText("000000000000");
                      m_TLV.UncheckCheckbox(RCTLNo, splitBoolLineEdit);
                  }
                break;

                case 0x25:
                   if (Length == 6){
                     UpdateTextFinal(Length, Buffertam, "DF2506", Index);
                     qDebug()  << "den day van on";
                     m_TLV.Updatetext(RCTL, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_listLineEditCheck[RCTL]->setText("000000000000");
                       m_TLV.UncheckCheckbox(RCTL, splitBoolLineEdit);
                   }
                break;

                case 0x26:
                    if (Length ==6){
                      UpdateTextFinal(Length, Buffertam, "DF2606", Index);
                      m_TLV.Updatetext(ReadCVMLimit, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                    }
                    else{
                        m_listLineEditCheck[ReadCVMLimit]->setText("0000000000");
                        m_TLV.UncheckCheckbox(ReadCVMLimit, splitBoolLineEdit);
                    }
                break;

                default:
                    m_TLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
                break;

            }
            break;

        default:
            m_TLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
            break;
        }
    }
}

/**
 * @brief cPaypassEntryPoint::ChangeStyleSheetLineEdit
 * @param index
 * @param text
 * @param length
 * @param widgetType
 * @return
 */
int cPaypassEntryPoint::ChangeStyleSheetLineEdit(int index, QString text,
                                                 int length, int widgetType)
{
   int indText =0;
   QString textKerID, textSpinbox;

   if (widgetType == LINEEDIT){
       if (m_listCheckBoxLine[index]->checkState())
       {
           if ((indText = textFinal.indexOf(text, indText)) != -1)
           {   
                if ("DF1A" != text)
                {
                   textFinal.replace(indText + 6, length*2, m_listLineEditCheck[index]->text());
                }
           }

           else
           {
               if ("DF1A" != text)
               {
                  textFinal += text + "0" + QString::number(length) + m_listLineEditCheck[index]->text();
               }
               else
               {
                   textFinal += "DF1A00";
               }
           }
       }

       else
       {
           if ((indText = textFinal.indexOf(text, indText)) != -1)
           {
               if ("DF1A" != text)
               {
                 textFinal.remove(indText, 6+ length*2);
               }
               else
               {
                   textFinal.remove(indText, textFixForDefUDOL.length());
               }
           }
       }
   }
   else if (widgetType == COMBOBOX)
   {
       if (text == "9F35")
       {
           if (ui->checkBoxcombo1->checkState())
           {
               if ((indText = textFinal.indexOf(text, indText)) != -1){
                   textFinal.replace(indText + 6, length*2, ui->comboBox->currentText());

               }
               else
               {
                   textFinal += text + "0" + QString::number(length) + ui->comboBox->currentText();
               }
       }

           else
           {
              if ((indText = textFinal.indexOf(text, indText)) != -1)
              {
                  textFinal.remove(indText, 6+ length*2);
              }
           }
       }
       else
       {
           if (ui->checkBoxCombo2->checkState())
           {
             for (int i =0; i< m_comboKerID.count(); i++)
             {
               if (ui->comboBox_2->currentText() == m_comboKerID[i])
               {
                   if ((i +1) < 0x0F)
                   {
                   textKerID = "0" + QString::number(i+1, 16);
                   }
                   else
                   {
                       textKerID = QString::number(i+1, 16);
                   }
               }
           }

           if ((indText = textFinal.indexOf(text, indText)) != -1)
           {
               textFinal.replace(indText +6, length*2, textKerID);
           }
           else
           {
               textFinal += text + "0" + QString::number(length) + textKerID;
           }
       }

      else
           {
            if ((indText = textFinal.indexOf(text, indText)) != -1)
            {
               textFinal.remove(indText, 6+ length*2);
            }
       }
   }
   }

   else
   {
       if (ui->checkBox17->checkState())
       {
           if (ui->spinBox->value()< 0x0F)
           {
               textSpinbox = "0" + QString::number(ui->spinBox->value(), 16);
           }
           else
           {
               textSpinbox = QString::number(ui->spinBox->value());
           }

           if ((indText = textFinal.indexOf(text, indText)) != -1)
           {
               textFinal.replace(indText +6, length*2, textSpinbox);
           }
           else
           {
               textFinal += text + "0" + QString::number(length) + textSpinbox;
           }
       }

       else
       {
           if ((indText = textFinal.indexOf(text, indText)) != -1)
           {
               textFinal.remove(indText, 6+ length*2);
           }
       }
   }

   return 0;
}

/**
 * @brief cPaypassEntryPoint::UpdateTextFinal
 * we got textFinal, after doing something in the dialog. the textFinal
 * will be updated to print to line edit Entry Point Configuration.
 * @param length
 * @param Buffertam
 * @param text
 * @param Index
 */
void cPaypassEntryPoint::UpdateTextFinal(int length, uint8_t *Buffertam, QString text, uint8_t Index)
{
    bool   defUDOL= false;
    QString textNeededToAdd;

    textFinal += text;
    if (-1 != text.indexOf("DF1A"))
    {
        textFixForDefUDOL  += text;
        defUDOL = true;
    }

    for (int i =0; i< length; i++){
        if (Buffertam[Index + 2 +1 +i] > 0x0F)
        {
            textNeededToAdd= QString::number(Buffertam[Index +2+1+i], 16).toUpper();

        }
        else
        {
            textNeededToAdd ="0" +QString::number(Buffertam[Index +2 +1+i], 16).toUpper();

        }

        textFinal += textNeededToAdd;

        if (true == defUDOL)
        {
            textFixForDefUDOL += textNeededToAdd;
        }
    }
}

void cPaypassEntryPoint::updateBuffer(uint16_t length, uint8_t bufferTemp[])
{
    for (qint32 i =0; i < 200; i++)
    {
        m_bufferTemp[i] = bufferTemp[i];
    }
    m_length = length;
}

/**
 * @brief
 * return: None
 */
void cPaypassEntryPoint::UpdateData(qint32 indexUseForOkButton)
{
    QList<QCheckBox*>    m_listCheckBoxHasButton;
    QList<QToolButton*>  listToolButton;

    m_listCheckBoxHasButton << ui->checkBox1 << ui->checkBox3 << ui->checkBox12
                          << ui->checkBox13
                          << ui->checkBox14 << ui->checkBox15 << ui->checkBox18
                          << ui->checkBox19 << ui->checkBox20;

    listToolButton        << ui->toolButtonAddTerCap << ui->toolButtonKerCon
                          << ui->toolButtonSecCap << ui->toolButtonCardData
                          << ui->toolButtonCVMCap << ui->toolButtonCVMCapNo
                          << ui->toolButtonTACDefault << ui->toolButtonTACDenial
                          << ui->toolButtonTACOnline;

    m_indexWidget = indexUseForOkButton;
    textFinal = "";
    CheckTagAvai(m_bufferTemp, m_length);
    ui->textEdit->setText(m_TLV.text);
    m_TLV.UncheckTag();

    for (qint32 i =0; i< listToolButton.count(); i++)
    {
        if (false == m_listCheckBoxHasButton.at(i)->checkState())
        {
            listToolButton.at(i)->setEnabled(false);
        }
    }

}

void cPaypassEntryPoint::UpdateAddTerCap(const QString &text)
{
    ui->lineEdit_2->setText(text);
}

void cPaypassEntryPoint::UpdateTACData(const QString &text)
{
    if (m_lineEditChoseTAC == 1){
        ui->lineEdit_15->setText(text);
    }
    else if (m_lineEditChoseTAC == 2){
        ui->lineEdit_20->setText(text);
    }
    else ui->lineEdit_21->setText(text);
}


void cPaypassEntryPoint::Update1ByteLineEdit()
{
    //ui->lineEdit_11->setText(SecCap->scTextLineEdit);
}

void cPaypassEntryPoint::UpdateCardData(const QString &text)
{
    ui->lineEdit_13->setText(text);
}

void cPaypassEntryPoint::UpdateSecCap(const QString &text)
{
    ui->lineEdit_11->setText(text);
}

void cPaypassEntryPoint::UpdateCVMCap(const QString &text)
{
    switch(m_lineEditChoseCVM){
    case 1:
        ui->lineEdit_12->setText(text);
        break;
    case 2:
        ui->lineEdit_17->setText(text);
        break;
    }
}

void cPaypassEntryPoint::UpdateKerConfig(const QString &text)
{
    ui->lineEdit_4->setText(text);
}

void cPaypassEntryPoint::checkbox_update_lineEdit(int index)
{
    if (m_listCheckBoxLine[index]->checkState()== Qt::Checked){
        m_listCheckBoxLine[index]->setStyleSheet("QCheckBox{color: black}");
        m_listLineEditCheck[index]->setEnabled(true);
        m_listLineEditCheck[index]->setStyleSheet("border: 1px solid black");
        switch (index){
        case 1:
             ui->toolButtonAddTerCap->setEnabled(true);
            break;
        case 3:
             ui->toolButtonKerCon->setEnabled(true);
             break;
        case 10:
             ui->toolButtonSecCap->setEnabled(true);
            break;
        case 11:
             ui->toolButtonCardData->setEnabled(true);
            break;
        case 12:
             ui->toolButtonCVMCap->setEnabled(true);
            break;
        case 13:
             ui->toolButtonCVMCapNo->setEnabled(true);
            break;
        case 15:
             ui->toolButtonTACDefault->setEnabled(true);
            break;
        case 16:
             ui->toolButtonTACDenial->setEnabled(true);
            break;
        case 17:
             ui->toolButtonTACOnline->setEnabled(true);
            break;
        }

    }

    else {
        m_listCheckBoxLine[index]->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        m_listLineEditCheck[index]->setEnabled(false);
        m_listLineEditCheck[index]->setStyleSheet("border: 1px solid rgb(210, 210, 210)");
        switch (index){
        case 1:
             ui->toolButtonAddTerCap->setEnabled(false);
            break;
        case 3:
             ui->toolButtonKerCon->setEnabled(false);
             break;
        case 10:
             ui->toolButtonSecCap->setEnabled(false);
            break;
        case 11:
             ui->toolButtonCardData->setEnabled(false);
            break;
        case 12:
             ui->toolButtonCVMCap->setEnabled(false);
            break;
        case 13:
             ui->toolButtonCVMCapNo->setEnabled(false);
            break;
        case 15:
             ui->toolButtonTACDefault->setEnabled(false);
            break;
        case 16:
             ui->toolButtonTACDenial->setEnabled(false);
            break;
        case 17:
             ui->toolButtonTACOnline->setEnabled(false);
            break;
        }
    }


}

void cPaypassEntryPoint::button_ok_clicked()
{

}

void cPaypassEntryPoint::on_toolButtonAddTerCap_clicked()
{
    AddTer->show();
    emit ToolButtonAddTerCap_clicked(ui->lineEdit_2->text(),  waPayPass);
}

void cPaypassEntryPoint::on_toolButtonTACDefault_clicked()
{
    TACWin->show();
    m_lineEditChoseTAC = 1;
    emit ToolButtonTACDefault_clicked(ui->lineEdit_15->text(), wdPayPass);
}

void cPaypassEntryPoint::on_toolButtonTACDenial_clicked()
{
    TACWin->show();
    m_lineEditChoseTAC = 2;
    emit ToolButtonTACDenial_clicked(ui->lineEdit_20->text(), wdPayPass);
}

void cPaypassEntryPoint::on_toolButtonTACOnline_clicked()
{
    TACWin->show();
    m_lineEditChoseTAC = 3;
    emit ToolButtonTACOnline_clicked(ui->lineEdit_21->text(), wdPayPass);
}

void cPaypassEntryPoint::on_toolButtonSecCap_clicked()
{
   winSecCap->show();
   emit ToolButtonSecCap_clicked(ui->lineEdit_11->text());
}

void cPaypassEntryPoint::on_toolButtonCardData_clicked()
{
    winCardData->show();
    emit ToolButtonCardData_clicked(ui->lineEdit_13->text());
}

void cPaypassEntryPoint::on_toolButtonCVMCap_clicked()
{
    winCVMCap->show();
    emit ToolButtonnCVMCap_clicked(ui->lineEdit_12->text());
    m_lineEditChoseCVM = 1;
}

void cPaypassEntryPoint::on_toolButtonCVMCapNo_clicked()
{
    winCVMCap->show();
    emit ToolButtonnCVMCap_clicked(ui->lineEdit_17->text());
    m_lineEditChoseCVM = 2;
}

void cPaypassEntryPoint::on_toolButtonKerCon_clicked()
{
    winKerConfig->show();
    emit ToolButtonKerConfig_clicked(ui->lineEdit_4->text());
}

void cPaypassEntryPoint::on_pushButton_clicked()
{
    QString  textOtherTLVTemp;

    for (int i =0; i < m_listCheckBoxLine.count(); i++){
    switch (i){
    case 0:
        ChangeStyleSheetLineEdit(i, "9F1A", 2, LINEEDIT);
        break;

    case 1:
        ChangeStyleSheetLineEdit(i, "9F40", 5, LINEEDIT);
        break;

    case 2:
        ChangeStyleSheetLineEdit(i, "9F7E", 1, LINEEDIT);
        break;

    case 3:
        ChangeStyleSheetLineEdit(i, "DF1B", 1, LINEEDIT);
        break;

    case 4:
        ChangeStyleSheetLineEdit(i, "DF2D", 3, LINEEDIT);
        break;

    case 5:
        ChangeStyleSheetLineEdit(i, "9F6D", 2, LINEEDIT);
        break;
    case 6:
        ChangeStyleSheetLineEdit(i, "DF1A", 3, LINEEDIT);
        break;

    case 7:
        ChangeStyleSheetLineEdit(i, "DF1E", 1, LINEEDIT);
        break;

    case 8:
        ChangeStyleSheetLineEdit(i, "DF2C", 1, LINEEDIT);
        break;

    case 9:
        ChangeStyleSheetLineEdit(i, "9F09", 2, LINEEDIT);
        break;

    case 10:
        ChangeStyleSheetLineEdit(i, "DF03", 1, LINEEDIT);
        break;

    case 11:
        ChangeStyleSheetLineEdit(i, "DF17", 1,LINEEDIT);
        break;

    case 12:
        ChangeStyleSheetLineEdit(i, "DF18", 1, LINEEDIT);
        break;

    case 13:
        ChangeStyleSheetLineEdit(i, "DF19", 1, LINEEDIT);
        break;

    case 14:
        ChangeStyleSheetLineEdit(i, "DF1C", 2, LINEEDIT);
        break;

    case 15:
        ChangeStyleSheetLineEdit(i, "DF20", 5, LINEEDIT);
        break;

    case 16:
        ChangeStyleSheetLineEdit(i, "DF21", 5, LINEEDIT);
        break;

    case 17:
        ChangeStyleSheetLineEdit(i, "DF22", 5, LINEEDIT);
        break;

    case 18:
        ChangeStyleSheetLineEdit(i, "DF04", 6,LINEEDIT);
        break;

    case 19:
        ChangeStyleSheetLineEdit(i, "DF05", 6, LINEEDIT);
        break;

    case 20:
        ChangeStyleSheetLineEdit(i, "DF23", 6, LINEEDIT);
        break;

    case 21:
        ChangeStyleSheetLineEdit(i, "DF24", 6, LINEEDIT);
        break;

    case 22:
        ChangeStyleSheetLineEdit(i, "DF25", 6, LINEEDIT);
        break;

    case 23:
        ChangeStyleSheetLineEdit(i, "DF26", 6, LINEEDIT);
        break;
    }
    }

    ChangeStyleSheetLineEdit(0, "9F35", 1, COMBOBOX);
    ChangeStyleSheetLineEdit(0, "DF0C", 1, COMBOBOX);
    ChangeStyleSheetLineEdit(0, "DF1D", 1, SPINBOX);

    textOtherTLVTemp = ui->textEdit->toPlainText();
    if (0!= (textOtherTLVTemp.length() %2))
    {
        textOtherTLVTemp += "0";
    }

    textFinal += textOtherTLVTemp;
    emit buttonOk_clicked(m_indexWidget, textFinal);
    qDebug() << textFinal << "  " << m_indexWidget;
    this->close();
}

void cPaypassEntryPoint::on_checkBoxcombo1_clicked()
{
    if (ui->checkBoxcombo1->checkState()){
        ui->checkBoxcombo1->setStyleSheet("QCheckBox{color: black}");
        ui->comboBox->setEnabled(true);
    }
    else {
        ui->checkBoxcombo1->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        ui->comboBox->setEnabled(false);
    }
}

void cPaypassEntryPoint::on_checkBoxCombo2_clicked()
{
    if (ui->checkBoxCombo2->checkState()){
        ui->checkBoxCombo2->setStyleSheet("QCheckBox{color: black}");
        ui->comboBox_2->setEnabled(true);
    }
    else {
        ui->checkBoxCombo2->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        ui->comboBox_2->setEnabled(false);
    }
}

void cPaypassEntryPoint::on_checkBox17_clicked()
{
    if (ui->checkBox17->checkState()){
        ui->checkBox17->setStyleSheet("QCheckBox{color: black}");
        ui->spinBox->setEnabled(true);
    }
    else{
        ui->checkBox17->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        ui->spinBox->setEnabled(false);
    }
}


void cPaypassEntryPoint::on_pushButton_2_clicked()
{
    this->close();
}

void cPaypassEntryPoint::on_textEdit_textChanged()
{
    bool state = false;
    quint8        buffertTemp[256] ={0};
    QString      textTemp;

    textTemp   = ui->textEdit->toPlainText();
    qDebug() << "here" ;
    state      = m_testTLV.convertStringtoNumAndCheck(textTemp, buffertTemp);

    if (false == state)
    {
        ui->textEdit->setStyleSheet("color: red;border: 1px solid black; border-radius: 3px");
    }
    else
    {
        ui->textEdit->setStyleSheet("color:black;border: 1px solid black;border-radius: 3px");
    }
}

void cPaypassEntryPoint::on_lineEdit_7_textChanged(const QString &arg1)
{


}

void cPaypassEntryPoint::on_checkBox21_clicked()
{

}
