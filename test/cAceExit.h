#ifndef CACEEXIT_H
#define CACEEXIT_H

#include <QDialog>

namespace Ui {
class cAceExit;
}

class cAceExit : public QDialog
{
    Q_OBJECT

public:
    explicit cAceExit(QWidget *parent = 0);
    ~cAceExit();

signals:
    void buttonOK_clicked();
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::cAceExit *ui;
};

#endif // CACEEXIT_H
