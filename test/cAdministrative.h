#ifndef CADMINISTRATIVE_H
#define CADMINISTRATIVE_H

#include <QDialog>
#include <QSignalMapper>
#include <QString>
#include <QCheckBox>
#include <QList>
#include <QLineEdit>

namespace Ui {
class cAdministrative;
}

class cAdministrative : public QDialog
{
    Q_OBJECT

public:
    explicit cAdministrative(QWidget *parent = 0);
    ~cAdministrative();

    void checkAndUncheck(const QString text, QList<QCheckBox *> *listCheck,
                         QLineEdit *line, qint32 ith);

private:
    Ui::cAdministrative *ui;

    QString m_textLineEdit;

    QList<QCheckBox*> m_listCheckBoxMSB;
    QList<QCheckBox*> m_listCheckBoxMiddle1;
    QList<QCheckBox*> m_listCheckBoxMiddle2;
    QList<QCheckBox*> m_listCheckBoxMiddle3;
    QList<QCheckBox*> m_listCheckBoxMiddle4;
    QList<QCheckBox*> m_listCheckBoxLSB;


    QSignalMapper *m_sigMappMSB;
    QSignalMapper *m_sigMappMiddle1;
    QSignalMapper *m_sigMappMiddle2;
    QSignalMapper *m_sigMappMiddle3;
    QSignalMapper *m_sigMappMiddle4;
    QSignalMapper *m_sigMappLSB;

    QString m_stringMSB, m_stringMiddle1, m_stringMiddle2, m_stringMiddle3;
    QString m_stringMiddle4, m_stringLSB;

public slots:

    void checkBoxMSBClicked(qint32 index);
    void checkBoxMiddle1Clicked(qint32 index);
    void checkBoxMiddle2Clicked(qint32 index);
    void checkBoxMiddle3Clicked(qint32 index);
    void checkBoxMiddle4Clicked(qint32 index);
    void checkBoxLSBClicked(qint32 index);

    void updateTextLineEdit(const QString &string);

private slots:
    void on_lineEdit_DefTran_textChanged(const QString &arg1);
    void on_lineEdit_TraceDep_textChanged(const QString &arg1);
    void on_lineEdit_DefLan_textChanged(const QString &arg1);
    void on_lineEdit_Polling_textChanged(const QString &arg1);
    void on_lineEdit_textChanged(const QString &arg1);

    void on_pushButton_clicked();

    void on_lineEdit_cursorPositionChanged(int arg1, int arg2);

    void on_lineEdit_textEdited(const QString &arg1);

signals:
    void closeButtonClicked(const QString &text);

};

#endif // CADMINISTRATIVE_H
