#ifndef cTerminalCapabilities_H
#define cTerminalCapabilities_H

#include <QDialog>
#include <QString>
#include <QCheckBox>
#include <QList>
#include <QStringList>

namespace Ui {
class cTerminalCapabilities;
}

class cTerminalCapabilities : public QDialog
{
    Q_OBJECT
signals:
    void ButtonOk_Clicked(const QString &text);

public:
    explicit cTerminalCapabilities(QWidget *parent = 0);
    ~cTerminalCapabilities();
    QString TextLineEdit;
    void InitilizeCheckBox();
    int SetCheckbox(const QString &text, int Pos, int Num);

    void updateCheckBoxBasedOnLineEdit(const QString &text, QList<QCheckBox*> *list);

public slots:
    void ChangeTextEdit(const QString &text);


private slots:
    void on_pushButtonOkConfig_clicked();

    void on_pushButtonCancelConfig_clicked();

    void on_checkBoxManual_clicked();

    void on_checkBoxMagnetic_clicked();

    void on_checkBoxICC_clicked();

    void on_checkBoxRFU1_clicked();

    void on_checkBoxRFU2_clicked();

    void on_checkBoxRFU3_clicked();

    void on_checkBoxRFU4_clicked();

    void on_checkBoxRFU5_clicked();

    void on_checkBoxSDA_clicked();

    void on_checkBoxDDA_clicked();

    void on_checkBoxCard_clicked();

    void on_checkBoxRFU9_clicked();

    void on_checkBoxCDA_clicked();

    void on_checkBoxRFU10_clicked();

    void on_checkBoxRFU11_clicked();

    void on_checkBoxRFU12_clicked();

    void on_checkBoxPlaintext_clicked();

    void on_checkBoxEncOnline_clicked();

    void on_checkBoxSingature_clicked();

    void on_checkBoxEncOffline_clicked();

    void on_checkBoxNoCVM_clicked();

    void on_checkBoxRFU6_clicked();

    void on_checkBoxRFU7_clicked();

    void on_checkBoxRFU8_clicked();

    void on_lineEdit_textEdited(const QString &arg1);

private:
    Ui::cTerminalCapabilities *ui;
    unsigned char m_headNum =0, m_middleNum =0, m_tailNum =0;
    QString m_headText , m_middleText, m_tailText;

    QString m_textLineEdit;

    QList<QCheckBox*> m_checkBoxCol1st;
    QList<QCheckBox*> m_checkBoxCol2nd;
    QList<QCheckBox*> m_checkBoxCol3rd;

};


#endif // cTerminalCapabilities_H
