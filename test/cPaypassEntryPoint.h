#ifndef cPaypassEntryPoint_H
#define cPaypassEntryPoint_H

#include <QDialog>
#include "cSplitTag.h"
#include <QtCore>
#include <QtGui>
#include <QColor>
#include "cAdditionalTerCapabilities.h"
#include "cTerminalActionCode.h"
#include "cCardDataInputCapabilities.h"
#include "cSecurityCapabilities.h"
#include "cCvmCapabilities.h"
#include "cKernelConfig.h"
#include <QSignalMapper>
#include <QLabel>
#include <QToolButton>
#include <QLineEdit>
#include <QRegExp>
#include <QValidator>
#include <QRegExpValidator>
#include "cTestOtherTlv.h"

namespace Ui {
class cPaypassEntryPoint;
}

#define LINEEDIT   0
#define COMBOBOX   1
#define SPINBOX    2

typedef enum{
    TerCounCode                 = 0x00,
    AddTerCap                   = 0x01,
    MobSupInd                   = 0x02,
    KerCon                      = 0x03,
    MesHoldTime                 = 0x04,
    AppVerNumMastripe           = 0x05,
    DefUDOLMastripe             = 0x06,
    CVMCapRequiredMastripe      = 0x07,
    CVMCapNoRequiredMastripe    = 0x08,
    AppVerNumEMV                = 0x09,
    SecCapEMV                   = 0x0A,
    CardDataEMV                 = 0x0B,
    CVMCapRequiredEMV           = 0x0C,
    CVMCapNoRequiredEMVp        = 0x0D,
    MaxLifeTimeEMV              = 0x0E,
    TACDefEMV                   = 0x0F,
    TACDenialEMV                = 0x10,
    TACOnlEMV                   = 0x11,
    BalReadBefACEMV             = 0x12,
    BalReadAftACEMV             = 0x13,
    ReadConFloor                = 0x14,
    RCTLNo                      = 0x15,
    RCTL                        = 0x16,
    ReadCVMLimit                = 0x17,

}
IndexCheckBox;



class cPaypassEntryPoint : public QDialog
{
    Q_OBJECT

public:
    explicit cPaypassEntryPoint(QWidget *parent = 0);
    ~cPaypassEntryPoint();

     void InitializePaypass();

     void InitializeToolButton();

     void CheckTagAvai(uint8_t *Buffertam, uint16_t LengTotal);

     void UpdateData(uint8_t Index, uint8_t Sobyte, uint8_t *Buffertam, uint8_t Length);

     int ChangeStyleSheetLineEdit(int index, QString text, int length, int widgetType);

     void UpdateTextFinal(int length, uint8_t *Buffertam, QString text, uint8_t Index);

     void updateBuffer(uint16_t length, uint8_t bufferTemp[]);



private:
    Ui::cPaypassEntryPoint         *ui;

    cAdditionalTerCapabilities     *AddTer;
    cTerminalActionCode            *TACWin;
    cCardDataInputCapabilities     *winCardData;
    cSecurityCapabilities          *winSecCap;
    cCvmCapabilities               *winCVMCap;
    cKernelConfig                  *winKerConfig;
    cSplitTag                       m_TLV;
    cTestOtherTLV                   m_testTLV;

    uint8_t                         m_lineEditChoseTAC ;
    uint8_t                         m_lineEditChoseCVM ;

    QStringList                     m_comboTerType;   /**/
    QStringList                     m_comboKerID;     /**/

    QList <QCheckBox*>               m_listCheckBox;   /**/
    QList <QLineEdit*>               m_listLineEditCheck;  /**/
    QList <QCheckBox*>               m_listCheckBoxLine;
    QList <QCheckBox*>               m_listCheckBoxCombo;
    QList <QCheckBox*>               m_listCheckBoxSpin;
    QList <QComboBox*>               m_listComBoBox;
    QList <QSpinBox*>                m_listSpin;

    QSignalMapper                   *sigMappLineEdit;

    QString                         textFinal ;
    QString                         textFixForDefUDOL;

    qint32                          m_indexWidget;

    uint8_t                         m_bufferTemp[200];
    uint16_t                        m_length;


public slots:
    void UpdateData(qint32 indexUseForOkButton);

    void UpdateAddTerCap(const QString &text);

    void UpdateTACData(const QString &text);

    void Update1ByteLineEdit();

    void UpdateCardData(const QString &text);

    void UpdateSecCap(const QString &text);

    void UpdateCVMCap(const QString &text);

    void UpdateKerConfig(const QString &text);

    void checkbox_update_lineEdit(int index);

    void button_ok_clicked();

private slots:
    void on_toolButtonAddTerCap_clicked();

    void on_toolButtonTACDefault_clicked();

    void on_toolButtonTACDenial_clicked();

    void on_toolButtonTACOnline_clicked();

    void on_toolButtonSecCap_clicked();

    void on_toolButtonCardData_clicked();

    void on_toolButtonCVMCap_clicked();

    void on_toolButtonCVMCapNo_clicked();

    void on_toolButtonKerCon_clicked();

    void on_pushButton_clicked();

    void on_checkBoxcombo1_clicked();

    void on_checkBoxCombo2_clicked();

    void on_checkBox17_clicked();

    void on_pushButton_2_clicked();

    void on_textEdit_textChanged();

    void on_lineEdit_7_textChanged(const QString &arg1);

    void on_checkBox21_clicked();

signals:
    void ToolButtonAddTerCap_clicked(const QString &text, mlsWinAdd_t winselected);

    void ToolButtonTACDefault_clicked(const QString &text, mlsWindowDef_t winSelected);

    void ToolButtonTACDenial_clicked(const QString &text, mlsWindowDef_t winSelected);

    void ToolButtonTACOnline_clicked(const QString &text, mlsWindowDef_t winSelected);

    void ToolButtonSecCap_clicked(const QString &text);

    void ToolButtonCardData_clicked(const QString &text);

    void ToolButtonnCVMCap_clicked(const QString &text);

    void ToolButtonKerConfig_clicked(const QString &text);

    void buttonOk_clicked(qint32 index, QString textFinal);
};

#endif // cPaypassEntryPoint_H
