#include "cHelpInfo.h"
#include "ui_cHelpInfo.h"

cHelpInfo::cHelpInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cHelpInfo)
{
    ui->setupUi(this);

    this->setWindowTitle("About SEPOSS");

    ui->label_system->setText("STYL EMV POS System");
    ui->label_version->setText("1.0.0");
    ui->label_name->setText("SYTL");
    ui->label_office->setText("The Rep. office of STYL Solutions Pte Ltd in HCMC");
    ui->label_address->setText("109 Luong Dinh Cua, Binh An Ward, District 2, HCMC, Vietnam");
    ui->label_tel->setText("Tel: +84 8 62917497 Fax: +84 8 62917498 ");
}

cHelpInfo::~cHelpInfo()
{
    delete ui;
}
