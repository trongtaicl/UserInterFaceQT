#include "cTerminalTransactionQualifier.h"
#include "ui_cTerminalTransactionQualifier.h"

cTerminalTransactionQualifier::cTerminalTransactionQualifier(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cTerminalTransactionQualifier)
{
    ui->setupUi(this);
}

cTerminalTransactionQualifier::~cTerminalTransactionQualifier()
{
    delete ui;
}
