#include "cCvmCapabilities.h"
#include "ui_cCvmCapabilities.h"

cCvmCapabilities::cCvmCapabilities(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cCvmCapabilities)
{
    ui->setupUi(this);
    this->setWindowTitle("CVM Capabilities");
    /* add all the checkbox into m_listCheckBoxCVMCap */
    m_listCheckBoxCVMCap << ui->checkBox << ui->checkBox_2 << ui->checkBox_3 << ui->checkBox_4
                         << ui->checkBox_5 << ui->checkBox_6 << ui->checkBox_7 << ui->checkBox_8;

    /* initialize the lineEdit */
    ui->lineEdit->setInputMask("hh");
    ui->lineEdit->setText("00");
    ui->lineEdit->setAlignment(Qt::AlignRight);

    m_sigMapp = new QSignalMapper(this);

    for (int i =0; i< 8; i++){
        m_sigMapp->setMapping(m_listCheckBoxCVMCap[i], i);
       /* connect every checkbox click signal with map() */
        connect(m_listCheckBoxCVMCap[i], SIGNAL(clicked()), m_sigMapp, SLOT(map()));
    }

    connect(m_sigMapp, SIGNAL(mapped(int)), this, SLOT(check_box_update(int)));
}

cCvmCapabilities::~cCvmCapabilities()
{
    delete ui;
}

/**
 * @brief cCvmCapabilities::check_box_update
 * @param index: checkbox in index position is clicked.
 * @return: none
 */
void cCvmCapabilities::check_box_update(int index)
{
    bool stateCheckbox =false;
    int num =0;
    for (int i =0; i< 8; i++){
        stateCheckbox = m_listCheckBoxCVMCap[i]->checkState();

        if (stateCheckbox){   /* if ith checkbox is checked, ith bit of Num will be 1 */
            num |= (1<<i);
        }
        else { /* if ith checkbox is unchecked, ith bit of Num will be 0 */
            num &= ~(1<<i);
        }
    }
    /* update value of Num to LineEdit */
    ui->lineEdit->setText(QString("%1").arg(num, 2, 16, QChar('0')).toUpper());
}

/**
 * @brief cCvmCapabilities::cvmChangeTextEdit
 * set text of cvmTextLineEdit into text of lineedit and check the checkboxes in this window
 * @return: none
 */
void cCvmCapabilities::cvm_change_text_edit(const QString &text)
{
    m_textLineEdit = text;
    int numTemp = m_textLineEdit.toUInt(NULL, 16);

    ui->lineEdit->setText(m_textLineEdit);

    for (int i =0; i< m_listCheckBoxCVMCap.length(); i++){
        m_listCheckBoxCVMCap[i]->setChecked((numTemp & (1 << i)));
    }
}
/**
 * @brief cCvmCapabilities::on_pushButton_clicked
 * Button Ok is clicked, emit ButtonOk_CVMCap_clicked()
 * close this window
 */

void cCvmCapabilities::on_pushButton_clicked()
{
    m_textLineEdit = ui->lineEdit->text();
    emit ButtonOk_CVMCap_clicked(ui->lineEdit->text());
    this->close();
}
/**
 * @brief cCvmCapabilities::on_pushButton_2_clicked
 * close this window
 */

void cCvmCapabilities::on_pushButton_2_clicked()
{
    this->close();
}

void cCvmCapabilities::on_lineEdit_textChanged(const QString &arg1)
{

}

void cCvmCapabilities::on_lineEdit_textEdited(const QString &arg1)
{
    qint32  lengthString;
    qint32  cursorPos;
    qint32  numTemp;
    QString stringTemp;
    bool    insert = false;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 2)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }

    if (true == insert)
    {
        ui->lineEdit->setText(stringTemp);
        ui->lineEdit->setCursorPosition(cursorPos-1);
        insert = false;
    }

    numTemp = stringTemp.toInt(NULL, 16);
    for (qint32 i =0; i < m_listCheckBoxCVMCap.length(); i++)
    {
        m_listCheckBoxCVMCap.at(i)->setChecked(numTemp &(1 << i));
    }
}
