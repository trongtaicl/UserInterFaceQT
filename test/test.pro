#-------------------------------------------------
#
# Project created by QtCreator 2017-10-04T14:49:37
#
#-------------------------------------------------

QT       += core gui
QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    cGetEntryPointAce.cpp \
    cAdditionalTerCapabilities.cpp \
    cCardDataInputCapabilities.cpp \
    cCvmCapabilities.cpp \
    cTerminalActionCode.cpp \
    cKernelConfig.cpp \
    cPaypassEntryPoint.cpp \
    cPaywaveEntryPoint.cpp \
    cSecurityCapabilities.cpp \
    cSplitTag.cpp \
    cTerminalCapabilities.cpp \
    cCreateStaticVariable.cpp \
    cTerminalTransactionQualifier.cpp \
    cAceExit.cpp \
    cAdministrative.cpp \
    cHelpInfo.cpp \
    cTestOtherTlv.cpp \
    cSha1.cpp

HEADERS += \
        mainwindow.h \
    cGetEntryPointAce.h \
    cAdditionalTerCapabilities.h \
    cCardDataInputCapabilities.h \
    cCvmCapabilities.h \
    cTerminalActionCode.h \
    cKernelConfig.h \
    cPaypassEntryPoint.h \
    cPaywaveEntryPoint.h \
    cSecurityCapabilities.h \
    cSplitTag.h \
    cTerminalCapabilities.h \
    cCreateStaticVariable.h \
    cTerminalTransactionQualifier.h \
    cAceExit.h \
    cAdministrative.h \
    cHelpInfo.h \
    cTestOtherTlv.h \
    cSha1.h

FORMS += \
        mainwindow.ui \
    cGetEntryPointAce.ui \
    cAdditionalTerCapabilities.ui \
    cCardDataInputCapabilities.ui \
    cCvmCapabilities.ui \
    cTerminalActionCode.ui \
    cKernelConfig.ui \
    cPaypassEntryPoint.ui \
    cPaywaveEntryPoint.ui \
    cSecurityCapabilities.ui \
    cTerminalCapabilities.ui \
    cTerminalTransactionQualifier.ui \
    cAceExit.ui \
    cAdministrative.ui \
    cHelpInfo.ui

RESOURCES += \
    image.qrc \
    image_open_save.qrc \
