#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtWidgets>
#include <QWidget>
#include <QDialog>
#include <QToolButton>
#include <QButtonGroup>
#include <QList>
#include <QResizeEvent>
#include <QRegExp>
#include <QRegExpValidator>
#include <QValidator>
#include "cTerminalCapabilities.h"
#include "cAdditionalTerCapabilities.h"
#include "cTerminalActionCode.h"
#include <QString>
#include <QComboBox>
#include "cPaypassEntryPoint.h"
#include <QSignalMapper>
#include <QList>
#include <QComboBox>
#include "cPaywaveEntryPoint.h"
#include <cCreateStaticVariable.h>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QAction>
#include <cAceExit.h>
#include <cAdministrative.h>
#include <cHelpInfo.h>
#include <cSplitTag.h>
#include <QByteArray>

namespace Ui {
class MainWindow;
class CustomTabStyle;
}

#define BUFFER_SIZE  32768

typedef enum{
    mproTACDef      = 0x00,
    mproTACOnl      = 0x01,
    mproTACDen      = 0x02,
    mterTACDef      = 0x03,
    mterTACOnl      = 0x04,
    mterTACDen      = 0x05
}mlsMainTACSelect_t;

class MainWindow : public QMainWindow, cTestOtherTLV
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void InitializeTerminalConfiguration();
    QString TextTerminal;
    void InitializeMainWindow();
    void InitializeProcessingConfiguration();
    void InitializeEntryPoint();
    void InitializeDataStorage();
    void InitializeSetup();
    void InitializeDRLVisa();

    void updateDir(QString dir);

    uint16_t CheckTag(const QString &text, uint8_t *Buffertam);

    void UpdateIndexTable();

    bool CheckTagAvai(quint8 *Buffertam, quint16 LengTotal, QString &error);

    bool updateCheckBoxLineSpin(quint8 index, quint8 numByte, quint8 *Buffertam, quint8 Length, QString &error);



protected:
   void resizeEvent(QResizeEvent *event);
signals:
   void ButtonTerminal_Clicked(const QString &text);
   void ButtonAddition_Clicked(const QString &text, mlsWinAdd_t winSelected);
   void ButtonDefault_Clicked(const QString &text, mlsWindowDef_t winSelected);
   void ButtonOnline_Clicked(const QString &text, mlsWindowDef_t winSelected);
   void Button_7_Clicked(const QString &text, mlsWindowDef_t winSelected);
   void UpdatePaypass(qint32 indexWidget);
   void UpdatePaywave();
   void adminShow(const QString &text);
   void terminalCapShow(const QString &text);

public slots:
    void GroupButtonClick(int Index);
    void Additional_Update(const QString &text);
    void Terminal_Update(const QString &text);
    void Default_Update(const QString &text);

    void OpenNewWindow(int Index);

    void updateLineEditEntryPointConfig(qint32 index, QString text);

    void listWidgetItemChanged();

    void aceExitClicked();

    void closeAdmin(const QString &text);

private slots:
    void on_toolButtonAddition_clicked();

    void on_toolButtonDefault_clicked();

    void on_toolButtonOnline_clicked();

    void on_toolButton_7_clicked();

    void on_checkBoxDefault_clicked();

    void on_toolButtonTerminal_clicked();

    void on_toolButtonAddProcessing_clicked();

    void on_toolButtonDenialProcessing_clicked();

    void on_toolButtonOnlineProcessing_clicked();

    void on_toolButtonDefaultProcessing_2_clicked();

    void on_toolButtonEntryUp_clicked();

    void on_toolButtonEntryDown_clicked();

    void on_toolButtonEntryAdd_clicked();

    void on_toolButtonEntryRemove_clicked();


    void on_ButtonEntrySave_clicked();

    void on_ButtonOpen_clicked();

    void on_pushButton_clicked();

    void on_ButtonOpenProcessing_clicked();

    void on_ButtonEntryOpen_clicked();

    void on_ButtonSaveAs_clicked();

    void on_ButtonSaveAsProcessing_clicked();

    void on_ButtonEntrySaveAs_clicked();

    void on_checkBoxAutorun_clicked();

    void on_toolButtonRemoveProcessing_clicked();

    void on_toolButtonUpProcessing_clicked();

    void on_toolButtonDownProcessing_clicked();

    void on_checkBox_clicked();

    void on_toolButtonDefaultProcessing_clicked();

    void on_listWidgetProcessing_itemChanged(QListWidgetItem *item);

    void on_listWidgetProcessing_itemClicked(QListWidgetItem *item);

    void on_toolButtonLoadDEK_clicked();

    void on_toolButtonClearDEK_clicked();

    void on_ButtonSave_clicked();

    void on_checkBox_TTQ_clicked();

    void on_checkBox_Referral_clicked();

    void on_toolButton_DRLAdd_clicked();

    void on_toolButton_DRLRemove_clicked();

    void on_toolButton_DRLUp_clicked();

    void on_toolButton_DRLDown_clicked();

    void on_toolButton_DRLOpen_clicked();

    void on_toolButton_DRLSaveAs_clicked();

    void on_ButtonSaveProcessing_clicked();

    void on_toolButton_DRLSave_clicked();

    void on_actionExit_Ctrl_Q_triggered();

    void on_actionExport_triggered();

    void on_actionSend_CA_Keys_triggered();

    void on_actionSend_Revocated_CA_Keys_triggered();

    void on_actionAdministrative_triggered();

    void on_actionAbout_triggered();

    void on_actionLoad_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionLoad_2_triggered();

    void on_actionSave_2_triggered();

    void on_actionSave_As_2_triggered();

    void on_actionLoad_3_triggered();

    void on_actionSave_3_triggered();

    void on_actionSave_As_3_triggered();

    void on_checkBoxAutorun_stateChanged(int arg1);

    void on_checkBoxDefault_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    QToolButton *ButtonWelcome;
    QToolButton *ButtonConfiguration;
    QToolButton *ButtonTransaction;
    QToolButton *ButtonBitmap;
    QToolButton *ButtonTool;
    QToolButton *ButtonTerminal;
    QToolButton *ButtonSetup;
    QAction     *actionClose;

    QButtonGroup *group;

    QList<QAbstractButton*> *ListButton;

    cTerminalCapabilities             *TermCap;
    cAdditionalTerCapabilities        *AddTerm;
    cTerminalActionCode               *Def;
    cPaypassEntryPoint                *PayPass;
    cPaywaveEntryPoint                *Paywave;
    cCreateStaticVariable             stringDir;
    cAceExit                          *aceExit;
    cAdministrative                   *admin;
    cHelpInfo                         *helpShow;
    cSplitTag                         m_TLV;

    QSignalMapper                     *sigMapp;
    QList<QToolButton*>               ListToolButton;
    QList<QLineEdit*>                 ListLineEdit;
    QList<QComboBox*>                 listComboKernelID;
    QList<QComboBox*>                 listComboAID;
    QList<QComboBox*>                 listComboTranType;

    QStringList                       m_tagConfiguration;

    int Count =0;
    int numRowTable =0;

    QString m_dirDataStorage;
    QString m_textAdmin;

    static QString m_dir1 ;

    mlsMainTACSelect_t LineEditChosed = mproTACDef; // point to which line edit is chosed between Default, Online.


};


#endif // MAINWINDOW_H
