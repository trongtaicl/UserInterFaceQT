#include "cTerminalActionCode.h"
#include "ui_cTerminalActionCode.h"
#include <QDebug>

cTerminalActionCode::cTerminalActionCode(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cTerminalActionCode)
{
    ui->setupUi(this);

    m_sigMappMSB = new QSignalMapper(this);
    m_sigMappLSB = new QSignalMapper(this);

    InitializeCheckbox();

    ui->pushButtonOK->setStyleSheet("QPushButton{background-color: black; color: white }");
    ui->pushButtonCancel->setStyleSheet("QPushButton{background-color: black; color:white;}");

    ui->lineEdit->setMaxLength(10);
    ui->lineEdit->setInputMask("hhhhhhhhhh");

    /* connect every clicked checkbox signal with slot: map() of m_sigMappMSB or m_sigMappLSB
     *
     * every time, a checkbox is clicked, it will call slot: SetTextMSB() or SetTextLSB()
     */
    for (int i =0; i< 16; i++){
        /* assign every element in checkboxMSB[] to m_listCheckBoxMSB */
        m_listCheckBoxMSB << m_checkBoxMSB[i];

        /* connect every statechanged signal of checkbox to slot map() of m_sigMappMSB */
        m_sigMappMSB->setMapping(m_listCheckBoxMSB[i], i);

        connect(m_listCheckBoxMSB[i], SIGNAL(stateChanged(int)), m_sigMappMSB, SLOT(map()));
    }
    for (int i =0; i< 24; i++){
         /* assign every element in checkboxLSB[] to m_listCheckBoxLSB */
        m_listCheckBoxLSB << m_checkBoxLSB[i];

        /* connect every statechanged signal of checkbox to slot map() of m_sigMappLSB */
        m_sigMappLSB->setMapping(m_listCheckBoxLSB[i], i);

        connect(m_listCheckBoxLSB[i], SIGNAL(clicked()), m_sigMappLSB, SLOT(map()));
    }
    connect(m_sigMappMSB, SIGNAL(mapped(int)), this, SLOT(set_text_MSB(int)));
    connect(m_sigMappLSB, SIGNAL(mapped(int)), this, SLOT(set_text_LSB(int)));
}

cTerminalActionCode::~cTerminalActionCode()
{
    delete ui;
}

void cTerminalActionCode::updateCheckBoxBasedOnLineEdit(const QString &text, QList<QCheckBox *> *list)
{
    qint64 numConverted ;

    numConverted = text.toInt(NULL, 16);
    for (qint32 i =0; i < list->length(); i++)
    {
        list->at(i)->setChecked(numConverted &(1<<i));
    }
}

/**
 * @brief cTerminalActionCode::def_change_text_edit
 * set text of dfTextLineEdit into text of lineedit and check the checkboxes in this window
 * @return: none
 */
void cTerminalActionCode::def_change_text_edit(const QString &text, mlsWindowDef_t winSelected)
{
  m_dfTextLineEdit = text;
  m_wdWindow       = winSelected;

  ui->lineEdit->setText(m_dfTextLineEdit);

  int num = ConvertStringtoNum(m_dfTextLineEdit, 0, 4);

  for (int i =0; i<16; i++){
      m_checkBoxMSB[i]->setChecked((num & (1<<i)));
  }
   /* assign number converted from dfTextLineEdit*/
  num = ConvertStringtoNum(m_dfTextLineEdit, 4, 6);

  for (int i =0; i<24; i++){
        m_checkBoxLSB[i]->setChecked((num & (1 << i)));
  }
}

/**
 * @brief cTerminalActionCode::set_text_MSB
 * this slot is connected with mapped(int) signal of sigmapp1
 * according to the value of LineEdit, checkbox will be checked or unchecked
 * it update value of two MSB (bytes) in LineEdit
 * @param index, checkbox in index position is clicked.
 * @return: none
 */
void cTerminalActionCode::set_text_MSB(int index)
{
  (void)&index;
  bool stateCheckbox = false;
  for (int i =0; i< 16; i++){
       /* check state of all the checkbox in m_listCheckBoxMSB */
      stateCheckbox = m_listCheckBoxMSB[i]->checkState();

      if (stateCheckbox == true){
      m_headNum |= (1<<i);
      }

      else{  /*if checkbox is checked, bit in i position of m_headNum will be 1 */
          m_headNum &= ~(1<<i);
      }
  }
    /* update text of LineEdit according to m_headNum and m_tailNum */
  ui->lineEdit->setText(QString("%1%2").arg(m_headNum, 4, 16, QChar('0')).arg(m_tailNum, 6, 16, QChar('0')).toUpper());
}

/**
 * @brief cTerminalActionCode::set_text_LSB
 * this slot is connected with mapped(int) signal of sigmapp1
 * according to the value of LineEdit, checkbox will be checked or unchecked
 * it update value of three LSB (bytes) in LineEdit
 * @param index
 */
void cTerminalActionCode::set_text_LSB(int index)
{
  (void)&index;
  bool stateCheckbox = false;

  for (int i =0; i< 24; i++){
      stateCheckbox = m_listCheckBoxLSB[i]->checkState();

      if (stateCheckbox == true){  /*if checkbox is checked, bit in i position of m_tailNum will be 1 */
      m_tailNum |= (1<<i);
      }

      else{   /*if checkbox is unchecked, bit in i position of m_tailNum will be 0 */
          m_tailNum &= ~(1<<i);
      }
  }
   /* update text of LineEdit according to m_headNum and m_tailNum */
  ui->lineEdit->setText(QString("%1%2").arg(m_headNum, 4, 16, QChar('0')).arg(m_tailNum, 6, 16, QChar('0')).toUpper());
}

/**
 * @brief cTerminalActionCode::on_pushButtonOK_clicked
 * when Button_Ok is clicked, check if the window to send a signal is mainwindow or paypassentrypoint
 * update value of TextLineEdit
 */
void cTerminalActionCode::on_pushButtonOK_clicked()
{
    m_dfTextLineEdit = ui->lineEdit->text();

    if (m_wdWindow == wdMainWindow){ /* if the window sent is mainwindow, emit a signal */
    emit ButtonOk_Clicked(ui->lineEdit->text());
    }

    else {  /* if the window sent is paypass, emit a signal */
        emit ButtonOK_PayPass_Clicked(ui->lineEdit->text());
    }

    this->close();
}
/**
 * @brief cTerminalActionCode::on_pushButtonCancel_clicked
 * close this window without saving anything
 * @return : none
 */
void cTerminalActionCode::on_pushButtonCancel_clicked()
{
    this->close();
}

/**
 * @brief cTerminalActionCode::ConvertStringtoNum, convert string to number
 * @param text
 * @param pos
 * @param num
 * @return the integer number,  the number after converting from text
 */
int cTerminalActionCode::ConvertStringtoNum(const QString &text, int pos, int num)
{
    bool status = false;
    QString textTemp;
    int numTemp;

    textTemp = text.mid(pos, num);
    numTemp = textTemp.toUInt(&status, 16);
    return numTemp;
}
/**
 * @brief cTerminalActionCode::InitializeCheckbox
 * initialize the checkbox,
 * add all the checkbox into two arrays  checkboxMSB and checkboxLSB
 */
void cTerminalActionCode::InitializeCheckbox()
{
    m_checkBoxMSB[15] = ui->checkBoxCDANotPerform;
    m_checkBoxMSB[14] = ui->checkBoxSDAFailed;
    m_checkBoxMSB[13] = ui->checkBoxICCData;
    m_checkBoxMSB[12] = ui->checkBoxCardOnTerminal;
    m_checkBoxMSB[11] = ui->checkBoxDDAFailed;
    m_checkBoxMSB[10] = ui->checkBoxCDAFailed;
    m_checkBoxMSB[9]  = ui->checkBoxSDASelected;
    m_checkBoxMSB[8]  = ui->checkBoxRFU1;
    m_checkBoxMSB[7]  = ui->checkBoxICCAndTerminal;
    m_checkBoxMSB[6]  = ui->checkBoxExpired;
    m_checkBoxMSB[5]  = ui->checkBoxApplication;
    m_checkBoxMSB[4]  = ui->checkBoxRequested;
    m_checkBoxMSB[3]  = ui->checkBoxNewCard;
    m_checkBoxMSB[2]  = ui->checkBoxRFU2;
    m_checkBoxMSB[1] = ui->checkBoxRFU3;
    m_checkBoxMSB[0]  = ui->checkBoxRFU4;

    m_checkBoxLSB[23] = ui->checkBoxCVMNotSuccess;
    m_checkBoxLSB[22] = ui->checkBoxUnrecognized;
    m_checkBoxLSB[21] = ui->checkBoxPinTryLimit;
    m_checkBoxLSB[20] = ui->checkBoxPinEntryReq1;
    m_checkBoxLSB[19] = ui->checkBoxPINEntryReq2;
    m_checkBoxLSB[18] = ui->checkBoxOnlinePIN;
    m_checkBoxLSB[17] = ui->checkBoxRFU5;
    m_checkBoxLSB[16] = ui->checkBoxRFU6;
    m_checkBoxLSB[15] = ui->checkBoxTransactionExceed;
    m_checkBoxLSB[14] = ui->checkBoxLCOLExceed;
    m_checkBoxLSB[13] = ui->checkBoxUCOLExeed;
    m_checkBoxLSB[12] = ui->checkBoxTransactionSelected;
    m_checkBoxLSB[11] = ui->checkBoxMerchantForce;
    m_checkBoxLSB[10] = ui->checkBoxRFU7;
    m_checkBoxLSB[9]  = ui->checkBoxRFU8;
    m_checkBoxLSB[8]  = ui->checkBoxRFU9;
    m_checkBoxLSB[7]  = ui->checkBoxDefaultTDOL;
    m_checkBoxLSB[6]  = ui->checkBoxIssuer;
    m_checkBoxLSB[5]  = ui->checkBoxScriptBeforeAC;
    m_checkBoxLSB[4]  = ui->checkBoxScriptAfterAC;
    m_checkBoxLSB[3]  = ui->checkBoxRelayThreshold;
    m_checkBoxLSB[2]  = ui->checkBoxRelayTime;
    m_checkBoxLSB[1]  = ui->checkBoxRRP1;
    m_checkBoxLSB[0]  = ui->checkBoxRRP2;
}



void cTerminalActionCode::on_lineEdit_textEdited(const QString &arg1)
{
    qint64  lengthString;
    qint64  cursorPos;
    QString stringTemp;
    QString stringSplitedMSB, stringSplitedLSB;
    bool    insert = false;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 10)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }

    if (true == insert)
    {
        ui->lineEdit->setText(stringTemp);
        ui->lineEdit->setCursorPosition(cursorPos -1);
        insert = false;

    }

    stringSplitedMSB = stringTemp.mid(0, 4);
    stringSplitedLSB = stringTemp.mid(4,6);

    updateCheckBoxBasedOnLineEdit(stringSplitedMSB, &m_listCheckBoxMSB);
    updateCheckBoxBasedOnLineEdit(stringSplitedLSB, &m_listCheckBoxLSB);
}
