#include "cTerminalCapabilities.h"
#include "ui_cTerminalCapabilities.h"
#include <QDebug>
#include <QCheckBox>


cTerminalCapabilities::cTerminalCapabilities(QWidget *parent ) :
    QDialog(parent ),
    ui(new Ui::cTerminalCapabilities)
{
    ui->setupUi(this);

    m_headText = "00";
    m_middleText = "00";
    m_tailText = "00";
    this->setWindowTitle("Terminal Capabilities (EMV tag 9F33)");
    InitilizeCheckBox();
    ui->lineEdit->setMaxLength(6);
    ui->lineEdit->setInputMask("hhhhhh");
    if (m_textLineEdit == "")
    {
        ui->lineEdit->setText("000000");
    }

    else ui->lineEdit->setText(m_textLineEdit );
    ui->pushButtonOkConfig->setStyleSheet("QPushButton{background-color: black; color: white; }");
    ui->pushButtonCancelConfig->setStyleSheet("QPushButton{background-color: black; color:white;}");

}

cTerminalCapabilities::~cTerminalCapabilities()
{
    delete ui;
}



void cTerminalCapabilities::ChangeTextEdit(const QString &text)
{

    m_textLineEdit = text;
    ui->lineEdit->setText(m_textLineEdit);
    unsigned int Num = SetCheckbox(m_textLineEdit, 0, 2);
    for (int i =0; i< 8; i++){
        if ((Num & (1<<i)) != 0)
            m_checkBoxCol1st.at(i)->setChecked((Num & (1 << i)));
    }
    Num = SetCheckbox(m_textLineEdit, 2, 2);
    for (int i =0; i < 8; i++){
            m_checkBoxCol2nd.at(i)->setChecked((Num & (1<<i)));
    }
    Num = SetCheckbox(m_textLineEdit, 4, 2);
    for (int i =0; i <8; i++){

            m_checkBoxCol3rd.at(i)->setChecked((Num &(1 << i)));
    }
}


void cTerminalCapabilities::on_pushButtonOkConfig_clicked()
{
    m_textLineEdit = ui->lineEdit->text();
    emit ButtonOk_Clicked(m_textLineEdit);
    this->close();
}

void cTerminalCapabilities::on_pushButtonCancelConfig_clicked()
{
    this->close();
}

void cTerminalCapabilities::on_checkBoxManual_clicked()
{
    m_headNum = m_headNum^(1<<7);
    if (m_headNum >0x0F)
    m_headText = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxMagnetic_clicked()
{
    m_headNum ^= (1<<6);
    if (m_headNum >0x0F)
    m_headText  = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxICC_clicked()
{
    m_headNum ^= (1<<5);
    if (m_headNum >0x0F)
    m_headText = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU1_clicked()
{
    m_headNum^= (1<<4);
    if (m_headNum > 0x0F)
    m_headText = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU2_clicked()
{
    m_headNum^= (1<<3);
    if (m_headNum > 0x0F)
    m_headText = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU3_clicked()
{
    m_headNum^= (1<<2);
    if (m_headNum > 0x0F)
    m_headText = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU4_clicked()
{
    m_headNum^= (1<<1);
    if(m_headNum > 0x0F)
    m_headText = QString::number(m_headNum, 16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU5_clicked()
{
    m_headNum^= 1;
    if (m_headNum > 0x0F)
    m_headText = QString::number(m_headNum,16);
    else m_headText = "0" + QString::number(m_headNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+ m_middleText.toUpper()+ m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxPlaintext_clicked()
{
    m_middleNum^= (1<<7);
    if (m_middleNum > 0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxEncOnline_clicked()
{
    m_middleNum^= (1<<6);
    if (m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}


void cTerminalCapabilities::on_checkBoxSingature_clicked()
{
    m_middleNum^= (1<<5);
    if (m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxEncOffline_clicked()
{
    m_middleNum^= (1<<4);
    if(m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxNoCVM_clicked()
{
    m_middleNum^= (1<<3);
    if(m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU6_clicked()
{
    m_middleNum^= (1<<2);

    if(m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU7_clicked()
{
    m_middleNum^= (1<<1);

    if(m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU8_clicked()
{
    m_middleNum^= 1;
    if(m_middleNum >0x0F)
    m_middleText  = QString::number(m_middleNum, 16);
    else m_middleText = "0" + QString::number(m_middleNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}


void cTerminalCapabilities::on_checkBoxSDA_clicked()
{
    m_tailNum^= (1<<7);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxDDA_clicked()
{
    m_tailNum^= (1<<6);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxCard_clicked()
{
    m_tailNum^= (1<<5);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU9_clicked()
{
    m_tailNum^= (1<<4);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxCDA_clicked()
{
    m_tailNum^= (1<<3);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU10_clicked()
{
    m_tailNum^= (1<<2);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU11_clicked()
{
    m_tailNum^= (1<<1);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

void cTerminalCapabilities::on_checkBoxRFU12_clicked()
{
    m_tailNum^= (1);
    if (m_tailNum > 0x0F)
    m_tailText = QString::number(m_tailNum, 16);
    else m_tailText = "0" + QString::number(m_tailNum, 16);

    ui->lineEdit->setText(m_headText.toUpper()+m_middleText.toUpper()+m_tailText.toUpper());
}

int cTerminalCapabilities::SetCheckbox(const QString &text, int Pos, int Num)
{
    bool status = false;
    QString TextTemp = text.mid(Pos, Num);
    int NumTemp = TextTemp.toUInt(&status, 16);
    return NumTemp;
}

void cTerminalCapabilities::updateCheckBoxBasedOnLineEdit(const QString &text, QList<QCheckBox *> *list)
{
    qint32 numConverted ;

    numConverted = text.toInt(NULL, 16);
    for (qint32 i =0; i < list->length(); i++)
    {
        list->at(i)->setChecked(numConverted &(1<<i));
    }
}


void cTerminalCapabilities::InitilizeCheckBox()
{
    m_checkBoxCol1st << ui->checkBoxRFU5 << ui->checkBoxRFU4 << ui->checkBoxRFU3
                     << ui->checkBoxRFU2 << ui->checkBoxRFU1 << ui->checkBoxICC
                     <<ui->checkBoxMagnetic << ui->checkBoxManual;

    m_checkBoxCol2nd << ui->checkBoxRFU8 << ui->checkBoxRFU7 << ui->checkBoxRFU6
                      << ui->checkBoxNoCVM << ui->checkBoxEncOffline << ui->checkBoxSingature
                      << ui->checkBoxEncOnline << ui->checkBoxPlaintext;

    m_checkBoxCol3rd << ui->checkBoxRFU12 << ui->checkBoxRFU11 << ui->checkBoxRFU10
                    << ui->checkBoxCDA << ui->checkBoxRFU9 << ui->checkBoxCard
                    << ui->checkBoxDDA << ui->checkBoxSDA;

}

void cTerminalCapabilities::on_lineEdit_textEdited(const QString &arg1)
{
    qint32  lengthString;
    qint32  cursorPos;
    QString stringTemp;
    QStringList stringSplited;
    bool    insert = false;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 6)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }
    ui->lineEdit->setText(stringTemp);

    if (true == insert)
    {
        ui->lineEdit->setCursorPosition(cursorPos -1);
        insert = false;
    }
    else
    {
        ui->lineEdit->setCursorPosition(cursorPos);
    }

    for (qint32 i =0; i < stringTemp.length(); i+=2)
    {
       stringSplited << stringTemp.mid(i, 2);
    }

    updateCheckBoxBasedOnLineEdit(stringSplited.at(0), &m_checkBoxCol1st);
    updateCheckBoxBasedOnLineEdit(stringSplited.at(1), &m_checkBoxCol2nd);
    updateCheckBoxBasedOnLineEdit(stringSplited.at(2), &m_checkBoxCol3rd);

}
