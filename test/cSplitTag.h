#ifndef cSplitTag_H
#define cSplitTag_H

#include <QDialog>
#include <QCheckBox>
#include <QList>
#include <QSpinBox>
#include <QComboBox>
#include <QString>
#include <QLineEdit>
#include <QTextEdit>
#include "stdint.h"

typedef enum{
    splitBoolLineEdit,
    splitBoolSpinBox,
    splitBoolComboBox
}mlsBoolWidgetSplitTag_t;

typedef enum{
    ctTerType     = 0x00,
    ctKerID       = 0x01,
    ctStat        = 0x02,
    ctMerType     = 0x03
}mls_comboType_t;

class cSplitTag : public QDialog
{
public:
    cSplitTag();


    QString text;


    void initializeList(QList<QCheckBox*> listCheckboxLine, QList<QLineEdit*> listLine,
                        QList<QCheckBox*> listCheckBoxCombo, QList<QCheckBox*> listCheckboxSpin,
                        QList<QComboBox*> listComboBox, QList<QSpinBox*> listSpinBox,
                        QStringList stringComboTerType, QStringList stringComboKerID, QStringList stringComboState);

    void updateComboType(mls_comboType_t comboType);

    void UncheckTag();

    void Updatetext(int Index, mlsBoolWidgetSplitTag_t Widget, uint8_t *Buffertam, uint8_t Pos, uint8_t Length);

    void UncheckCheckbox(int Index, mlsBoolWidgetSplitTag_t Widget);

    void UpdateOtherTLV(uint8_t *Buffertam, uint8_t Pos, uint8_t Sobyte, uint8_t Length);


private:  

    bool m_stateCheckboxLineEdit[25] ;
    bool m_stateCheckboxCombo[5]     ;
    bool m_stateCheckboxSpin[5]      ;

    QList<QCheckBox*>            m_listCheckBoxLineEdit;
    QList<QLineEdit*>            m_listLineEdit;

    QList<QCheckBox*>            m_listCheckBoxCombo;
    QList<QCheckBox*>            m_listCheckBoxSpin;

    QList<QComboBox*>            m_listComboBox;

    QList<QSpinBox*>             m_listSpinBox;

    QStringList                  m_stringComboTerType;
    QStringList                  m_stringComboKerID;
    QStringList                  m_stringComboState;

    mls_comboType_t              m_comboType;


};

#endif // cSplitTag_H
