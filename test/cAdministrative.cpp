#include "cAdministrative.h"
#include "ui_cAdministrative.h"
#include <QDebug>
#include <QStringList>

cAdministrative::cAdministrative(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cAdministrative)
{
    ui->setupUi(this);

    this->setWindowTitle("Administrative");

    m_listCheckBoxMSB << ui->checkBox << ui->checkBox_1 << ui->checkBox_2
                   << ui->checkBox_3 ;
    m_listCheckBoxMiddle1 << ui->checkBox_4 << ui->checkBox_5
                   << ui->checkBox_6;
    m_listCheckBoxMiddle2 << ui->checkBox_7 << ui->checkBox_8
                   << ui->checkBox_9 << ui->checkBox_10 ;
    m_listCheckBoxMiddle3 << ui->checkBox_11
                   << ui->checkBox_12 << ui->checkBox_13 << ui->checkBox_14
                   << ui->checkBox_15 ;
    m_listCheckBoxMiddle4 << ui->checkBox_16 << ui->checkBox_17
                   << ui->checkBox_18 << ui->checkBox_19 << ui->checkBox_20;
    m_listCheckBoxLSB << ui->checkBox_21 << ui->checkBox_22 << ui->checkBox_23
                   << ui->checkBox_24 << ui->checkBox_25;

    QString stringTemp;
    for (qint32 i =0; i < 60; i++)
    {
        stringTemp += "h";
    }
    ui->lineEdit->setInputMask(stringTemp);
    ui->lineEdit->setText("000000000000000000000000000000"
                          "000000000000000000000000000000");

    m_stringLSB = "0000000000";
    m_stringMiddle1 = "0000000000";
    m_stringMiddle2 = "0000000000";
    m_stringMiddle3 = "0000000000";
    m_stringMiddle4 = "0000000000";
    m_stringMSB     = "0000000000";

    m_sigMappMSB = new QSignalMapper(this);
    m_sigMappMiddle1 = new QSignalMapper(this);
    m_sigMappMiddle2 = new QSignalMapper(this);
    m_sigMappMiddle3 = new QSignalMapper(this);
    m_sigMappMiddle4 = new QSignalMapper(this);
    m_sigMappLSB    = new QSignalMapper(this);

    for (qint32 i =0; i < m_listCheckBoxMSB.length(); i++)
    {
      m_sigMappMSB->setMapping(m_listCheckBoxMSB[i],i);
      connect(m_listCheckBoxMSB[i], SIGNAL(clicked()), m_sigMappMSB, SLOT(map()));
    }

    connect(m_sigMappMSB, SIGNAL(mapped(qint32)), this, SLOT(checkBoxMSBClicked(qint32)));

    for (qint32 i =0; i < m_listCheckBoxMiddle1.length(); i++)
    {
      m_sigMappMiddle1->setMapping(m_listCheckBoxMiddle1[i],i);
      connect(m_listCheckBoxMiddle1[i], SIGNAL(clicked()), m_sigMappMiddle1, SLOT(map()));
    }

    connect(m_sigMappMiddle1, SIGNAL(mapped(qint32)), this, SLOT(checkBoxMiddle1Clicked(qint32)));

    for (qint32 i =0; i < m_listCheckBoxMiddle2.length(); i++)
    {
      m_sigMappMiddle2->setMapping(m_listCheckBoxMiddle2[i],i);
      connect(m_listCheckBoxMiddle2[i], SIGNAL(clicked()), m_sigMappMiddle2, SLOT(map()));
    }

    connect(m_sigMappMiddle2, SIGNAL(mapped(qint32)), this, SLOT(checkBoxMiddle2Clicked(qint32)));

    for (qint32 i =0; i < m_listCheckBoxMiddle3.length(); i++)
    {
      m_sigMappMiddle3->setMapping(m_listCheckBoxMiddle3[i],i);
      connect(m_listCheckBoxMiddle3[i], SIGNAL(clicked()), m_sigMappMiddle3, SLOT(map()));
    }

    connect(m_sigMappMiddle3, SIGNAL(mapped(qint32)), this, SLOT(checkBoxMiddle3Clicked(qint32)));

    for (qint32 i =0; i < m_listCheckBoxMiddle4.length(); i++)
    {
      m_sigMappMiddle4->setMapping(m_listCheckBoxMiddle4[i],i);
      connect(m_listCheckBoxMiddle4[i], SIGNAL(clicked()), m_sigMappMiddle4, SLOT(map()));
    }

    connect(m_sigMappMiddle4, SIGNAL(mapped(qint32)), this, SLOT(checkBoxMiddle4Clicked(qint32)));

    for (qint32 i =0; i < m_listCheckBoxLSB.length(); i++)
    {
      m_sigMappLSB->setMapping(m_listCheckBoxLSB[i],i);
      connect(m_listCheckBoxLSB[i], SIGNAL(clicked()), m_sigMappLSB, SLOT(map()));
    }

    connect(m_sigMappLSB, SIGNAL(mapped(qint32)), this, SLOT(checkBoxLSBClicked(qint32)));
}

cAdministrative::~cAdministrative()
{
    delete ui;
}

void cAdministrative::checkAndUncheck(const QString text, QList<QCheckBox *> *listCheck,
                                      QLineEdit *line, qint32 ith)
{
    QString stringTemp;
    qint32  numConverted;

    for (qint32 i=0; i < text.length(); i+=2)
    {
        stringTemp = text.mid(i, 2);
        numConverted = stringTemp.toInt();

        if (line != NULL)
        {
            if (i < ith)
            {
                if (numConverted > 0)
                {
                    listCheck->at(i/2)->setChecked(true);
                }
                else
                {
                    listCheck->at(i/2)->setChecked(false);
                }
             }
            else if (i >ith)
            {
                if (numConverted > 0)
                {
                    listCheck->at(i/2 -1)->setChecked(true);
                }
                else
                {
                    listCheck->at(i/2-1)->setChecked(false);
                }

            }
            else
            {
                line->setText(stringTemp);
            }
        }
        else
        {
            if (numConverted >0)
            {
                listCheck->at(i/2)->setChecked(true);
            }
            else
            {
                listCheck->at(i/2)->setChecked(false);
            }
        }
    }

}

void cAdministrative::checkBoxMSBClicked(qint32 index)
{
    bool stateCheck = false;

    m_stringMSB = "";
    for (qint32 i =0; i < m_listCheckBoxMSB.length(); i++)
    {
        if (i ==3)
        {
            m_stringMSB += ui->lineEdit_DefTran->text();
        }

        stateCheck = m_listCheckBoxMSB[i]->checkState();

            if (stateCheck)
            {
                m_stringMSB += "01";
            }
        else
        {
            m_stringMSB += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::checkBoxMiddle1Clicked(qint32 index)
{
    bool stateCheck = false;

    m_stringMiddle1 ="";
    for (qint32 i =0; i < m_listCheckBoxMiddle1.length(); i++)
    {
        if (i ==0)
        {
            m_stringMiddle1 += ui->lineEdit_TraceDep->text();
        }

        if(i == 1)
        {
            m_stringMiddle1+= ui->lineEdit_DefLan->text();
        }

        stateCheck = m_listCheckBoxMiddle1[i]->checkState();

        if (stateCheck)
        {
           m_stringMiddle1 += "01";
        }
        else
        {
            m_stringMiddle1 += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::checkBoxMiddle2Clicked(qint32 index)
{
    bool stateCheck = false;

    m_stringMiddle2 = "";
    for (qint32 i =0; i < m_listCheckBoxMiddle2.length(); i++)
    {
        if (i ==0)
        {
            m_stringMiddle2 += ui->lineEdit_Polling->text();
        }

        stateCheck = m_listCheckBoxMiddle2[i]->checkState();

        if (stateCheck)
        {
           m_stringMiddle2 += "01";
        }
        else
        {
            m_stringMiddle2 += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::checkBoxMiddle3Clicked(qint32 index)
{
    bool stateCheck = false;

    m_stringMiddle3 ="";
    for (qint32 i =0; i < m_listCheckBoxMiddle3.length(); i++)
    {
        stateCheck = m_listCheckBoxMiddle3[i]->checkState();

        if (stateCheck)
        {
           m_stringMiddle3 += "01";
        }
        else
        {
            m_stringMiddle3 += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::checkBoxMiddle4Clicked(qint32 index)
{
    bool stateCheck = false;

    m_stringMiddle4 = "";
    for (qint32 i =0; i < m_listCheckBoxMiddle4.length(); i++)
    {
        stateCheck = m_listCheckBoxMiddle4[i]->checkState();

        if (stateCheck)
        {
           m_stringMiddle4 += "01";
        }
        else
        {
            m_stringMiddle4 += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::checkBoxLSBClicked(qint32 index)
{
    bool stateCheck = false;

    m_stringLSB = "";
    for (qint32 i =0; i < m_listCheckBoxLSB.length(); i++)
    {
        stateCheck = m_listCheckBoxLSB[i]->checkState();

        if (stateCheck)
        {
           m_stringLSB += "01";
        }
        else
        {
            m_stringLSB += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::updateTextLineEdit(const QString &string)
{   
    QStringList stringTemp;
    QString     stringSplited;
    qint32      numConverted;

    m_textLineEdit = string;
    ui->lineEdit->setText(m_textLineEdit);

    for (qint32 i =0; i < m_textLineEdit.length(); i+= 10)
    {
        stringTemp << m_textLineEdit.mid(i, 10);
    }

    m_stringMSB = stringTemp.at(0);
    m_stringMiddle1 = stringTemp.at(1);
    m_stringMiddle2 = stringTemp.at(2);
    m_stringMiddle3 = stringTemp.at(3);
    m_stringMiddle4 = stringTemp.at(4);
    m_stringLSB     = stringTemp.at(5);

    checkAndUncheck(stringTemp.at(0), &m_listCheckBoxMSB, ui->lineEdit_DefTran,2);
    checkAndUncheck(stringTemp.at(2), &m_listCheckBoxMiddle2, ui->lineEdit_Polling, 0);
    checkAndUncheck(stringTemp.at(3), &m_listCheckBoxMiddle3, NULL, 0);
    checkAndUncheck(stringTemp.at(4), &m_listCheckBoxMiddle4, NULL, 0);
    checkAndUncheck(stringTemp.at(5), &m_listCheckBoxLSB, NULL, 0);

    ui->lineEdit_TraceDep->setText(stringTemp.at(1).mid(0,2));
    ui->lineEdit_DefLan->setText(stringTemp.at(1).mid(2,2));
    for (qint32 i = 4; i < stringTemp.at(1).length(); i+= 2)
    {
        stringSplited = stringTemp.at(1).mid(i, 2);
        numConverted  = stringSplited.toInt();
        if (numConverted > 0)
        {
            m_listCheckBoxMiddle1.at(i/2-2)->setChecked(true);
        }
        else
        {
            m_listCheckBoxMiddle1.at(i/2-2)->setChecked(false);
        }
    }

}

void cAdministrative::on_lineEdit_DefTran_textChanged(const QString &arg1)
{
    bool stateCheck = false;

    m_stringMSB = "";
    for (qint32 i =0; i < m_listCheckBoxMSB.length(); i++)
    {
        if (i ==2)
        {
            m_stringMSB += ui->lineEdit_DefTran->text();
        }

        stateCheck = m_listCheckBoxMSB[i]->checkState();

        if (stateCheck)
        {
           m_stringMSB += "01";
        }
        else
        {
            m_stringMSB += "00";
        }
    }

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::on_lineEdit_TraceDep_textChanged(const QString &arg1)
{
    bool stateCheck = false;
    QString stringTemp;
    for (qint32 i =0; i < m_listCheckBoxMiddle1.length(); i++)
    {
        if (i ==0)
        {
            stringTemp += ui->lineEdit_TraceDep->text();
        }

        if(i == 1)
        {
            stringTemp += ui->lineEdit_DefLan->text();
        }

        stateCheck = m_listCheckBoxMiddle1[i]->checkState();

        if (stateCheck)
        {
           stringTemp += "01";
        }
        else
        {
            stringTemp += "00";
        }
    }

    m_stringMiddle1 = stringTemp;
    m_textLineEdit = m_stringMSB + m_stringMiddle1 + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::on_lineEdit_DefLan_textChanged(const QString &arg1)
{
    bool stateCheck = false;
    QString stringTemp;
    for (qint32 i =0; i < m_listCheckBoxMiddle1.length(); i++)
    {
        if (i ==0)
        {
            stringTemp += ui->lineEdit_TraceDep->text();
        }

        if(i == 1)
        {
            stringTemp += ui->lineEdit_DefLan->text();
        }

        stateCheck = m_listCheckBoxMiddle1[i]->checkState();

        if (stateCheck)
        {
           stringTemp += "01";
        }
        else
        {
            stringTemp += "00";
        }
    }

    m_stringMiddle1 = stringTemp;
    m_textLineEdit = m_stringMSB + stringTemp + m_stringMiddle2 +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::on_lineEdit_Polling_textChanged(const QString &arg1)
{
    bool stateCheck = false;
    QString stringTemp;

    for (qint32 i =0; i < m_listCheckBoxMiddle2.length(); i++)
    {
        if (i ==0)
        {
            stringTemp += ui->lineEdit_Polling->text();
        }

        stateCheck = m_listCheckBoxMiddle2[i]->checkState();

        if (stateCheck)
        {
           stringTemp += "01";
        }
        else
        {
            stringTemp += "00";
        }
    }

    m_stringMiddle2 = stringTemp;

    m_textLineEdit = m_stringMSB + m_stringMiddle1 + stringTemp +
            m_stringMiddle3 + m_stringMiddle4 + m_stringLSB;
    ui->lineEdit->setText(m_textLineEdit);

}

void cAdministrative::on_lineEdit_textChanged(const QString &arg1)
{

}

void cAdministrative::on_pushButton_clicked()
{
    emit closeButtonClicked(m_textLineEdit);
    this->close();
}

void cAdministrative::on_lineEdit_cursorPositionChanged(int arg1, int arg2)
{

}

void cAdministrative::on_lineEdit_textEdited(const QString &arg1)
{
    QStringList stringTemp;
    QString     stringSplited;
    qint32      numConverted;
    qint32      pos;
    bool        lineDeleted = false;

    pos = ui->lineEdit->cursorPosition();
    m_textLineEdit = arg1;

    while (m_textLineEdit.length() < 60)
    {
        m_textLineEdit.insert(pos, QString("0"));
        lineDeleted  = true;
        pos ++;
    }
    ui->lineEdit->setText(m_textLineEdit);

    if (lineDeleted == true)
    {
        ui->lineEdit->setCursorPosition(pos-1);
        lineDeleted = false;
    }
    else
    {
        ui->lineEdit->setCursorPosition(pos);
    }


    for (qint32 i =0; i < m_textLineEdit.length(); i+= 10)
    {
        stringTemp << m_textLineEdit.mid(i, 10);
    }

    m_stringMSB = stringTemp.at(0);
    m_stringMiddle1 = stringTemp.at(1);
    m_stringMiddle2 = stringTemp.at(2);
    m_stringMiddle3 = stringTemp.at(3);
    m_stringMiddle4 = stringTemp.at(4);
    m_stringLSB     = stringTemp.at(5);

    checkAndUncheck(stringTemp.at(0), &m_listCheckBoxMSB, ui->lineEdit_DefTran,2);
    checkAndUncheck(stringTemp.at(2), &m_listCheckBoxMiddle2, ui->lineEdit_Polling, 0);
    checkAndUncheck(stringTemp.at(3), &m_listCheckBoxMiddle3, NULL, 0);
    checkAndUncheck(stringTemp.at(4), &m_listCheckBoxMiddle4, NULL, 0);
    checkAndUncheck(stringTemp.at(5), &m_listCheckBoxLSB, NULL, 0);

    ui->lineEdit_TraceDep->setText(stringTemp.at(1).mid(0,2));
    ui->lineEdit_DefLan->setText(stringTemp.at(1).mid(2,2));
    for (qint32 i = 4; i < stringTemp.at(1).length(); i+= 2)
    {
        stringSplited = stringTemp.at(1).mid(i, 2);
        numConverted  = stringSplited.toInt();
        if (numConverted > 0)
        {
            m_listCheckBoxMiddle1.at(i/2-2)->setChecked(true);
        }
        else
        {
            m_listCheckBoxMiddle1.at(i/2-2)->setChecked(false);
        }
    }

}
