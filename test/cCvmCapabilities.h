#ifndef cCvmCapabilities_H
#define cCvmCapabilities_H

#include <QDialog>
#include <QSignalMapper>
#include <QString>
#include <QCheckBox>
#include <QList>

namespace Ui {
class cCvmCapabilities;
}

class cCvmCapabilities : public QDialog
{
    Q_OBJECT

signals:
    void ButtonOk_CVMCap_clicked(const QString &text);  /* ok button is clicked */


public:
    explicit cCvmCapabilities(QWidget *parent = 0);
    ~cCvmCapabilities();

public slots:
    void check_box_update(int index);

    void cvm_change_text_edit(const QString &text);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_textEdited(const QString &arg1);

private:
    Ui::cCvmCapabilities *ui;
   /* this variable is reprensentive for list checkbox presented in window */
    QList<QCheckBox*> m_listCheckBoxCVMCap;

    QSignalMapper *m_sigMapp;
    QString       m_textLineEdit;
};

#endif // cCvmCapabilities_H
