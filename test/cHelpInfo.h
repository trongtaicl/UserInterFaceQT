#ifndef CHELPINFO_H
#define CHELPINFO_H

#include <QDialog>

namespace Ui {
class cHelpInfo;
}

class cHelpInfo : public QDialog
{
    Q_OBJECT

public:
    explicit cHelpInfo(QWidget *parent = 0);
    ~cHelpInfo();

private:
    Ui::cHelpInfo *ui;
};

#endif // CHELPINFO_H
