#ifndef cTerminalActionCode_H
#define cTerminalActionCode_H

#include <QDialog>
#include <QString>
#include <QCheckBox>
#include <QSignalMapper>

namespace Ui {
class cTerminalActionCode;
}

typedef enum{
    wdMainWindow   = 0x00,
    wdPayPass      = 0x01
}mlsWindowDef_t;

class cTerminalActionCode : public QDialog
{
    Q_OBJECT
signals:
    void ButtonOk_Clicked(const QString &text);
    void ButtonOK_PayPass_Clicked(const QString &text);

public:
    explicit cTerminalActionCode(QWidget *parent = 0);
    ~cTerminalActionCode();


    void updateCheckBoxBasedOnLineEdit(const QString &text, QList<QCheckBox*> *list);

public slots:
    void def_change_text_edit(const QString &text, mlsWindowDef_t winSelected);

private slots:
    void on_pushButtonOK_clicked();

    void on_pushButtonCancel_clicked();

    void set_text_MSB(int index);

    void set_text_LSB(int index);

    void on_lineEdit_textEdited(const QString &arg1);

private:
    Ui::cTerminalActionCode *ui;

    QCheckBox* m_checkBoxMSB[16];/* an array of checkbox that represents for 2 MSB (byte)*/
    QCheckBox* m_checkBoxLSB[24]; /* an array of checkbox that represents for 3 LSB (byte)*/

    QList<QCheckBox*> m_listCheckBoxMSB; /* a list of checkbox that represents for 2 MSB (byte)*/
    QList<QCheckBox*> m_listCheckBoxLSB; /* a list of checkbox that represents for 3 LSB (byte)*/

    QSignalMapper *m_sigMappMSB;
    QSignalMapper *m_sigMappLSB;
    /* headNum is represented for 3 MSB (byte)*/
    /* tailNum is represented for 2 LSB (byte)*/
    unsigned int m_headNum =0, m_tailNum =0;

    QString m_dfTextLineEdit;
    mlsWindowDef_t m_wdWindow;

    void InitializeCheckbox();

    int ConvertStringtoNum(const QString &text, int pos, int num);
};

#endif // cTerminalActionCode_H
