#include "cSecurityCapabilities.h"
#include "ui_cSecurityCapabilities.h"

cSecurityCapabilities::cSecurityCapabilities(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cSecurityCapabilities)
{
    ui->setupUi(this);
    this->setWindowTitle("Security Capabilities");
    m_sigMapp = new QSignalMapper(this);
    m_listCheckBoxSecCap << ui->checkBox << ui->checkBox_2 << ui->checkBox_3 << ui->checkBox_4
                 << ui->checkBox_5 << ui->checkBox_6 << ui->checkBox_7 << ui->checkBox_8;

    ui->lineEdit->setInputMask("hh");
    ui->lineEdit->setText("00");
    ui->lineEdit->setAlignment(Qt::AlignRight);

    for (int i =0; i< 8; i++){
        m_sigMapp->setMapping(m_listCheckBoxSecCap[i], i);
        connect(m_listCheckBoxSecCap[i], SIGNAL(clicked()), m_sigMapp, SLOT(map()));
    }
    connect(m_sigMapp, SIGNAL(mapped(int)), this, SLOT(CheckboxUpdate(int)));
}

cSecurityCapabilities::~cSecurityCapabilities()
{
    delete ui;
}

void cSecurityCapabilities::CheckboxUpdate(int index)
{
    bool State =false;
    for (int i =0; i< 8; i++){
        State = m_listCheckBoxSecCap[i]->checkState();

        if (State){
            m_num |= (1<<i);
        }
        else {
            m_num &= ~(1<<i);
        }
    }
    ui->lineEdit->setText(QString("%1").arg(m_num, 2, 16, QChar('0')).toUpper());
}

void cSecurityCapabilities::scChangeTextEdit(const QString &text)
{
    m_textLineEdit = text;
    int numTemp = text.toUInt(NULL, 16);
    ui->lineEdit->setText(text);
    for (int i =0; i< 8; i++){
        m_listCheckBoxSecCap[i]->setChecked((numTemp & (1 << i)));
    }
}

void cSecurityCapabilities::on_pushButton_clicked()
{
    m_textLineEdit = ui->lineEdit->text();
    emit ButtonOK_SecCap_clicked(m_textLineEdit);
    this->close();
}

void cSecurityCapabilities::on_pushButton_2_clicked()
{
    this->close();
}

void cSecurityCapabilities::on_lineEdit_textEdited(const QString &arg1)
{
    qint32  lengthString;
    qint32  cursorPos;
    qint32  numTemp;
    QString stringTemp;
    bool    insert = false;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 2)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }

    if (true == insert)
    {
        ui->lineEdit->setText(stringTemp);
        ui->lineEdit->setCursorPosition(cursorPos-1);
        insert = false;
    }

    numTemp = stringTemp.toInt(NULL, 16);
    for (qint32 i =0; i < m_listCheckBoxSecCap.length(); i++)
    {
        m_listCheckBoxSecCap.at(i)->setChecked(numTemp &(1 << i));
    }
}
