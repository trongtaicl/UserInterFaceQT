#include "cCreateStaticVariable.h"

QString cCreateStaticVariable::m_dirToDataStorage = "C:/";
QString cCreateStaticVariable::m_dirToTerProcess  = "C:/";

cCreateStaticVariable::cCreateStaticVariable()
{

}

QString cCreateStaticVariable::getDirToOpenProcessing()
{
    qint32 index =0;
    QString dirTemp;

    index = m_dirToTerProcess.lastIndexOf("/");

    if (index < m_dirToTerProcess.length()-1)
    {
       dirTemp = m_dirToTerProcess.remove(index+1, m_dirToTerProcess.length()-index);
    }
    else
    {
        dirTemp = m_dirToTerProcess;
    }

    return dirTemp;


}

QString cCreateStaticVariable::getDirToDataStorage()
{
    qint32 index =0;
    QString dirTemp;

    index = m_dirToDataStorage.lastIndexOf("/");

    if (index < m_dirToDataStorage.length()-1)
    {
       dirTemp = m_dirToDataStorage.remove(index+1, m_dirToDataStorage.length()-index);
    }
    else
    {
        dirTemp = m_dirToDataStorage;
    }

    return dirTemp;
}

void cCreateStaticVariable::updateValueDirTerProcess(QString text)
{
    m_dirToTerProcess = text;
}

void cCreateStaticVariable::updateValueDirDataStorage(QString text)
{
    m_dirToDataStorage = text;
}
