#include "cAdditionalTerCapabilities.h"
#include "ui_cAdditionalTerCapabilities.h"
#include <QDebug>


cAdditionalTerCapabilities::cAdditionalTerCapabilities(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cAdditionalTerCapabilities)
{
    ui->setupUi(this);
    this->setWindowTitle("Additional Terminal Capabilities (tag 9F40)");
    m_sigMappMSB = new QSignalMapper(this);
    m_sigMappLSB = new QSignalMapper(this);

    ui->pushButtonOK->setStyleSheet("QPushButton{background-color: black; color: white; }");
    ui->pushButtonCancel->setStyleSheet("QPushButton{background-color: black; color:white;}");

    InitializeCheckbox();

    /* initialize line edit */
    ui->lineEdit->setInputMask("hhhhhhhhhh");
    ui->lineEdit->setText("0000000000");

    /* connect every clicked checkbox signal with slot: map() of m_sigMappMSB or m_sigMappLSB
     *
     * every time, a checkbox is clicked, it will call slot: SetTextMSB() or SetTextLSB()
     */
    for (int i =0; i< 24; i++){
        m_listCheckBoxMSB<< m_checkBoxMSB[i];   /* assign every element in m_checkBoxMSB[] to m_listCheckBoxMSB */

        /* connect every statechanged signal of checkbox to slot map() of m_sigMappMSB */
        m_sigMappMSB->setMapping(m_listCheckBoxMSB[i], i);
        connect(m_listCheckBoxMSB[i], SIGNAL(clicked()), m_sigMappMSB, SLOT(map()));
    }

    for (int i =0; i< 16; i++){
        m_listCheckBoxLSB << m_checkBoxLSB[i];  /* assign every element in m_checkBoxLSB[] to m_listCheckBoxLSB */

        /* connect every statechanged signal of checkbox to slot map() of m_sigMappLSB */
        m_sigMappLSB->setMapping(m_listCheckBoxLSB[i], i);
        connect(m_listCheckBoxLSB[i], SIGNAL(clicked()), m_sigMappLSB, SLOT(map()));
    }

    connect(m_sigMappMSB, SIGNAL(mapped(int)), this, SLOT(set_text_MSB()));
    connect(m_sigMappLSB, SIGNAL(mapped(int)), this, SLOT(set_text_LSB()));
}

cAdditionalTerCapabilities::~cAdditionalTerCapabilities()
{
    delete ui;
}

/**
 * @brief cAdditionalTerCapabilities::ChangeTextEdit
 * get text from mainwindow.cpp or paypassentrypoint.cpp to the LineEdit
 * check the checkboxes according to value of LineEdit
 * @return: none
 */
void cAdditionalTerCapabilities::change_text_edit(const QString &text, mlsWinAdd_t win)
{
  m_addterTextLineEdit = text;
  m_winSelected        = win;

  ui->lineEdit->setText(m_addterTextLineEdit);

  int num = ConvertStringtoNum(m_addterTextLineEdit, 0, 6);  /* call function ConvertStringtoNum that is defined below */

  for (int i =0; i<24; i++){
      /* check or uncheck ith checkbox of array m_checkBoxMSB */
       m_checkBoxMSB[i]->setChecked((num & (1<<i)));
  }
/* call function ConvertStringtoNum that is defined below */
  num = ConvertStringtoNum(m_addterTextLineEdit, 6, 4);

  for (int i =0; i<16; i++){
      /* check or uncheck ith checkbox of array m_checkBoxLSB */
       m_checkBoxLSB[i]->setChecked((num & (1 << i)));
  }
}

/**
 * @brief cAdditionalTerCapabilities::SetText1
 * @param Index: checkbox in index position is clicked.
 * this slot is connected with mapped(int) signal of sigmapp1
 * according to the value of LineEdit, checkbox will be checked or unchecked
 * it update value of three MSB (bytes) in LineEdit
 * @return: none
 */
void cAdditionalTerCapabilities::set_text_MSB()
{
  bool state = false;

  for (int i =0; i< 24; i++){
      state = m_listCheckBoxMSB[i]->checkState();  /* check state of all the checkbox in m_listCheckBoxMSB */

      if (state){  /*if checkbox is checked, bit in i position of m_headNum will be 1 */
        m_headNum |= (1<<i);
      }

      else{   /* if checkbox is unchecked, bit in i position of m_headNum will be 0*/
          m_headNum &= ~(1<<i);
      }
  }
  /* update text of LineEdit according to m_headNum and m_tailNum */
  ui->lineEdit->setText(QString("%1%2").arg(m_headNum, 6, 16,
                                            QChar('0')).arg(m_tailNum, 4, 16, QChar('0')).toUpper());
}

/**
 * @brief cAdditionalTerCapabilities::SetText2
 * @param Index: checkbox in Index position is clicked.
 * this slot is connected with mapped(int) signal of sigmapp2
 * according to the value of LineEdit, checkbox will be checked or unchecked
 * it update value of two LSB (bytes) in LineEdit
 * @return: none
 */
void cAdditionalTerCapabilities::set_text_LSB()
{
    bool state = false;

    for(int i =0; i< 16; i++){
        state = m_listCheckBoxLSB[i]->checkState();

        if (state ==true){  /*if checkbox is checked, bit in i position of m_tailNum will be 1 */
        m_tailNum |= (1<<i);
        }

        else {  /*if checkbox is checked, bit in i position of m_tailNum will be 0 */
            m_tailNum &= ~(1<<i);
        }
    }
    /* update text of LineEdit according to m_headNum and m_tailNum */
    ui->lineEdit->setText(QString("%1%2").arg(m_headNum, 6, 16,
                                              QChar('0')).arg(m_tailNum, 4, 16, QChar('0')).toUpper());
}

/**
 * @brief cAdditionalTerCapabilities::on_pushButtonOK_clicked
 * when Button_Ok is clicked, check if the window to send a signal is mainwindow or paypassentrypoint
 * update value of TextLineEdit
 * @return : none
 */
void cAdditionalTerCapabilities::on_pushButtonOK_clicked()
{
    m_addterTextLineEdit = ui->lineEdit->text();

    if (m_winSelected == waMainWindow){  /* if the window sent is mainwindow, emit a signal */
    emit ButtonOk_MainWindow_Clicked(ui->lineEdit->text());
    }

    else if (m_winSelected == waPayPass){  /* if the window sent is paypass, emit a signal */
        emit ButtonOk_PayPass_Clicked(ui->lineEdit->text());
    }
    this->close();  /* close the window */
}

/**
 * @brief cAdditionalTerCapabilities::on_pushButtonCancel_clicked
 * close this window, but doesn't save any change that maked
 * @return : none
 */
void cAdditionalTerCapabilities::on_pushButtonCancel_clicked()
{
    this->close();
}

/**
 * @brief cAdditionalTerCapabilities::InitializeCheckbox
 * initialize the checkbox,
 * add all the checkbox into two arrays  m_checkBoxMSB and m_checkBoxLSB
 */
void cAdditionalTerCapabilities::InitializeCheckbox()
{
    m_checkBoxMSB[23] = ui->checkBoxCash;
    m_checkBoxMSB[22] = ui->checkBoxGoods;
    m_checkBoxMSB[21] = ui->checkBoxService;
    m_checkBoxMSB[20] = ui->checkBoxCashback;
    m_checkBoxMSB[19] = ui->checkBoxInquiry;
    m_checkBoxMSB[18] = ui->checkBoxTransfer;
    m_checkBoxMSB[17] = ui->checkBoxPayment;
    m_checkBoxMSB[16] = ui->checkBoxAdmin;
    m_checkBoxMSB[15] = ui->checkBoxCashDepo;
    m_checkBoxMSB[14] = ui->checkBoxRFU1;
    m_checkBoxMSB[13] = ui->checkBoxRFU2;
    m_checkBoxMSB[12] = ui->checkBoxRFU3;
    m_checkBoxMSB[11] = ui->checkBoxRFU4;
    m_checkBoxMSB[10] = ui->checkBoxRFU5;
    m_checkBoxMSB[9]  = ui->checkBoxRFU6;
    m_checkBoxMSB[8]  = ui->checkBoxRFU7;
    m_checkBoxMSB[7]  = ui->checkBoxNumeric;
    m_checkBoxMSB[6]  = ui->checkBoxAlpha;
    m_checkBoxMSB[5]  = ui->checkBoxCommand;
    m_checkBoxMSB[4]  = ui->checkBoxFunction;
    m_checkBoxMSB[3]  = ui->checkBoxRFU8;
    m_checkBoxMSB[2]  = ui->checkBoxRFU9;
    m_checkBoxMSB[1]  = ui->checkBoxRFU10;
    m_checkBoxMSB[0]  = ui->checkBoxRFU11;

    m_checkBoxLSB[15] = ui->checkBoxPrint;
    m_checkBoxLSB[14] = ui->checkBoxCardholder;
    m_checkBoxLSB[13] = ui->checkBoxAttendant;
    m_checkBoxLSB[12] = ui->checkBoxDisplayCard;
    m_checkBoxLSB[11] = ui->checkBoxRFU12;
    m_checkBoxLSB[10] = ui->checkBoxRFU13;
    m_checkBoxLSB[9]  = ui->checkBoxCode10;
    m_checkBoxLSB[8]  = ui->checkBoxCode9;
    m_checkBoxLSB[7]  = ui->checkBoxCode8;
    m_checkBoxLSB[6]  = ui->checkBoxCode7;
    m_checkBoxLSB[5]  = ui->checkBoxCode6;
    m_checkBoxLSB[4]  = ui->checkBoxCode5;
    m_checkBoxLSB[3]  = ui->checkBoxCod4;
    m_checkBoxLSB[2]  = ui->checkBoxCode3;
    m_checkBoxLSB[1]  = ui->checkBoxCode2;
    m_checkBoxLSB[0]  = ui->checkBoxCode1;


}

void cAdditionalTerCapabilities::updateCheckBoxBasedOnLineEdit(const QString &text, QList<QCheckBox *> *list)
{
    qint64 numConverted ;

    numConverted = text.toInt(NULL, 16);
    for (qint32 i =0; i < list->length(); i++)
    {
        list->at(i)->setChecked(numConverted &(1<<i));
    }
}
/*
 * convert from string to number
 * @return: a integer number
 * */
/**
 * @brief cAdditionalTerCapabilities::ConvertStringtoNum, convert string to number
 * @param text
 * @param Pos
 * @param Num
 * @return integer number, the number after converting from text
 *
 */
int cAdditionalTerCapabilities::ConvertStringtoNum(const QString &text, int pos, int num)
{
    bool status = false;
    int numTemp = 0;
    QString textTemp =0;

    /* get Num characters from Pos position of string text */
    textTemp = text.mid(pos, num);

    numTemp = textTemp.toUInt(&status, 16);

    return numTemp;
}

void cAdditionalTerCapabilities::on_lineEdit_cursorPositionChanged(int arg1, int arg2)
{

}

void cAdditionalTerCapabilities::on_lineEdit_textEdited(const QString &arg1)
{
    qint32  lengthString;
    qint32  cursorPos;
    QString stringTemp;
    QString stringSplitedMSB, stringSplitedLSB;
    bool    insert = false;

    qDebug() << arg1;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 10)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }
    qDebug() << insert;
    if (true == insert)
    {
        ui->lineEdit->setText(stringTemp);
        ui->lineEdit->setCursorPosition(cursorPos-1);
        insert = false;
    }

    stringSplitedMSB = stringTemp.mid(0, 6);
    stringSplitedLSB = stringTemp.mid(6,4);

    updateCheckBoxBasedOnLineEdit(stringSplitedMSB, &m_listCheckBoxMSB);
    updateCheckBoxBasedOnLineEdit(stringSplitedLSB, &m_listCheckBoxLSB);
}

void cAdditionalTerCapabilities::on_lineEdit_textChanged(const QString &arg1)
{

}
