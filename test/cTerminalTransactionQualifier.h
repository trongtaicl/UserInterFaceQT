#ifndef CTERMINALTRANSACTIONQUALIFIER_H
#define CTERMINALTRANSACTIONQUALIFIER_H

#include <QDialog>
#include <QSignalMapper>
#include <QString>
#include <QCheckBox>
#include <QList>
namespace Ui {
class cTerminalTransactionQualifier;
}

class cTerminalTransactionQualifier : public QDialog
{
    Q_OBJECT

public:
    explicit cTerminalTransactionQualifier(QWidget *parent = 0);
    ~cTerminalTransactionQualifier();

private:
    Ui::cTerminalTransactionQualifier *ui;
};

#endif // CTERMINALTRANSACTIONQUALIFIER_H
