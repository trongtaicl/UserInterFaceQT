#include "cAceExit.h"
#include "ui_cAceExit.h"

cAceExit::cAceExit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cAceExit)
{
    ui->setupUi(this);
}

cAceExit::~cAceExit()
{
    delete ui;
}

void cAceExit::on_pushButton_clicked()
{
    this->close();
    emit buttonOK_clicked();
}

void cAceExit::on_pushButton_2_clicked()
{
    this->close();
}
