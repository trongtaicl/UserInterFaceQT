#ifndef cPaywaveEntryPoint_H
#define cPaywaveEntryPoint_H

#include <QDialog>
#include <QComboBox>
#include <QSpinBox>
#include <QCheckBox>
#include "cSplitTag.h"

namespace Ui {
class cPaywaveEntryPoint;
}

typedef enum{
    inpwTerCounCode      = 0x00,
    inpwTerCap           = 0x01,
    inpwAddTerCap        = 0x02,
    inpwTerTranQual      = 0x03,
    inpwKerConfig        = 0x04,
    inpwMessHoldTime     = 0x05,
    inpwAppVerNum        = 0x06,
    inpwBitmap           = 0x07,
    inpwReadContFloLimit = 0x08,
    inpwRCTL             = 0x09,
    inpwReadCVMLimit     = 0x0A
}mls_IndexPaywave_t;


class cPaywaveEntryPoint : public QDialog
{
    Q_OBJECT

public:
    explicit cPaywaveEntryPoint(QWidget *parent = 0);
    ~cPaywaveEntryPoint();

    void InitializePaywaveEntryPoint();

    void InitializeToolButton();

    void CheckTagAvai(uint8_t *Buffertam, uint16_t LengTotal);

    void UpdateData(uint8_t Index, uint8_t Sobyte, uint8_t *Buffertam, uint8_t Length);

    uint8_t Buffertam[200];
    uint16_t Length;

public slots:
    void UpdatePaywave();

private:
    Ui::cPaywaveEntryPoint *ui;

    QList <QCheckBox*>  ListCheckbox;   /**/
    QList <QLineEdit*>  LineEditCheck;  /**/
    QList <QComboBox*>  listCombo;
    QList <QCheckBox*>  listCheckboxCombo;
    QList <QSpinBox*>   listSpinbox;
    QList <QCheckBox*>  listCheckboxSpin;

    QStringList         ComboTerType;   /**/
    QStringList         ComboState;
    cSplitTag           mTLV;
};

#endif // cPaywaveEntryPoint_H
