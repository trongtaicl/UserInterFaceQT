#include "cCardDataInputCapabilities.h"
#include "ui_cCardDataInputCapabilities.h"

cCardDataInputCapabilities::cCardDataInputCapabilities(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cCardDataInputCapabilities)
{
    ui->setupUi(this);
    this->setWindowTitle("Card Data Input Capabilities");
    /* save all checkbox in window a listCheckBoxcCardDataInputCapabilities*/
    m_listCheckBoxCardData << ui->checkBox << ui->checkBox_2 << ui->checkBox_3 << ui->checkBox_4
                         << ui->checkBox_5 << ui->checkBox_6 << ui->checkBox_7 << ui->checkBox_8;

    /* initialize the properties of lineEdit, user only type hexa number into line edit*/
    ui->lineEdit->setInputMask("hh");
    ui->lineEdit->setText("00");
    ui->lineEdit->setAlignment(Qt::AlignRight);

    m_sigMapp = new QSignalMapper(this); /* declare m_sigMapp */

    for (int i =0; i< 8; i++){  /*connect every single checkbox of list to m_sigMapp */
        m_sigMapp->setMapping(m_listCheckBoxCardData[i], i);
        connect(m_listCheckBoxCardData[i], SIGNAL(clicked()), m_sigMapp, SLOT(map()));
    }
    /* connect m_sigMapp with index with slot: CheckboxUpdate */
    connect(m_sigMapp, SIGNAL(mapped(int)), this, SLOT(check_box_update(int)));

}

cCardDataInputCapabilities::~cCardDataInputCapabilities()
{
    delete ui;
}

/**
 * @brief carddata::on_pushButton_clicked, OK button is clicked
 * assign value of LineEdit to cdTextLineEdit to update value of lineEdit in paypassentrypoint.cpp
 * @return: none
 */
void cCardDataInputCapabilities::on_pushButton_clicked()
{
  m_textLineEdit = ui->lineEdit->text();
  emit ButtonOk_CardData_clicked(ui->lineEdit->text());
  this->close();
}

/**
 * @brief cCardDataInputCapabilities::check_box_update, every checkbox is clicked, it will update value of text in LineEdit
 * @param index
 * @return: none
 */
void cCardDataInputCapabilities::check_box_update(int index)
{
   bool state =false;
   int num =0;
   for (int i =0; i< 8; i++){
       state = m_listCheckBoxCardData[i]->checkState();

       if (state){       /* ith checkbox is checked, bit i of Num will be 1  */
           num |= (1<<i);
       }
       else {         /* ith checkbox is not checked, bit i of Num will be 0  */
           num &= ~(1<<i);
       }
   }
   /* assign value of Num to text of LineEdit */
   ui->lineEdit->setText(QString("%1").arg(num, 2, 16, QChar('0')).toUpper());
}

/**
 * @brief cCardDataInputCapabilities::cd_change_text_edit, convert string: cdTextLineEdit to an integer number.
 * check all bit of numTemp, if ith bit equals 1, ith checkbox will be checked
 * return: none
 */
void cCardDataInputCapabilities::cd_change_text_edit(const QString &text)
{
    m_textLineEdit = text;
    /* convert from string to a integer number*/
    int numTemp = m_textLineEdit.toUInt(NULL, 16);

    ui->lineEdit->setText(m_textLineEdit);  /*assign value of cdTextLineEdit to lineEdit of cCardDataInputCapabilities.cpp */

    for (int i =0; i< 8; i++){ /*check the ith checkbox if ith bit of numTemp is 1 */
        m_listCheckBoxCardData[i]->setChecked((numTemp & (1 << i)));
    }
}

/**
 * @brief cCardDataInputCapabilities::on_pushButton_2_clicked, Cancel Button is clicked
 * close the window
 */
void cCardDataInputCapabilities::on_pushButton_2_clicked()
{
    this->close();
}

void cCardDataInputCapabilities::on_lineEdit_textEdited(const QString &arg1)
{
    qint32  lengthString;
    qint32  cursorPos;
    qint32  numTemp;
    QString stringTemp;
    bool    insert = false;

    lengthString = arg1.length();
    cursorPos = ui->lineEdit->cursorPosition();
    stringTemp = arg1;

    while (stringTemp.length() < 2)
    {
        stringTemp.insert(cursorPos, QString("0"));
        cursorPos ++;
        insert = true;
    }

    if (true == insert)
    {
        ui->lineEdit->setText(stringTemp);
        ui->lineEdit->setCursorPosition(cursorPos-1);
        insert = false;
    }

    numTemp = stringTemp.toInt(NULL, 16);
    for (qint32 i =0; i < m_listCheckBoxCardData.length(); i++)
    {
        m_listCheckBoxCardData.at(i)->setChecked(numTemp &(1 << i));
    }
}
