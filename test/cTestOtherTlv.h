#ifndef TESTOTHERTLV_H
#define TESTOTHERTLV_H

#include <QString>
#include <QtCore>

class cTestOtherTLV
{
public:
    cTestOtherTLV();

    bool convertStringtoNumAndCheck(QString stringInput, quint8 *bufferTemp);

    bool checkTagOtherTLV(quint8 *bufferTemp, quint16 lengthTotal);
};

#endif // TESTOTHERTLV_H
