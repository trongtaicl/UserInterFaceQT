#include "cGetEntryPointAce.h"
#include "ui_cGetEntryPointAce.h"

cGetEntryPointAce::cGetEntryPointAce(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cGetEntryPointAce)
{
    ui->setupUi(this);
    this->setWindowTitle("Dialog");
    QStringList stringLib ;
    QStringList stringConfig;
    stringLib << "MCL v3 TAL2-Testing Env July 2015" << "MCL v3.1 TAL2-Testing Env July 2016"
              << "cup" << "dpas10" << "eftpos" << "emvco-entrypoint" << "expressionpay30" << "interact"
              << "jcb" << "paypass302" << "paywave213_ICC" << "paywave213b_qVSDC_Eval" << "pure21"
              << "zip" ;

    for (int i =0; i< stringLib.count(); i++){
        ui->comboBoxLib->addItem(QString(stringLib.at(i)));
    }
    stringConfig << "PPS_BAL" << "PPS_BAL2" << "PPS_CVM_1" << "PPS_CVM_2" << "PPS_LIMIT_1" << "PPS_LIMIT_1B"
                << "PPS_LIMIT_2" << "PPS_LIMIT_3" << "PPS_MC_ATM" << "PPS_MCERRKERNELCONF" << "PPS_MCHIP1"
                << "PPS_MCHIP2" << "PPS_MCHIP3" << "PPS_MCHIP4" << "PPS_MCHIP5" << "PPS_MCHIP6" << "PPS_MCHIP7"
                << "PPS_MCHIP8" << "PPS_MCNODEFAULT_1" << "PPS_MCNODEFAULT_2" << "PPS_MCNODEFAULT_3" << "PPS_MCNODEFAULT_4"
                << "PPS_MCNODEFAULT_5" << "PPS_MCNOMSI" << "PPS_MC_NOATM" << "PPS_MCONLINEONLY" << "PPS_MC_PROP"
                << "PPS_MC_PROP2" << "PPS_MC_PROP3" << "PPS_MSTRIPE1" << "PPS_NODEFAULT_1" << "PPS_NODEFAULT_2"
                << "PPS_NODEFAULT_3" << "PPS_PERF_MC" << "PPS_PERF_MS" << "PPS_POSTBAL" << "PPS_POSTBAL2" << "PPS_PREBAL"
                << "PPS_PREBAL2" << "PPS_SELECT1" << "PPS_TIP_OFFLINE" << "PPS_TIP_ONLINE" << "PPS_TORN3MIN" << "PPS_TORN5SEC"
                << "PPS_TORN_BAL" << "PPS_TORN_MAX1" << "PPS_TORN_MAX2" << "PPS_TRXTYPEOTHER";

    for(int i =0; i< stringConfig.count(); i++){
        ui->comboBoxConfig->addItem(QString(stringConfig.at(i)));
    }

}

cGetEntryPointAce::~cGetEntryPointAce()
{
    delete ui;
}
