#ifndef cSecurityCapabilities_H
#define cSecurityCapabilities_H

#include <QDialog>
#include <QString>
#include <QCheckBox>
#include <QSignalMapper>
#include <QList>
#include "stdint.h"

namespace Ui {
class cSecurityCapabilities;
}

class cSecurityCapabilities : public QDialog
{
    Q_OBJECT

public:
    explicit cSecurityCapabilities(QWidget *parent = 0);
    ~cSecurityCapabilities();

    QString seccapTextLineEdit;

public slots:
    void CheckboxUpdate(int index);

    void scChangeTextEdit(const QString &text);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_lineEdit_textEdited(const QString &arg1);

private:
    Ui::cSecurityCapabilities *ui;
    QSignalMapper *m_sigMapp;

    QList<QCheckBox*> m_listCheckBoxSecCap;
    uint8_t m_num =0;

    QString m_textLineEdit;

signals:
    void ButtonOK_SecCap_clicked(const QString &text);
};

#endif // cSecurityCapabilities_H
