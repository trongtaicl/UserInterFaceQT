// NAME.......  sha1.c
// PURPOSE....  Generic Platform Interface - SHA-1 Hash (or Digest) Algorithm Functions Simulated
// PROJECT....  GPI / Linux Platform
// REFERENCES.
//
// Copyright ©2005-2016 - 9164-4187 QUEBEC INC (“AMADIS”), All Rights Reserved
//

//---------------------------------------------------------
//			Include Files
//---------------------------------------------------------
//---- Local Headers ----
#include "cSha1.h"
//---- System Headers ----
#include <string.h>			// memxxx
#include <stdio.h>			// printf

//---------------------------------------------------------
//			Definitions
//---------------------------------------------------------
//
typedef unsigned char 	uchar;
typedef unsigned long	uint32;

//---------------------------------------------------------
//			Declarations
//---------------------------------------------------------
//

//#define BigEndian

/*
#ifdef Word64
typedef unsigned short	int32;
#else
typedef unsigned int	uint32;
#endif
*/
#ifdef BigEndian
#define StoreBig32(in,out);\
	*(in)=(uchar)(out);\
	*(in+1)=(uchar)((out)>>8);\
	*(in+2)=(uchar)((out)>>16);\
	*(in+3)=(uchar)((out)>>24);
#define StoreLittle32(in,out)\
	memcpy(in,(uchar*)&out,4);
#else
#define StoreBig32(in,out)\
	memcpy(in,(uchar*)&out,4);
#define StoreLittle32(in,out);\
	*(in)=(uchar)(out>>24);\
	*(in+1)=(uchar)(out>>16);\
	*(in+2)=(uchar)(out>>8);\
	*(in+3)=(uchar)(out);
#define LoadBig32(in,out)\
	memcpy((uchar*)&in,out,4);
#endif

void	ProcessShaBlock(		// Process 16 Byte Block
			tSHA*,				// Context
			unsigned char*);	// Block To Process


//---------------------------------------------------------
//			ShaInit()
//---------------------------------------------------------
//	SHA Initialisation
//
//void gpiShaInit(tSHA* ctx) {
void ShaInit(tSHA* ctx) {
	ctx->totLen=ctx->bufLen=0;
	ctx->hashLen=20;
	ctx->H[0]=0x67452301L;
	ctx->H[1]=0xEFCDAB89L;
	ctx->H[2]=0x98BADCFEL;
	ctx->H[3]=0x10325476L;
	ctx->H[4]=0xC3D2E1F0L;
	memset(ctx->W,0,sizeof(ctx->W));
}


//---------------------------------------------------------
//			ShaUpdate()
//---------------------------------------------------------
//	SHA Processing on Data
//
//void gpiShaUpdate(tSHA* ctx,unsigned char* msg,int len) {
void ShaUpdate(tSHA* ctx,unsigned char* msg,int len) {
	// 1. Update Data Buffer
	ctx->totLen+=len<<3;	// Update Length in Bytes
	if ((ctx->bufLen+len)<=64){
		memcpy(ctx->buf+ctx->bufLen,msg,len);
		ctx->bufLen+=len;
		len=0;
	}
	else{
		memcpy(ctx->buf+ctx->bufLen,msg,64-ctx->bufLen);
		len-=64-ctx->bufLen;
		msg+=64-ctx->bufLen;
		ctx->bufLen=64;
	}

	// 2. Iterations
	for(; ctx->bufLen==64;){
 		ProcessShaBlock(ctx,ctx->buf);
		ctx->bufLen=(len>64)? 64: len;
		memcpy(ctx->buf,msg,ctx->bufLen);
		len-=ctx->bufLen;
		msg+=ctx->bufLen;
	}
}


//---------------------------------------------------------
//			ShaFinal()
//---------------------------------------------------------
//	Last SHA Processing
//
//void gpiShaFinal(tSHA* ctx,unsigned char* out) {
void ShaFinal(tSHA* ctx,unsigned char* out) {
	// 1.Complete With Padding
	ctx->buf[ctx->bufLen++]=0x80;
	if (ctx->bufLen>56){		// Padding Generate Two Blocks
		memset(ctx->buf+ctx->bufLen,0,128-ctx->bufLen);
		ctx->bufLen=124;
	}
	else{
		memset(ctx->buf+ctx->bufLen,0,64-ctx->bufLen);
		ctx->bufLen=60;
	}

	// 2. Store Length
	StoreLittle32(ctx->buf+ctx->bufLen,ctx->totLen);

	// 3. Last Iterations
	ProcessShaBlock(ctx,ctx->buf);
	if (ctx->bufLen>64)
		ProcessShaBlock(ctx,ctx->buf+64);

	// 4. Result
	StoreLittle32(out,ctx->H[0]);
	StoreLittle32(out+4,ctx->H[1]);
	StoreLittle32(out+8,ctx->H[2]);
	StoreLittle32(out+12,ctx->H[3]);
	StoreLittle32(out+16,ctx->H[4]);
}


//---------------------------------------------------------
//			ProcessShaBlock()
//---------------------------------------------------------
//	64 Bytes Block SHA Process (Mi Process)
//
void ProcessShaBlock(tSHA* ctx,unsigned char* block) {
	int		i;			// Item Loop
	uint32	val;		// Temporary Value


	// 1. Divide Mi Into W0,...,W15 (step a)
	for(i=0; i<64; i++)
		ctx->W[i>>2]=(ctx->W[i>>2]<<8)+*block++;

	// 2. Processing W16 To W79 - Init A To E (step b c)
	for(i=16; i<80; i++){
		val=ctx->W[i-3]^ctx->W[i-8]^ctx->W[i-14]^ctx->W[i-16];
		ctx->W[i]=(val<<1)+(val>>31);
	}
	ctx->A=ctx->H[0]; ctx->B=ctx->H[1]; ctx->C=ctx->H[2]; ctx->D=ctx->H[3]; ctx->E=ctx->H[4];

	// 3. Round 0 To 79 (step d)
	for(i=0; i<20; i++){
		val=(ctx->A<<5)+(ctx->A>>27)+((ctx->B&ctx->C)|((~ctx->B)&ctx->D))+ctx->E+ctx->W[i]+0x5A827999L;
		ctx->E=ctx->D; ctx->D=ctx->C; ctx->C=(ctx->B<<30)+(ctx->B>>2); ctx->B=ctx->A; ctx->A=val;
	}
	for(; i<40; i++){
		val=(ctx->A<<5)+(ctx->A>>27)+(ctx->B^ctx->C^ctx->D)+ctx->E+ctx->W[i]+0x6ED9EBA1L;
		ctx->E=ctx->D; ctx->D=ctx->C; ctx->C=(ctx->B<<30)+(ctx->B>>2); ctx->B=ctx->A; ctx->A=val;
	}
	for(; i<60; i++){
		val=(ctx->A<<5)+(ctx->A>>27)+((ctx->B&ctx->C)|(ctx->B&ctx->D)|(ctx->C&ctx->D))+ctx->E+ctx->W[i]+0x8F1BBCDCL;
		ctx->E=ctx->D; ctx->D=ctx->C; ctx->C=(ctx->B<<30)+(ctx->B>>2); ctx->B=ctx->A; ctx->A=val;
	}
	for(; i<80; i++){
		val=(ctx->A<<5)+(ctx->A>>27)+(ctx->B^ctx->C^ctx->D)+ctx->E+ctx->W[i]+0xCA62C1D6L;
		ctx->E=ctx->D; ctx->D=ctx->C; ctx->C=(ctx->B<<30)+(ctx->B>>2); ctx->B=ctx->A; ctx->A=val;
	}

	// 4. Update Digest (step e)
	ctx->H[0]+=ctx->A; ctx->H[1]+=ctx->B; ctx->H[2]+=ctx->C; ctx->H[3]+=ctx->D; ctx->H[4]+=ctx->E;
}
